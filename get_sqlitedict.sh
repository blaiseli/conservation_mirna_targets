#!/bin/bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="sqlitedict"
version="1.2.0"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${prog}-${version}*
# Download it
wget https://pypi.python.org/packages/source/s/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
python setup.py build || error_exit "${prog} build failed"
python setup.py install --user || error_exit "${prog} install failed"

exit 0
