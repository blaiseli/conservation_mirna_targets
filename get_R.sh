#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="R"
version="3.2.2"

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}-${version}*
wget --continue "https://cran.rstudio.com/src/base/R-3/${program}-${version}.tar.gz"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
#./configure --prefix=${HOME} --with-x=no CFLAGS="-O3 -march=native -fomit-frame-pointer -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
# It seems that either -O3, either -fomit-frame-pointer makes compilation segfault (not further tested)
./configure --prefix=${HOME} --with-x=no CFLAGS="-march=native -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
#./configure --prefix=${HOME} --with-x=no CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
make || error_exit "${program} build failed"
make check || error_exit "${program} check failed"
make install || error_exit "${program} install failed"

exit 0
