#!/bin/bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="2.5"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf lbzip2-${version}*
# Download it
wget http://archive.lbzip2.org/lbzip2-${version}.tar.bz2 || error_exit "lbzip2 download failed"
tar -xvjf lbzip2-${version}.tar.bz2
cd lbzip2-${version}
# Default ?
#./configure --prefix=${HOME} CFLAGS="-g -O2"
# Trying to optimize
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" || error_exit "lbzip2 config failed"
make || error_exit "lbzip2 build failed"
make install || error_exit "lbzip2 install failed"

exit 0
