#!/usr/bin/env bash
# To use after get_samtools.sh so that the programs can use the locally-compiled zlib
# Also depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

prog="pybedtools"
version="0.7.4"
samtools_version="1.1"
htslib_version=${samtools_version}

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${prog}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/p/${prog}/${prog}-${version}.tar.gz || error_exit "${prog} download failed"
tar -xvzf ${prog}-${version}.tar.gz
cd ${prog}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${prog} build failed"
python setup.py install --user || error_exit "${prog} install failed"


if [[ -e $1 ]]
then
    version_file="$1"
else
    version_file="/dev/stdout"
fi

echo -e "${prog}\t${version}" >> ${version_file}

exit 0
