#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""
This script combines information from various sources to add overconservation
information as extra columns to lines from a *_Family_Info.txt TargetScan file.
"""

import argparse
import sys
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


#import copy
import os
OPJ = os.path.join
OPF = os.path.isfile
#OPE = os.path.exists
#OPB = os.path.basename
from os.path import splitext
#from re import sub
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write


#from operator import attrgetter
#from operator import and_, or_
#from operator import eq
#
#from bisect import insort
#from bisect import bisect_left
from itertools import \
        compress, \
        dropwhile, \
        imap, \
        izip, \
        repeat, \
        starmap, \
        takewhile

#from collections import defaultdict


#import pysam
from pysam import TabixFile

from pybedtools import BedTool as BT
#from pybedtools import Interval
#
#from Bio.Seq import reverse_complement
# To get back dictionaries that convert between codes and NCBI taxonomy IDs
#from cPickle import load

from string import maketrans, split
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")
RNA2DNA_TABLE = maketrans("U", "T")


def to_dna(seq):
    """Converts an upper-case RNA sequence into an upper-case DNA sequence.
    Actually, this just substitutes Us for Ts."""
    return seq.translate(RNA2DNA_TABLE)


# To avoid repeatedly calling these as string methods:
UPPER = str.upper
STRIP = str.rstrip
# Cannot deal with unicode
#SPLIT = str.split
# To join the letters of the seeds
CAT = "".join
JOIN = str.join


class UnknownTranscriptId(Exception):
    """This exception is used to indicate that a transcript ID is not known."""
    pass


class SpecialChromosome(Exception):
    """This exception is used to indicate that a transcript comes from a
    non-standard chromosome."""
    pass


class UnknownCDS(Exception):
    """This exception is used to indicate that a CDS is not known."""
    pass

class NonMatchingSeed(Exception):
    """This exception is used to indicate that a miRNA seed match did not
    correspond to the associated seed."""
    pass

class OutsideTranscript(Exception):
    """This exception is used to indicate that site coordinates fall outside
    of the transcript as described in RefSeq refGene.txt file."""
    pass

class AcrossIntron(Exception):
    """This exception is used to indicate that site coordinates fall both sides
    of an intron."""
    pass


def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]


def lengths_from_starts_ends(starts, ends):
    """Returns a list of fragment lengths given the list of the fragments
    starts and ends"""
    return list((end + 1) - start for (start, end) in izip(starts, ends))


def strip_split(text):
    """Strips trailing newline character and then splits around tabs."""
    return split(STRIP(text, "\n"), "\t")


def remove_ext(word):
    """Returns the part of a word before the last dot."""
    return splitext(word)[0]


#def strip_split_de_ext(text):
#    """Strips trailing blank characters and then splits around tabs."""
#    return map(remove_ext, split(STRIP(text), "\t"))
#
#
def space_strip_split(text):
    """Strips trailing blank characters and then splits around spaces."""
    return split(STRIP(text), " ")


def get_transcript_info(refseq_file, refgene2enst, only_chrom=None):
    """*refseq_file* should be a file giving information about the structure of
    transcripts.
    This stores information about transcript location and structure in
    *Globals.enst2transcript_info*.
    *refgene2enst* is a function that converts a refgene transcript ID
    into an Ensembl transcript ID.
    This builds the set of chromosomes *Globals.chromosomes*."""
    # Coordinates are in BED-style format.
    # We convert this in 1-based inclusive.
    for fields in imap(strip_split, refseq_file):
        # transcript_id
        #name = fields[1]
        #chrom = fields[2]
        #strand = fields[3]
        name, chrom, strand = fields[1:4]
        if only_chrom is not None:
            if chrom != only_chrom:
                continue
        try:
            transcript_id = refgene2enst(name)
            Globals.converted_transcript_ids += 1
        except KeyError:
            Globals.unconverted_transcript_ids += 1
            # Gene name
            gene_name = fields[12]
            warnings.warn(
                "%s (%s) could not be converted to an Ensembl ID." % (
                    name, gene_name))
            continue
        Globals.chromosomes.add(chrom)
        # Gene name
        #name2 = fields[12]
        #exonStarts = [int(pos) for pos in fields[9].split(",")]
        #exonEnds = [int(pos) for pos in fields[10].split(",")]
        # Exon ends from the file are 1-based, but exon starts are 0-based,
        # because it is BED-style.
        # We have to add 1 to get the starts 1-based
        # and have everything internally consistant.
        exon_starts = [int(pos) + 1 for pos in split(fields[9], ",") if pos]
        exon_ends = [int(pos) for pos in split(fields[10], ",") if pos]

        # Determine 3'UTR "start" and "end"
        if strand == "+":
            #cdsEndStat = fields[14]
            if fields[14] != "cmpl":
                # Use this to flag the TargetScan data as ignored.
                Globals.enst2transcript_info[transcript_id] = (
                    chrom,
                    strand,
                    None,
                    None)
                # Skip data without a reliable CDS "end" coordinate.
                continue
            # BED-style end coordinates are "1-based".
            # We use them as-is internally.
            #txEnd = fields[5]
            # Not used here
            #utr_end = int(fields[5])
            #cdsEnd = fields[7]
            # UTR starts just after the end of the CDS.
            # Hence the "+ 1".
            utr_start = int(fields[7]) + 1
            if utr_start > exon_ends[-1]:
                # Gene name
                gene_name = fields[12]
                warnings.warn(
                    "%s (%s) has no 3'UTR." % (
                        name, gene_name))
                continue
            utr_exon_starts = list(
                dropwhile(lambda pos: pos < utr_start, exon_starts))
            utr_exon_ends = list(
                dropwhile(lambda pos: pos < utr_start, exon_ends))
            if not utr_exon_starts:
                # utr_start is inside the last exon
                utr_exon_starts.append(utr_start)
                assert utr_exon_ends
            elif utr_exon_ends[0] < utr_exon_starts[0]:
                utr_exon_starts = [utr_start] + utr_exon_starts
            # We ignore the exons before the first intron
            # and after the last one because we should never
            # have to take them into account when calculating
            # the genomic coordinate of a position in a 3'UTR
            next_intron_starts = [pos + 1 for pos in utr_exon_ends[:-1]]
            next_intron_ends = [pos - 1 for pos in utr_exon_starts[1:]]
            utr_exon_lengths = lengths_from_starts_ends(
                utr_exon_starts, utr_exon_ends)
            next_intron_lengths = lengths_from_starts_ends(
                next_intron_starts, next_intron_ends)
            assert len(next_intron_lengths) + 1 == len(utr_exon_lengths)
            Globals.enst2transcript_info[transcript_id] = (
                chrom,
                strand,
                utr_exon_starts[0],
                # append a None so that we do not loose
                # the last exon length in the zip
                zip(utr_exon_lengths, next_intron_lengths + [None]))
        else:
            #cdsStartStat = fields[13]
            if fields[13] != "cmpl":
                # Use this to flag the TargetScan data as ignored.
                Globals.enst2transcript_info[transcript_id] = (
                    chrom,
                    strand,
                    None,
                    None)
                # Skip data without a reliable CDS "start" coordinate.
                continue
            #txStart = fields[4]
            # txStart is 0-based, because it is BED-style.
            # Internally, we want a 1-based utr_start
            # Hence the "+ 1".
            # Not used here
            #utr_start = int(fields[4]) + 1
            #cdsStart = fields[6]
            # cdsStart is 0-based, because it is BED-style.
            # This happens to corresponds to the 1-based coordinate
            # of the position just before: utr_end
            utr_end = int(fields[6])
            if utr_end < exon_starts[0]:
                # Gene name
                gene_name = fields[12]
                warnings.warn(
                    "%s (%s) has no 3'UTR." % (
                        name, gene_name))
                continue
            utr_exon_starts = list(
                takewhile(lambda pos: pos <= utr_end, exon_starts))
            utr_exon_ends = list(
                takewhile(lambda pos: pos <= utr_end, exon_ends))
            if not utr_exon_ends:
                # utr_end is inside the first exon
                utr_exon_ends.append(utr_end)
                assert utr_exon_starts
            elif utr_exon_ends[-1] < utr_exon_starts[-1]:
                utr_exon_ends.append(utr_end)
            # We ignore the exons before the first intron
            # and after the last one because we should never
            # have to take them into account when calculating
            # the genomic coordinate of a position in a 3'UTR
            next_intron_starts = [pos + 1 for pos in utr_exon_ends[:-1]]
            next_intron_ends = [pos - 1 for pos in utr_exon_starts[1:]]
            utr_exon_lengths = list(reversed(lengths_from_starts_ends(
                utr_exon_starts, utr_exon_ends)))
            next_intron_lengths = list(reversed(lengths_from_starts_ends(
                next_intron_starts, next_intron_ends)))
            assert len(next_intron_lengths) + 1 == len(utr_exon_lengths)
            Globals.enst2transcript_info[transcript_id] = (
                chrom,
                strand,
                utr_exon_ends[-1],
                # append a None so that we do not loose
                # the last exon length in the zip
                zip(utr_exon_lengths, next_intron_lengths + [None]))


def utrcoord2genomecoord(transcript_id, start_in_utr, end_in_utr):
    """*transcript_id* is an 'ENST*' Ensembl transcript ID.
    *utr_start* and *utr_end* are the start and end coordinates
    of a seed match relative to the spliced 3'UTR of the transcript."""
    # To convert Family_Info UTR-based coordinates to genomic coordinates,
    # add the length of preceding introns
    try:
        chrom, strand, utr_start, lengths = \
            Globals.enst2transcript_info[transcript_id]
    except KeyError:
        # We haven't encountered a matching refGene transcript.
        # Transmit specific failure info to calling context.
        raise UnknownTranscriptId
    if chrom not in Globals.genomes:
        raise SpecialChromosome
    # Calculate the cumulated length of introns before utr_start
    if lengths is None:
        raise UnknownCDS
    cumul_intron = 0
    cumul_exon = 0
    first_exon_length = lengths[0][0]
    latest_intron_length = 0
    lengths_iter = iter(lengths)
    #try:
    #    lengths_iter = iter(lengths)
    #except TypeError:
    #    raise UnknownCDS
    if start_in_utr > first_exon_length:
        # start is not within the first exon
        # we need to find what intron length
        # needs to be taken into account
        for (ex_len, in_len) in lengths_iter:
            cumul_exon += ex_len
            if cumul_exon >= start_in_utr:
                # genomic start can be calculated given strand, utr_start,
                # cumul_intron (at this stage), and start_in_utr
                latest_intron_length = in_len
                break
            # Only add the intron length if we know that the start
            # will be in a further exon
            try:
                cumul_intron += in_len
            except TypeError:
                assert in_len is None
                msg = CAT([
                    "Not enough exon length (%d) " % cumul_exon,
                    "in %s " % transcript_id,
                    "to find position %d in UTR" % start_in_utr])
                warnings.warn(msg)
                Globals.too_short += 1
                #raise OutsideTranscript
    # Calculate genomic coordinate of the site start,
    # using current cumulated intron length.
    if strand == "+":
        # if start_in_utr is 0-based
        #genomic_start = utr_start + cumul_intron + start_in_utr
        # if start_in_utr is 1-based
        genomic_start = utr_start + (cumul_intron + start_in_utr - 1)
    else:
        genomic_start = utr_start - (cumul_intron + start_in_utr - 1)
    if end_in_utr > max(cumul_exon, first_exon_length):
        # end is in the next exon (or further away, but his is unlikely dealing
        # with seed-matches coordinates)
        # Count the intron length that was not counted
        if latest_intron_length is not None:
            if latest_intron_length:
                # start and end are not in the same exon:
                # We haven't looked to conservation for such sites
                raise AcrossIntron
        try:
            cumul_intron += latest_intron_length
        except TypeError:
            assert latest_intron_length is None
            msg = CAT([
                "Not enough exon length (%d) " % cumul_exon,
                "in %s " % transcript_id,
                "to find position %d in UTR" % end_in_utr])
            warnings.warn(msg)
            Globals.too_short += 1
            #raise OutsideTranscript
        # iterator continues from where it last was used
        for (ex_len, in_len) in lengths_iter:
            cumul_exon += ex_len
            if cumul_exon >= end_in_utr:
                # genomic end can be calculated given strand, utr_start,
                # cumul_intron (at this stage), and end_in_utr
                break
            # Only add the intron length if we know that the end
            # will be in a further exon
            try:
                cumul_intron += in_len
                # start and end are not in the same exon:
                # We haven't looked to conservation for such sites
                raise AcrossIntron
            except TypeError:
                assert in_len is None
                msg = CAT([
                    "Not enough exon length (%d) " % cumul_exon,
                    "in %s " % transcript_id,
                    "to find position %d in UTR" % end_in_utr])
                warnings.warn(msg)
                Globals.too_short += 1
                #raise OutsideTranscript
    # Calculate genomic coordinate of the site end,
    # using current cumulated intron length.
    if strand == "+":
        # if end_in_utr is 0-based
        #genomic_end = utr_start + cumul_intron + end_in_utr
        # if end_in_utr is 1-based
        genomic_end = utr_start + (cumul_intron + end_in_utr - 1)
    else:
        genomic_end = utr_start - (cumul_intron + end_in_utr - 1)
    return chrom, strand, genomic_start, genomic_end



#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################
class Globals(object):
    """This object holds global variables."""
    #@staticmethod
    #def load_dicts(pickle_file_name):
    #    """Use cPickle.load to retrieve the dictionaries."""
    #    with open(pickle_file_name, "rb") as pickle_file:
    #        Globals.mirtax2taxid = load(pickle_file)
    #        Globals.taxid2mirtax = load(pickle_file)
    #        Globals.ucsc2taxid = load(pickle_file)
    #        Globals.taxid2ucsc = load(pickle_file)

    genome2taxid = {
        "hg19" : "9606",
        "mm10" : "10090"}
    # Existing chromosome names
    chromosomes = set()
    # BedTools for fasta files of the chromosomes
    # Some of the chromosomes in Globals.chromosomes
    # may be absent from Globals.genomes
    genomes = {}
    seedtype2length = {
        "7mer-1a" : 7,
        "7mer-m8" : 7,
        "8mer" : 8}
    # key: Genbank NM_* or NR_* transcript ID
    # value: Ensembl transcript ID
    refgene2enst = {}
    # key: Ensembl transcript ID
    # value: quadruplet with the following elements:
    # 1) chromosome
    # 2) strand
    # 3) 1-based coordinate of the start of the 3'UTR
    #    (if strand is "-", 3'UTR is upstream of this, genomic-wise)
    # 4) list of pairs of (exon, following intron) lengths, strand-wise:
    enst2transcript_info = {}
    converted_transcript_ids = 0
    unconverted_transcript_ids = 0



class Options(object):
    """This object contains the values of the global options."""


#################
# main function #
#################

def main():
    """Main function of the program."""
    WRITE("%s\n" % JOIN(" ", sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-f", "--family_info",
        required=True,
        help="'Family_Info' file from TargetScan from which "
        "to make a version with over-conservation status added.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-g", "--genome_dir",
        required=True,
        help="Directory in which the fasta files for the "
        "chromosomes of the genome specified with option "
        "-r reside.")
    parser.add_argument(
        "-e", "--ens2nm",
        # Actually, it seems that TargetScan mouse
        # does not use Ensembl transcript IDs
        #required=True,
        help="Table matching ENST* Ensembl transcript IDs "
        "with RefGene ARNm IDs.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-p", "--phyl_ext",
        required=True,
        help="Table giving the phylogenetic extension of miRNA families.\n"
        "First line is headers.\n"
        "Next lines have three space-separated fields:\n"
        "Seed, "
        "Clade_of_which_seed_is_specific, "
        "Species_outside_that_clade.\n"
        "This last field is a comma-separated list of UCSC genome names "
        "(as in the MAF files).",
        type=argparse.FileType("r"))
    parser.add_argument(
        "--refseq_file",
        required=True,
        help="File formatted as "
        "http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/refGene.txt.gz"
        " (after gunzipping).",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-r", "--ref",
        required=True,
        help="Name (as in the .maf file) of the of the organism used as "
        "reference for the zones to extract in the .bed files. Ex: hg19")
    parser.add_argument(
        "-c", "--chrom",
        help="Name of the chromosome to consider. If this option is set, "
        "all transcripts that come from other chromosmes will be skipped. "
        "This is mainly for testing purposes. Ex: chrY")
    parser.add_argument(
        "-s", "--sites_bed",
        required=True,
        help="Directory where the files in bed format containing "
        "the information about site conservation in chromosomes "
        "are to be looked for.")
    parser.add_argument(
        "-m", "--mir_faminfo",
        required=True,
        help="File from TargetScan giving the "
        "miR Family <-> seed correspondence.",
        type=argparse.FileType("r"))
    #parser.add_argument(
    #    "-m", "--mir_fasta",
    #    required=True,
    #    help="File in .fasta format containing the mature miRNA sequences.",
    #    type=argparse.FileType("r"))
    #parser.add_argument(
    #    "-t", "--mirbase2taxid",
    #    required=True,
    #    help="File from miRBase giving (among other information), "
    #    "the NCBI taxon IDs corresponding to the species codes used "
    #    "in the miRNA names.",
    #    type=argparse.FileType("r"))
    parser.add_argument(
        "-o", "--output_family_info",
        required=True,
        help="Name of the filtered 'Family_Info' file to write.")
    args = parser.parse_args()


    # Read input files.
    ##################################
    # Making a dictionary converting #
    # an Ensembl transcript ID       #
    # to a genbank NM_* or NR_* ID   #
    ##################################
    if args.ens2nm:
        with args.ens2nm as ens2nm_file:
            headers = strip_split(ens2nm_file.readline())
            assert headers[1] == "Ensembl Transcript ID", \
                "Unexpected header structure."
            assert headers[7] == "RefSeq mRNA [e.g. NM_001195597]", \
                "Unexpected header structure."
            assert headers[8] == "RefSeq ncRNA [e.g. NR_002834]", \
                "Unexpected header structure."
            #assert headers[0] == "stable_id", \
            #        "Unexpected column name %s" % headers[0]
            #assert headers[1] == "display_label", \
            #        "Unexpected column name %s" % headers[1]
            ## This will enable matching data from TargetScan Family_Info
            ## with RefGene intron-exon boundaries information
            #Globals.refgene2enst = dict(imap(
            #    reversed, imap(strip_split_de_ext, ens2nm_file)))
            # Use compress to ignore some fields
            for fields in imap(strip_split, ens2nm_file):
                if fields[7]:
                    assert not fields[8]
                    Globals.refgene2enst[fields[7]] = fields[1]
                if fields[8]:
                    assert not fields[7]
                    Globals.refgene2enst[fields[8]] = fields[1]
    else:
        def identity(key):
            return key

    ######################################
    # Making a dictionary storing        #
    # phylogenetic extension information #
    # for miRNA families (seeds)         #
    ######################################
    with args.phyl_ext as phyl_ext_file:
        headers = space_strip_split(phyl_ext_file.readline())
        assert headers[0] == "Seed", \
                "Unexpected column name %s" % headers[0]
        assert headers[1] == "Clade_of_which_seed_is_specific", \
                "Unexpected column name %s" % headers[1]
        assert headers[2] == "Species_outside_that_clade", \
                "Unexpected column name %s" % headers[2]
        # Use compress to ignore second field
        filters = repeat([1, 0, 1])
        Globals.seed2outgroups = dict((seed, set(split(outgroups, ","))) for (
                seed,
                outgroups) in starmap(
                    compress,
                    izip(
                        imap(space_strip_split, phyl_ext_file),
                        filters)))

        #for (seed, outgroups) in Globals.seed2outgroups.iteritems():
        #    print seed, len(outgroups)
    with args.refseq_file as refseq_file:
        if args.ens2nm:
            refgene2enst = Globals.refgene2enst.get
        else:
            refgene2enst = identity
        get_transcript_info(
            refseq_file,
            refgene2enst,
            only_chrom=args.chrom)
    msg = "\n".join([
        "%d genbank transcript IDs converted." % \
            Globals.converted_transcript_ids,
        "%d genbank transcript IDs not converted." % \
            Globals.unconverted_transcript_ids])
    warnings.warn(msg)

    # Open genome
    if args.chrom:
        chrom_list = [args.chrom]
    else:
        chrom_list = sorted(Globals.chromosomes)
    for chrom in chrom_list:
        try:
            Globals.genomes[chrom] = BT(OPJ(args.genome_dir, "%s.fa" % chrom))
        except ValueError:
            warnings.warn("Problem for chromosome %s" % chrom)
            continue

    ##
    # Just for checking
    ##
    #from Bio import SeqIO
    #mir2seeds = defaultdict(set)
    #with args.mir_fasta as mirbase_file:
    #    for record in SeqIO.parse(mirbase_file, "fasta"):
    #        seq_name = split(record.name)[0]
    #        if not seq_name:
    #            seq_name = split(record.id)[0]
    #        #mir_name = JOIN("-", split(seq_name, "-")[1:])
    #        seed = to_dna(str(record.seq.upper())[1:7])
    #        #mir2seeds[mir_name].add(seed)
    #        mir2seeds[seq_name].add(seed)
    ## Transform the dictionary of sets in a dictionary of single items
    #for (mir_name, seeds) in mir2seeds.iteritems():
    #    if len(seeds) != 1:
    #        warnings.warn("There are several seeds for %s!\n%s" % (
    #            mir_name,
    #            JOIN(", ", sorted(seeds))))
    #        sys.exit(1)
    #    # http://stackoverflow.com/questions/1619514/how-to-extract-the-member-from-single-member-set-in-python
    #    (mir2seeds[mir_name], ) = seeds
    #    # Seems to work even with a defaultdict...
    ## If we just use the "mir_name" part above, we notice many cases
    ## where miRNAs with the same name (excluding species prefix) have
    ## several possible seeds.
    ## To be able to match information from Family_Info and miRBase,
    ## we will have have to prepend the miRBase species code to the
    ## "miR Family" field.
    ## We can use the NCBI taxon ID provided in Family_Info and the
    ## information from the "organisms.txt" miRBase file to obtain
    ## the species code.
    #with args.mirbase2taxid as taxid_file:
    #    headers = strip_split(taxid_file.readline())
    #    assert headers[0] == "#organism", "Unexpected header structure."
    #    assert headers[4] == "#NCBI-taxid", "Unexpected header structure."
    #    # Use compress to ignore some fields
    #    filters = repeat([1, 0, 0, 0, 1])
    #    Globals.taxid2mirbase = dict((taxid, species) for (
    #            species,
    #            taxid) in starmap(
    #                compress,
    #                izip(
    #                    imap(strip_split, taxid_file),
    #                    filters)))
    with args.mir_faminfo as faminfo_file:
        headers = strip_split(faminfo_file.readline())
        assert headers[0] == "miR family", "Unexpected header structure."
        assert headers[1] == "Seed+m8", "Unexpected header structure."
        assert headers[2] == "Species ID", "Unexpected header structure."
        # Use compress to ignore some fields
        filters = repeat([1, 1, 1, 0, 0, 0, 0])
        # We use the (mir_name, taxid) pair as key because
        # the seed may differ between species for a same miR family name
        mir2seed = dict(((mir_name, taxid), to_dna(seed)[:-1]) for (
                mir_name,
                seed,
                taxid) in starmap(
                    compress,
                    izip(
                        imap(strip_split, faminfo_file),
                        filters)))

    # Generate seed list
    #seeds = {}
    #seeds[6] = imap(CAT, product("ACGT", repeat=6))
    #seeds[7] = imap(CAT, product("ACGT", repeat=7))
    conservation_info = {}
    #for seed in imap(CAT, product("ACGT", repeat=6)):
    #    conservation_info[seed] = BT(OPJ(
    #        args.sites_db_dir,
    #        "%s.txt" % seed)).tabix()
    for chrom in chrom_list:
        try:
            bed_file = OPJ(
                args.sites_bed,
                "%s_6-mer_conservation.bed.gz" % chrom)
            fetch_from_tabix = TabixFile(bed_file).fetch
            def fetch(reference, start, end):
                """Function to fetch bed entries from a tabix-indexed file."""
                try:
                    return fetch_from_tabix(reference, start, end)
                except ValueError:
                    return []
            conservation_info[chrom] = fetch
        except ValueError:
            warnings.warn("No conservation bed file for chromosome %s" % chrom)
            continue

    #if OPF("%s.bgz" % args.sites_bed):
    #    tabix_file = TabixFile("%s.bgz" % args.sites_bed)
    #elif OPF("%s.gz" % args.sites_bed):
    #    tabix_file = TabixFile("%s.gz" % args.sites_bed)
    #else:
    #    tabix_file = TabixFile(args.sites_bed)
    ##
    #fetch_from_tabix = tabix_file.fetch
    ##annotated_refs = tabix_file.contigs
    #def fetch(reference, start, end):
    #    try:
    #        return fetch_from_tabix(reference, start, end)
    #    except ValueError:
    #        return []


    ref_taxid = Globals.genome2taxid[args.ref]
    unknown_id = 0
    special_chromosome = 0
    unknown_cds = 0
    Globals.too_short = 0
    #too_short = 0
    no_fail = 0
    no_seed_check = 0
    no_match = 0
    across_intron = 0
    with args.family_info as family_info_file, \
            open(args.output_family_info, "w") as outfile:
        headers = strip_split(family_info_file.readline())
        assert headers[0] == "miR Family", "Unexpected header structure."
        assert headers[3] == "Transcript ID", "Unexpected header structure."
        assert headers[4] == "Species ID", "Unexpected header structure."
        assert headers[5] == "UTR start", "Unexpected header structure."
        assert headers[6] == "UTR end", "Unexpected header structure."
        assert headers[9] == "Seed match", "Unexpected header structure."
        assert headers[10] == "PCT", "Unexpected header structure."
        for fields in imap(strip_split, family_info_file):
            taxid = fields[4]
            if taxid != ref_taxid:
                # We cannot easily deal with species other than the reference
                # one, because we have the conservation information with
                # reference to the position in the reference species only.
                # TODO: One day we could try to use information from MAF files
                # to handle the other species.
                continue
            match_type = fields[9]
            if match_type not in Globals.seedtype2length:
                # We haven't looked for conservation at sites
                # that are not exact seed matches.
                continue
            transcript_id = remove_ext(fields[3])
            start_in_utr = int(fields[5])
            end_in_utr = int(fields[6])
            match_length = 1 + end_in_utr - start_in_utr
            try:
                if match_type == "8mer":
                    assert match_length == 8
                else:
                    assert match_length == 7
            except AssertionError:
                msg = CAT([
                    "Site length (%d) " % match_length,
                    "for %s " % transcript_id,
                    "is not correct for a %s" % match_type])
                warnings.warn(msg)
                sys.exit(1)
            printline = False
            try:
                chrom, strand, start, end = utrcoord2genomecoord(
                    transcript_id, start_in_utr, end_in_utr)
                if chrom not in chrom_list:
                    printline = False
                else:
                    printline = True
                    # Determine the appropriate over-conservation result file
                    # in args.sites using the fasta genome and the
                    # genomic coordinates of the site to find the seed.
                    if match_type == "8mer" or match_type == "7mer-m8": 
                        # We only look at the matches to the 2-7 miRNA position
                        # For 8mer and 7mer-m8 match types
                        # we need to remove the position matching
                        # the 8-th miRNA position.
                        # This is the first site position UTR-wise.
                        if strand == "+":
                            start += 1
                        else:
                            start -= 1
                    if match_type == "8mer" or match_type == "7mer-1a": 
                        # We only look at the matches to the 2-7 miRNA position
                        # For 8mer and 7mer-1a match types
                        # we need to remove the position in front of the
                        # first miRNA position.
                        # This is the last site position UTR-wise.
                        if strand == "+":
                            end -= 1
                        else:
                            end += 1
                    # Switch start and end if necessary
                    if strand == "-":
                        start, end = end, start
                    position = "%s:%s-%s:%s" % (chrom, start, end, strand)
                    #bed_match = Interval(chrom, start - 1, end)
                    site_seq = UPPER(BT.seq(
                        position[:-2],
                        Globals.genomes[chrom]))
                    if strand == "+":
                        seed = dna_rev_compl(site_seq)
                    else:
                        seed = site_seq
                    if len(seed) != 6:
                        chrom, strand, utr_start, lengths = \
                            Globals.enst2transcript_info[transcript_id]
                        warnings.warn("Seed is does not have length 6.")
                        print position
                        print lengths
                        print utr_start
                        print match_type
                        print "start_in_utr: %d" % start_in_utr
                        print "end_in_utr: %d" % end_in_utr
                        sys.exit(1)
                    mir_family = fields[0]
                    #mir_name = fields[0]
                    #assert seed in mir2seeds[mir_name]
                    #mir_name = "%s-%s" % (Globals.taxid2mirbase[taxid], fields[0])
                    # Checking that seed match is compatible
                    # with the documented seed.
                    if (mir_family, taxid) not in mir2seed:
                        # The seed is not documented.
                        # We will continue, but append "_unchecked"
                        # to the overconservation flag.
                        msg = CAT([
                            "Site sequence (%s) inferred from " % site_seq,
                            "site coordinates (%s) " % position,
                            "cannot be compared with the seed.\n",
                            "%s has no %s for species %s" % (
                                args.mir_faminfo, mir_family, taxid)])
                        warnings.warn(msg)
                        no_check = "_unchecked"
                        no_seed_check += 1
                    else:
                        try:
                            assert seed == mir2seed[(mir_family, taxid)]
                            no_check = ""
                        except AssertionError:
                            #print "position: %s" % position
                            #print "site_seq: %s" % site_seq
                            #print "match_type (strand): %s (%s)" % (match_type, strand)
                            #print "inferred seed:", seed
                            #print "Real seed for %s: %s" % (
                            #    mir_family,
                            #    mir2seed[(mir_family, taxid)])
                            msg = CAT([
                                "Site sequence (%s) inferred from " % site_seq,
                                "site coordinates (%s) " % position,
                                "doesn't match seed (%s)." % mir2seed[(
                                    mir_family,
                                    taxid)]])
                            warnings.warn(msg)
                            raise NonMatchingSeed
                    # Can we use pybedtools to make this fast ?
                    # Maybe: The field with genome names in which a
                    # site is conserved is stored in the score attribute
                    # of each bed element.
                    # The seed itself is in the name attribute
                    # (but it is not very useful).
                    # Open seed-match conservation info
                    # TODO: Can we do this once for all seeds
                    # and store the tabixes in a dictionary ?
                    #conservation = BT(OPJ(
                    #    args.sites_db_dir, "%s.txt" % seed)).tabix()
                    #conservation = conservation_info[seed]
                    #hits = conservation.all_hits(bed_match)
                    #hits = []
                    #for hit in conservation.tabix_intervals(bed_match):
                    #    if (hit.start == start - 1) and (hit.end == end):
                    #        hits.append(hit)
                    #try:
                    #    assert len(hits) == 1
                    #except AssertionError:
                    #    # Somehow, several identical lines may have appeared
                    #    # in one of the files
                    #    genome_sets = [set(split(
                    #        hit.score,
                    #        ",")) for hit in hits]
                    #    genome_names = genome_sets[0]
                    #    assert genome_names == reduce(or_, genome_sets)
                    #    assert genome_names == reduce(and_, genome_sets)
                    #    #print position
                    #    #print JOIN("\n", map(str, hits))
                    #    #sys.exit(1)
                    # Get the list of genome names
                    # in which the seed match was conserved
                    #genome_names = set(split(hits[0].score, ","))
                    genome_sets = []
                    #check_hits = []
                    fetch = conservation_info[chrom]
                    for hit in imap(split, fetch(chrom, start - 1, end)):
                        #if (hit[1] == start - 1) and (hit[2] == end)):
                        #if hit[1:3] == [
                        #    str(start - 1),
                        #    str(end)] and hit[3] == seed:
                        if hit[1:3] == [
                            str(start - 1),
                            str(end)] and hit[3] == site_seq:
                            #if (
                            #    strand == "-" and hit[3] != seed) or (
                            #        strand == "+" and dna_rev_compl(
                            #            hit[3]) != seed):
                            #    #msg = JOIN("\n", [
                            #    #    "hit: %s" % JOIN("\t", hit),
                            #    #    "position: %s:%d-%d:%s" % (
                            #    #        chrom,
                            #    #        start,
                            #    #        end,
                            #    #        strand),
                            #    #    "seed: %s" % seed,
                            #    #    "site_seq: %s" % site_seq])
                            #    #warnings.warn(msg)
                            #    check_hits.append(hit)
                            #    #sys.exit(1)
                            genome_sets.append(set(split(hit[4], ",")))
                    genome_names = genome_sets[0]
                    #if len(check_hits) > 1:
                    #    print JOIN("\n", map(str, check_hits))
                    #    assert genome_names == reduce(or_, genome_sets)
                    #    assert genome_names == reduce(and_, genome_sets)
                    #    sys.exit(1)
                    #else:
                    if len(genome_sets) != 1:
                        #print JOIN("\n", map(str, check_hits))
                        print JOIN("\n", map(str, genome_sets))
                        msg = JOIN("\n", [
                            "position: %s:%d-%d:%s" % (
                                chrom,
                                start,
                                end,
                                strand),
                            "seed: %s" % seed,
                            "site_seq: %s" % site_seq])
                        warnings.warn(msg)
                        sys.exit(1)
                        #assert len(genome_sets) == 1
                    # Determine over_conservation by comparing info from
                    # Globals.seed2outgroups and the appropriate
                    # over-conservation result file:
                    # intersect outgroups from the first
                    # with genome names from the second
                    try:
                        if genome_names & Globals.seed2outgroups[seed]:
                            # The seed match was found in an outgroup
                            # to the clade having the miRNA
                            over_conservation = "yes%s" % no_check
                        else:
                            over_conservation = "no%s" % no_check
                    except KeyError:
                        # The seed was not found in Globals.seed2outgroups
                        over_conservation = "unk_phylext"
                    no_fail += 1
            except NonMatchingSeed:
                over_conservation = "unk_coords"
                position = "no_match"
                no_match += 1
            except UnknownTranscriptId:
                over_conservation = "unk_coords"
                position = "ID_unk"
                unknown_id += 1
            except SpecialChromosome:
                over_conservation = "unk_coords"
                position = "special_chromosome"
                special_chromosome += 1
            except UnknownCDS:
                over_conservation = "unk_coords"
                position = "CDS_unk"
                unknown_cds += 1
            except OutsideTranscript:
                over_conservation = "unk_coords"
                position = "too_short"
                #too_short += 1
            except AcrossIntron:
                over_conservation = "unk_coords"
                position = "across_intron"
                across_intron += 1
            if printline:
                outfile.write(
                    "%s\n" % JOIN("\t", fields + [over_conservation, position]))

    msg = JOIN("\n", [
        "\nunknown_id: %d" % unknown_id,
        "special_chromosome: %d" % special_chromosome,
        "unknown_cds: %d" % unknown_cds,
        "too_short: %d" % Globals.too_short,
        "no_seed_check: %d" % no_seed_check,
        "no_match: %d" % no_match,
        "across_intron: %d" % across_intron,
        "success: %d" % no_fail])
    warnings.warn(msg)
    return 0

if __name__ == "__main__":
    sys.exit(main())
