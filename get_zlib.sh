#!/bin/bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="1.2.8"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf zlib-${version}*
# Download it
wget http://zlib.net/zlib-${version}.tar.gz || error_exit "zlib download failed"
tar -xvzf zlib-${version}.tar.gz
cd zlib-${version}
#./configure
# Trying to optimize
CFLAGS="-O3 -march=native -fomit-frame-pointer" ./configure || error_exit "zlib config failed"
make test || error_exit "zlib build failed"
make install prefix=${HOME} || error_exit "zlib install failed"

exit 0
