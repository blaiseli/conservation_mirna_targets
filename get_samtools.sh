#!/bin/bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="samtools"
version="1.2"
htslib_version="1.2.1"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf samtools-${version}*
#wget http://downloads.sourceforge.net/project/samtools/samtools/${version}/samtools-${version}.tar.bz2 || error_exit "samtools download failed"
wget --continue https://github.com/${program}/${program}/releases/download/${version}/${program}-${version}.tar.bz2 || error_exit "samtools download failed"
tar -xvjf samtools-${version}.tar.bz2
cd samtools-${version}/
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
#sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
#sed -i 's|^LDFLAGS\(.*\)$|LDFLAGS\1 -Wl,-rpath,${HOME}/lib|g' Makefile
#sed -i 's|^LDLIBS\(.*\)$|LDLIBS\1 -L${HOME}/lib|g' Makefile
#make CFLAGS="-march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include -I" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "samtools build failed"
make || error_exit "samtools build failed"
# Install in home directory
make prefix=${HOME} install || error_exit "samtools install failed"
#cp -f htslib-${htslib_version}/{bgzip,tabix} ${HOME}/bin
# https://github.com/cole-trapnell-lab/cufflinks
cp libbam.a ${HOME}/lib/. || error_exit "samtools library install failed"
mkdir -p ${HOME}/include/bam
cp *.h ${HOME}/include/bam/. || error_exit "samtools headers install failed"

cd htslib-${htslib_version}
#sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
#sed -i 's|^LDFLAGS\(.*\)$|LDFLAGS\1 -Wl,-rpath,${HOME}/lib|g' Makefile
#sed -i 's|^LDLIBS\(.*\)$|LDLIBS\1 -L${HOME}/lib|g' Makefile
#make CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" || error_exit "htslib build failed"
#make CFLAGS="-march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "htslib build failed"
make || error_exit "htslib build failed"
## Install in home directory
make prefix=${HOME} install || error_exit "htslib install failed"
cd ..

exit 0
