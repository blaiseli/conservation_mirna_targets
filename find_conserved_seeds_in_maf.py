#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""
This script will read a maf-formatted genome alignment and a file
containing bed-formatted coordinates of miRNA seeds in a reference sequence
(reference that should be present in the alignment). For each seed, and for
each seed location, it will determine the list of species in which the seed is
conserved with respect to the reference and add the information to a database.

The database should have been created beforehand using
`get_miRBase_seed_info.py`.
"""

import argparse
import sys
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


#import copy
import os
OPJ = os.path.join
OPE = os.path.exists
OPB = os.path.basename
from os.path import splitext
from re import sub
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write

#maf_path = "/home/bli/src/alignio-maf"
#sys.path.insert(1, maf_path)
try:
    #from Bio import AlignIO
    from Bio.AlignIO.MafIO import MafIndex
except ImportError:
    sys.stderr.write(
        "This script requires the presence of the MafIO "
        "Biopython extra module\n"
        "(http://biopython.org/wiki/Multiple_Alignment_Format)\n")
    sys.exit(1)

#try:
#    from seed2num import make_seeds
#except ImportError:
#    sys.stderr.write(
#        "This script requires the presence of the seed2num module.\n")
#    sys.exit(1)


from operator import attrgetter
#from operator import or_
#
#from bisect import insort
#from bisect import bisect_left
from itertools import ifilter, imap, izip, product, repeat
#from itertools import ifilterfalse
#import pysam
#
#from Bio.Seq import reverse_complement
from sqlite3 import connect, IntegrityError, OperationalError
# To get back dictionaries that convert between codes and NCBI taxonomy IDs
from cPickle import load

from string import maketrans, Template
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")


def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]

# Query Seed Taxa
QST = Template("select taxid from_miRBase from seed_taxa where seed == \"$seed\" and from_miRBase == 1").substitute

#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################
class Globals(object):
    """This object holds global variables."""
    @staticmethod
    def load_dicts(pickle_file_name):
        """Use cPickle.load to retrieve the dictionaries."""
        with open(pickle_file_name, "rb") as pickle_file:
            Globals.mirtax2taxid = load(pickle_file)
            Globals.taxid2mirtax = load(pickle_file)
            Globals.ucsc2taxid = load(pickle_file)
            Globals.taxid2ucsc = load(pickle_file)


class Options(object):
    """This object contains the values of the global options."""
    bed_file = None
    maf_file = None
    ref = None


#################
# main function #
#################

def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-m", "--maf",
        required=True,
        help="Alignemnt file in .maf format from which sub-alignments "
        "will be extracted. Its name should be in the form "
        "<ref_chromosome>.maf, where <ref_chromosome> is a chromosome name "
        "in the reference sequence.")
    parser.add_argument(
        "-b", "--bed",
        required=True,
        help="Space-separated list of files in .bed format indicating the "
        "coordinates (in the reference organism) of the zones to extract.")
    parser.add_argument(
        "-r", "--ref",
        required=True,
        help="Name (as in the .maf file) of the of the organism used as "
        "reference for the zones to extract in the .bed files. Ex: hg19")
    parser.add_argument(
        "-l", "--seed_length",
        type=int,
        default=7,
        help="Length of the seeds.")
    parser.add_argument(
        "-d", "--db_dir",
        required=True,
        help="Name of the directory in which to write the files containing "
        "sites information.")
    parser.add_argument(
        "-s", "--database",
        required=True,
        help="Name of the sqlite file in which to store the site "
        "information in the form of a database. "
        "This database should have been prepared previously "
        "using get_miRBase_seed_info.py")
    parser.add_argument(
        "-w", "--write_db",
        action="store_true",
        help="Set this option to write information in the sql database.")
    args = parser.parse_args()
    Options.bed_file = args.bed
    Options.maf_file = args.maf
    Options.ref = args.ref

    # To avoid repeatedly calling these as string methods:
    upper = str.upper
    strip = str.rstrip
    split = str.split
    # To join the letters of the seeds
    cat = "".join
    # Generate seed list
    seeds = imap(cat, product("ACGT", repeat=args.seed_length))


    def strip_split(text):
        """Strips trailing blank characters and then splits arount tabs."""
        return split(strip(text), "\t")
    

    # This is unclean:
    #chrom = ".".join(OPB(Options.maf_file).split(".")[:-1])
    #chrom_name, ext = splitext(OPB(Options.maf_file))
    chrom_name, ext = splitext(OPB(Options.maf_file))
    if ext == ".bgz":
        msg = "".join([
            "Possibility removed because there were problems in "
            "offset calculation in MafIO.py."])
        raise NotImplementedError(msg)
        #chrom_name, ext = splitext(chrom_name)
        #Options.maf_file = splitext(Options.maf_file)[0]
    elif ext == ".gz":
        chrom_name, ext = splitext(chrom_name)
        Options.maf_file = splitext(Options.maf_file)[0]
    mafindex = "%s.mafindex" % splitext(Options.maf_file)[0]
    assert ext == ".maf"
    ref_chrom = ".".join([Options.ref, chrom_name])
    WRITE("Will search for seeds in %s\n" % ref_chrom)
    WRITE(
        "Loading (building if necessary) mafindex "
        "for %s...\n" % Options.maf_file)
    idx = MafIndex(
        mafindex,
        Options.maf_file, ref_chrom)
    WRITE("Index loaded.\n")

    WRITE("Loading taxon and genomes conversion dictionaries.\n")
    # Loading dictionaries pickled b yget_miRBase_seed_info.py
    Globals.load_dicts("%s.pickle" % splitext(args.database)[0])

    # Needed to get seed information
    WRITE("Connecting to sqlite3 database %s.\n" % args.database)
    site_db = connect(args.database)
    if args.write_db:
        # Preparing tables concerning sites if needed
        WRITE("Ensuring tables seed_sites and site_taxa exist.\n")
        table_checked = False
        while not table_checked:
            # Table associating sites to a seed
            # Only one seed per site, but a seed may have many sites
            try:
                site_db.execute(
                    "create table if not exists seed_sites "
                    "(seed text, site text primary key, "
                    "foreign key(seed) references seeds(seed))")
                table_checked = True
            except OperationalError as e:
                warnings.warn("Error: %s\nRetrying.\n" % e)
                table_checked = False
        table_checked = False
        while not table_checked:
            # Table associating taxa to sites.
            # A site may be found in various taxa.
            # A taxon may have various sites.
            try:
                site_db.execute(
                    "create table if not exists site_taxa "
                    "(site text, taxid text, primary key (site, taxid), "
                    "foreign key(site) references seed_sites(site), "
                    "foreign key(taxid) references taxa(taxid))")
                table_checked = True
            except OperationalError as e:
                warnings.warn("Error: %s\nRetrying.\n" % e)
                table_checked = False

    id_getter = attrgetter("id")
    seq_getter = attrgetter("seq")

    def has_ref_chrom(fields):
        """Tells if a given tuple has its first element equal to the reference
        chromosome"""
        return fields[0] == chrom_name

    def is_ref(seq_record):
        """Tells if the id attribute of a *seq_record* is ref_chrom."""
        return id_getter(seq_record) == ref_chrom

    def get_upper_seq(seq_record):
        """Returns the uppercase version of the seq attribute
        of *seq_record*."""
        return upper(str(seq_getter(seq_record)))

    #def get_id_and_seq(seq_record):
    #    """Returns the id attribute uppercase version
    #    of the seq attribute of *seq_record*."""
    #    return (id_getter(seq_record), get_upper_seq(seq_record))
    #
    #def get_genome_code_and_seq(seq_recor):
    #    """Returns the genome code and uppercase version
    #    of the seq attribute of *seq_record*."""
    #    return (split(id_getter(seq_rec), ".")[0], get_upper_seq(seq_record))

    WRITE("Parsing bed file.\n")
    # Storing values to later write in the database (for efficiency)
    seed_sites = set()
    site_taxa = set()
    # TODO: check there are no problems with seed strandedness
    with open(Options.bed_file, "r") as bed_file:
        ucsc2taxid = Globals.ucsc2taxid.get
        taxid2mirtax = Globals.taxid2mirtax.get
        #ref_id = ucsc2taxid(Options.ref)
        for seed in seeds:
            # Initialize file for the seed if it does not exist yet
            if not OPE(OPJ(args.db_dir, "%s.txt" % seed)):
                #seed_taxa = []
                #results = site_db.execute(QST(seed=seed)).fetchall()
                #for result in results:
                #    mirtax = taxid2mirtax(str(result[0]))
                #    print result[0], mirtax
                #    seed_taxa.append(mirtax)
                #seed_taxa = sorted(seed_taxa)
                seed_taxa = sorted(map(
                    taxid2mirtax,
                    [str(result[0]) for result in site_db.execute(
                        QST(seed=seed)).fetchall()]))
                with open(OPJ(args.db_dir, "%s.txt" % seed), "w") as site_info_file:
                    site_info_file.write("%s\t%s\n" % (seed, ",".join(seed_taxa)))
        # Used to avoid recalculaions when bed is well sorted:
        prev_seed = None
        # Open a dummy first results file, because we will have to close one.
        site_info_file = open("/dev/null", "w")
        # ifilter may be useless since we work with
        # per-reference chromosome bed files.
        for (chrom, start, end, seed, score, strand) in ifilter(
            has_ref_chrom, imap(strip_split, bed_file)):
            assert ref_chrom == "%s.%s" % (Options.ref, chrom)
            # Multiple sequence alignments
            #msas = idx.search([start], [end])
            # bed strands are "+"/"-", biopython strands are 1/-1
            msas = idx.get_spliced(
                [int(start)],
                [int(end)-1],
                strand=int(strand + "1"))
            # Take the alignment line for the reference
            #gapped_seed = get_upper_seq(
            #    list(ifilter(is_ref, iter(msas)))[0])
            gapped_seed = get_upper_seq(
                next(ifilter(is_ref, iter(msas))))
            # This piece of code could be removed if we were certain of the
            # consistency between maf and hg19.
            if seed != prev_seed:
                # Update ungapped_seed (removing indels from the sequence
                # obtained in the .maf file) and check coherence
                # with the seed read in the bed file.
                ungapped_seed = sub("-", "", gapped_seed)
                #msg = "%s != %s\nIs the strand always \"+\" for %s?\n" % (
                #    ungapped_seed,
                #    seed,
                #    ref_chrom)
                #assert ungapped_seed == seed, msg
                if ungapped_seed != seed:
                    msg = "ungapped_seed: %s\nbed line:\n%s\n" % (
                        ungapped_seed,
                        "\t".join((chrom, start, end, seed, score, strand)))
                    warnings.warn(msg)
                #assert ungapped_seed == seed, "%s != %s" % (ungapped_seed, seed)
                prev_seed = seed
                # Change current results file because we changes seed
                site_info_file.close()
                # Open in append mode.
                # We do not want to erase last result put in the file
                site_info_file = open(OPJ(args.db_dir, "%s.txt" % seed), "a")
            # TODO: This might be optimizable using more itertools
            # (and skipping assertion)
            #genomes = set([])
            #for seq_rec in iter(msas):
            #    the_seq = get_upper_seq(seq_rec)
            #    if the_seq != gapped_seed:
            #        assert id_getter(seq_rec) != ref_chrom
            #        #if id_getter(seq_rec) == ref_chrom:
            #        #    print the_seq, seed, gapped_seed
            #    else:
            #        genomes.add(split(id_getter(seq_rec), ".")[0])
            genomes = {split(identifier, ".")[0] for identifier in {id_getter(seq_rec) for seq_rec in iter(msas) if get_upper_seq(seq_rec) == gapped_seed}}
            assert Options.ref in genomes
            #taxids = {ucsc2taxid(split(identifier, ".")[0]) for identifier in {
            #    id_getter(seq_rec) for seq_rec in msas if get_upper_seq(seq_rec) == gapped_seed}}
            #assert ref_id in taxids
            site_key = "%s:%s..%d:%s" % (chrom, start, int(end) - 1, strand)
            seed_sites.add((seed, site_key))
            site_taxa |= set(izip(repeat(site_key), imap(ucsc2taxid, genomes)))
            site_info_file.write("%s\n" % "\t".join(
                imap(str, [chrom, start, end, seed, ",".join(sorted(genomes))])))
        # Close last open results file
        site_info_file.close()
    if args.write_db:
        WRITE("Inserting %d sites in table `seed_sites`\n" % len(seed_sites))
        table_written = False
        while not table_written:
            try:
                site_db.executemany(
                    "insert or replace into seed_sites(seed, site) values (?, ?)",
                    seed_sites)
                table_written = True
            except OperationalError as e:
                warnings.warn("Error: %s\nRetrying.\n" % e)
                table_written = False
        WRITE("Inserting %d relations in table `site_taxa`\n" % len(site_taxa))
        table_written = False
        while not table_written:
            try:
                site_db.executemany(
                    "insert or replace into site_taxa(site, taxid) values (?, ?)",
                    site_taxa)
                table_written = True
            except OperationalError as e:
                warnings.warn("Error: %s\nRetrying.\n" % e)
                table_written = False
        table_written = False
        while not table_written:
            try:
                site_db.commit()
                table_written = True
            except OperationalError as e:
                warnings.warn("Error: %s\nRetrying.\n" % e)
                table_written = False
    site_db.close()
    # In case we need more efficiency, might be using and integer
    # instead of a string helps database indexing
    #num2seed, seed2num = make_seeds(seed_length=7)

    return 0

if __name__ == "__main__":
    sys.exit(main())
