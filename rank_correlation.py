#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""

This script performs a rank correlation test between data taken from two files.

The items will be matched ased on the name present in the first column, and the
ranks will be based on the values present in the second column.

Items not present in both files will be ignored.

"""

import argparse
import sys
import warnings
from string import split, strip
from collections import defaultdict
from numpy import asarray
from scipy.stats import pearsonr, spearmanr

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

WRITE = sys.stdout.write

#################
# main function #
#################

def main():
    """Main function of the program."""
    #WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #parser.add_argument(
    #    "-d", "--domain",
    #    required=True,
    #    help="Name of the file containing the domain "
    #    "within which to perform the test.\n"
    #    "The file should contain one element per line.")
    parser.add_argument(
        "file1",
        help="File containing one name per line, "
        "to be used as first set of elements.")
    parser.add_argument(
        "file2",
        help="File containing one name per line, "
        "to be used as second set of elements.")
    args = parser.parse_args()
    
    data = defaultdict(lambda : [None, None])
    #with open(args.domain) as file0:
    #    domain = set(map(strip, file0))
    with open(args.file1) as file1:
        for line in file1:
            fields = split(strip(line), "\t")
            if fields[0][0] != "#":
                data[fields[0]][0] = float(fields[1])
    with open(args.file2) as file2:
        for line in file2:
            fields = split(strip(line), "\t")
            if fields[0][0] != "#":
                data[fields[0]][1] = float(fields[1])
    #list_of_pairs = [(val1, val2) for val1, val2 in data.itervalues() if (val1 is not None and val2 is not None)]
    #print "\n".join(map(str, list_of_pairs))
    #values = zip(*list_of_pairs)
    #print "\n".join(map(str, values[0]))
    #print "\n".join(map(str, values[1]))
    values = map(
        asarray,
        zip(*[(val1, val2) for (val1, val2) in data.values() if (
            val1 is not None and val2 is not None)]))
    scorrelation, sp_value = spearmanr(*values)
    pcorrelation, pp_value = pearsonr(*values)
    #correlation, p_value = spearmanr(*map(
    #    asarray,
    #    zip(*[(val1, val2) for (val1, val2) in data.values() if (
    #        val1 is not None and val2 is not None)])))
    WRITE("spearman rank correlation: %f\t(p-value: %f)\n" % (scorrelation, sp_value))
    WRITE("pearson correlation: %f\t(p-value: %f)\n" % (pcorrelation, pp_value))
    return 0

if __name__ == "__main__":
    sys.exit(main())
