#!/usr/bin/env bash
# Depends on python3 setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="snakemake"
version="3.4.1"

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}-${version}*
wget --continue https://pypi.python.org/packages/source/s/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python3 setup.py build || error_exit "${program} build failed"
python3 setup.py install --user || error_exit "${program} install failed"

exit 0
