#!/usr/bin/env bash
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

# apt-get install python-numpy python-qt4 python-lxml

version="0.9.4"
program="xlrd"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/x/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"

exit 0
