#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="phyml"
alt_program="PhyML"
version="3.1"

mkdir -p ${HOME}/src
cd ${HOME}/src

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${alt_program}-${version}*
wget --continue http://www.atgc-montpellier.fr/download/binaries/${program}/${alt_program}-${version}.zip || error_exit "${program} download failed"
unzip ${alt_program}-${version}.zip || error_exit "${program} unpacking failed"
ln -sf ${PWD}/${alt_program}-${version}/${alt_program}-${version}_linux64 ${HOME}/bin/${program} || "${program} install failed"


exit 0
