#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src

for prog in "bpp-core" "bpp-seq" "bpp-phyl" "bpp-popgen" "bppsuite"
do
    cd ${HOME}/src
    if [ ! -e ${prog} ]
    then
        git clone http://biopp.univ-montp2.fr/git/${prog} || error_exit "${prog} download failed"
        cd ${prog}
        sed -i 's/^SET(CMAKE_CXX_FLAGS "-Wall/SET(CMAKE_CXX_FLAGS "-march=native -Wall/' CMakeLists.txt
        cmake -DCMAKE_INSTALL_PREFIX=${HOME} -DCMAKE_LIBRARY_PATH=${HOME}/lib -DCMAKE_INCLUDE_PATH=${HOME}/include -DBUILD_TESTING=FALSE ./ 
    else
        cd ${prog}
        git pull || error_exit "${prog} update failed"
        sed -i 's/^SET(CMAKE_CXX_FLAGS "-Wall/SET(CMAKE_CXX_FLAGS "-march=native -Wall/' CMakeLists.txt
        cmake ./
    fi
    #make clean
    make || error_exit "${prog} build failed"
    make install || error_exit "${prog} install failed"
done

#export CPATH=$CPATH:${HOME}/include    # for compilation
#export LIBRARY_PATH=$LIBRARY_PATH:${HOME}/lib    # for compilation
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${HOME}  # for execution

exit 0
