#!/bin/bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="EMBOSS"
version="6.6.0"


mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}-${version}*
wget ftp://emboss.open-bio.org/pub/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}/

#./configure --prefix=${HOME} --without-x CC="gcc -march=native -fomit-frame-pointer" INCLUDES="-I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
./configure --prefix=${HOME} --without-x CC="gcc -march=native -fomit-frame-pointer" LDFLAGS="-Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
make || "${program} build failed"
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries

make check || "${program} check failed"

make install || "${program} install failed"

exit 0
