#!/usr/bin/env python

from sqlitedict import SqliteDict
import sys

from collections import namedtuple

My_tuple = namedtuple("My_tuple", ["foo", "bar"])

d = SqliteDict(sys.argv[1], autocommit=True)

if "a" not in d:
    t = My_tuple("a", set([]))
    d["a"] = t
d["a"].bar.add(sys.argv[2])

print "\n".join(sorted(d["a"].bar))

#d["c:d"] = "ab"
#d[frozenset(["c", "d"])] = "ab"
