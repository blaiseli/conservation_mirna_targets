#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="tophat"
version="2.1.0"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${program}-${version}*
# Download it
wget --continue http://ccb.jhu.edu/software/${program}/downloads/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
make || error_exit "${program} build failed"
make install || error_exit "${program} install failed"

exit 0
