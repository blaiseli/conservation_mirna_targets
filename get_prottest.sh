#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="prottest3"
#version="3.4"

mkdir -p ${HOME}/src
cd ${HOME}/src
#sudo apt-get install openjdk-7-jdk
#sudo apt-get install ant



mkdir -p ${HOME}/src
cd ${HOME}/src


if [ ! -e ${program} ]
then
    git clone https://github.com/ddarriba/${program}.git || error_exit "${program} download failed"
    cd ${program}
else
    cd ${program}
    git pull || error_exit "${program} update failed"
fi

JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64/jre" ant jar || error_exit "${program} build failed"

JAR=$(ls dist/prottest*.jar)
version=$(echo ${JAR%.jar} | sed 's|dist/prottest-||')

cat << EOF > ${HOME}/bin/${program}
#!/bin/bash
java -jar ${HOME}/src/${program}/dist/prottest-${version}.jar \$@
EOF

chmod +x ${HOME}/bin/${program}

#${program} -help 2> /dev/null || error_exit "${program} test failed"

exit 0
