#!/bin/bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# Also depends on ncurses

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="2.23.0"


mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf v${version}*
rm -rf bedtools2-${version}*

wget https://github.com/arq5x/bedtools2/archive/v${version}.tar.gz || error_exit "bedtools download failed"
tar -xvzf v${version}.tar.gz
cd bedtools2-${version}/

# Use native processor architecture for compiling 
sed -i 's|^export CXXFLAGS =\(.*\) \$(INCLUDES)$|export CXXFLAGS =\1 -march=native -fomit-frame-pointer $(INCLUDES)|g' Makefile
# Add the path to possible locally-compiled included libraries
INCLUDES="-I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" make || "bedtools build failed"

ln -sf ${HOME}/src/bedtools2-${version}/bin/bedtools ${HOME}/bin/. || error_exit "bedtools install failed"

exit 0
