#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

project="standard-RAxML"
program=`echo ${project} | tr '[:upper:]' '[:lower:]'`


mkdir -p ${HOME}/src
cd ${HOME}/src

if [ ! -e ${project} ]
then
    git clone https://github.com/stamatak/${project}.git || error_exit "${project} download failed"
    cd ${project}
else
    cd ${project}
    git pull || error_exit "${project} update failed"
fi

if [[ $(cat /proc/cpuinfo | grep flags | grep -w avx2) ]]
then
    the_makefile="Makefile.AVX2.PTHREADS.gcc"
else
    if [[ $(cat /proc/cpuinfo | grep flags | grep -w avx) ]]
    then
        the_makefile="Makefile.AVX.PTHREADS.gcc"
    else
        if [[ $(cat /proc/cpuinfo | grep flags | grep -w avx) ]]
        then
            the_makefile="Makefile.SSE3.PTHREADS.gcc"
        else
            the_makefile="Makefile.PTHREADS.gcc"
        fi
    fi
fi

# Use native processor architecture for compiling 
sed -i 's|^CFLAGS = \(.*\)$|CFLAGS = -march=native \1|' ${the_makefile}

# On linux, we may try to enforce thread affinity
sed -i 's|^#define _PORTABLE_PTHREADS$|//#define _PORTABLE_PTHREADS|' axml.c


# Add the path to possible locally-compiled included libraries
LDFLAGS="-I ${HOME}/include -L${HOME}/lib -Wl,-rpath,${HOME}/lib" make -f ${the_makefile} || "${program} build failed"

the_bin=$(awk '$1=="all" {print $NF}' ${the_makefile})
ln -sf ${HOME}/src/${project}/${the_bin} ${HOME}/bin/raxml || error_exit "${program} install failed"

exit 0
