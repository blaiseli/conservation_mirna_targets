#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a series of pfm matrices to build a dictionary
associating k-mers to matrix consensuses containing such k-mers."""

import argparse

import os
OPB = os.path.basename

import sys
WRITE = sys.stdout.write

#from string import split, strip
SPLIT = str.split
STRIP = str.strip

from numpy import array, transpose
#from numpy import argmax, array, asarray
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s:\n%s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

# To store dictionaries
from cPickle import dump, HIGHEST_PROTOCOL

#from collections import deque, defaultdict
from collections import defaultdict
from itertools import imap, product
from operator import or_ as union
from operator import mul



def read_JASPAR_pfm(file_handle, prot_name):
    """Reads four lines from *file_handle*.
    Computes the consensus of the corresponding ATGC profile.
    Lists the contained k-mers.
    Records *prot_name* in *Globals.hexamer2tf* and *Globals.heptamer2tf*
    at the k-mer keys.
    Records the motif degenerescence in *Globals.motif_degen*.
    Example matrix
    >MA0027.1 En1
    4   5   3   0   4   3   3   2   1   1   1
    1   2   0   0   0   0   0   1   3   4   6
    2   2   7   2   3   7   0   4   3   1   1
    3   1   0   8   3   0   7   3   3   4   2
    (The header should have been skipped before calling the present function.)
    """
    a_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    motif_len = len(a_counts)
    if motif_len < 6:
        warnings.warn("Skipping %s: matrix too short." % prot_name)
        # No hexamer can be found in the motif consensus
        # skip the other lines and do nothing else
        file_handle.readline()
        file_handle.readline()
        file_handle.readline()
        return
    c_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    g_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    t_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    counts = transpose(array([a_counts, c_counts, g_counts, t_counts]))
    consensus = make_consensus(counts)
    record_kmers(consensus, prot_name)
    return


def read_RBPmap_pfm(file_handle, prot_name):
    """Reads the remaining lines from *file_handle*.
    Computes the consensus of the corresponding ATGC profile.
    Lists the contained k-mers.
    Records *prot_name* in *Globals.hexamer2tf* and *Globals.heptamer2tf*
    at the k-mer keys.
    Records the motif degenerescence in *Globals.motif_degen*."""
    freqs_generator = (map(
        float,
        SPLIT(STRIP(line))[1:]) for line in file_handle)
    consensus = make_consensus(freqs_generator)
    motif_len = len(consensus)
    if motif_len < 6:
        warnings.warn("Ignoring %s: matrix too short." % prot_name)
        # No hexamer can be found in the motif consensus
        return
    record_kmers(consensus, prot_name)
    return


def make_consensus(values):
    """Takes an iterable *values* yielding quadruplets corresponding to
    counts or frequencies for letters A, C, G and T, in that order.
    Builds and returns the corresponding majority-rule consensus in the form
    of a list of sets of letters, one set per position."""
    consensus = []
    # Loop over positions in the motif
    for acgt_values in values:
        max_val = 0
        letters = set()
        for (letter, val) in zip("ACGT", acgt_values):
            if val > max_val:
                letters = set(letter)
                max_val = val
            elif val == max_val:
                letters.add(letter)
        consensus.append(letters)
    return consensus


def record_kmers(consensus, prot_name):
    """Records *prot_name* for the 6-mers and 7-mers present in
    *consensus* as well as its degeneracy in dictionaries held
    in *Globals*."""
    # We don't want to over-ride stuff
    assert prot_name not in Globals.motif_degen
    # Compute the product of the numbers of letters
    # at the successive positions in the consensus.
    Globals.motif_degen[prot_name] = reduce(mul, imap(len, consensus), 1)
    hexamers = reduce(
            union,
            [{"".join(letters) for letters in product(*consensus[s:s+6])}
                for s in range(len(consensus) - 5)])
    for hexamer in hexamers:
        Globals.hexamer2tf[hexamer].add(prot_name)
    if len(consensus) < 7:
        return
    heptamers = reduce(
            union,
            [{"".join(letters) for letters in product(*consensus[s:s+7])}
                for s in range(len(consensus) - 6)])
    for heptamer in heptamers:
        Globals.heptamer2tf[heptamer].add(prot_name)
    return


class Globals(object):
    """Class used to hold global variables as its class attributes."""
    # key: transcription factor name
    # value: consensus degeneration for this transcription factor
    motif_degen = defaultdict(int)
    # key: k-mer
    # value: set of transcription factor names
    # having the k-mer in their consensus
    hexamer2tf = defaultdict(set)
    heptamer2tf = defaultdict(set)
    @staticmethod
    def store_dicts(pickle_file):
        """Use cPickle.load to retrieve the dictionaries."""
        dump(Globals.motif_degen, pickle_file, HIGHEST_PROTOCOL)
        dump(Globals.hexamer2tf, pickle_file, HIGHEST_PROTOCOL)
        dump(Globals.heptamer2tf, pickle_file, HIGHEST_PROTOCOL)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "matrices",
        help="Space-separated paths to the files containing the pfm matrices.",
        nargs="+")
    parser.add_argument(
        "-d", "--kmer_dicts",
        help="Pickle file in which to store the dictionary.",
        type=argparse.FileType("wb"))
    parser.add_argument(
        "-f", "--m_format",
        help="Where do the matrices come from. "
        "This determines the way the matrices are parsed.",
        choices=["JASPAR", "RBPmap"],
        default="JASPAR")
    args = parser.parse_args()
    for input_file in args.matrices:
        if args.m_format == "JASPAR":
            with open(input_file) as matrix:
                matrix_header = matrix.readline()
                while matrix_header:
                    tf_name = "_".join(SPLIT(STRIP(matrix_header))[1:])
                    # Modifies Globals.hexamer2tf, Globals.heptamer2tf,
                    # and the state of the file handle
                    read_JASPAR_pfm(matrix, tf_name)
                    matrix_header = matrix.readline()
        elif args.m_format == "RBPmap":
            prot_name = "_".join(SPLIT(OPB(input_file), "_")[:-1])
            with open(input_file) as matrix:
                matrix_header = matrix.readline()
                assert SPLIT(matrix_header) == ["Pos", "A", "C", "G", "U"]
                # Modifies Globals.hexamer2tf, Globals.heptamer2tf,
                # and the state of the file handle
                read_RBPmap_pfm(matrix, prot_name)
        else:
            msg = "%s format not recognised.\n" % args.m_format
            raise NotImplementedError(msg)
    with args.kmer_dicts as pickle_file:
        WRITE("\nPickling dictionaries in %s ...\n" % pickle_file.name)
        Globals.store_dicts(pickle_file)
    return 0

if __name__ == "__main__":
    sys.exit(main())
