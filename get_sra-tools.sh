#!/bin/bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src
cd ${HOME}/src

if [ ! -e ngs ]
then
    git clone https://github.com/ncbi/ngs.git || error_exit "ngs-sdk download failed"
    cd ngs
else
    cd ngs
    git pull || error_exit "ngs-sdk update failed"
fi

cd ngs-sdk

./configure --prefix=${HOME} || error_exit "ngs-sdk configure failed"
make clean
make || error_exit "ngs-sdk build failed"
make install || error_exit "ngs-sdk install failed"

cd ${HOME}/src

if [ ! -e ncbi-vdb ]
then
    git clone https://github.com/ncbi/ncbi-vdb.git || error_exit "ncbi-vdb download failed"
    cd ncbi-vdb
else
    cd ncbi-vdb
    git pull || error_exit "ncbi-vdb update failed"
fi

./configure --prefix=${HOME} || error_exit "ncbi-vdb configure failed"
make clean
make || error_exit "ncbi-vdb build failed"
make install || error_exit "ncbi-vdb install failed"
export LD_LIBRARY_PATH=${HOME}/lib64:$LD_LIBRARY_PATH
export NCBI_VDB_LIBDIR=${HOME}/lib64
#echo "export LD_LIBRARY_PATH=${HOME}/lib64:$LD_LIBRARY_PATH" >> ${HOME}/.bashrc
#echo "export NCBI_VDB_LIBDIR=/home/severine.chambeyron/lib64" >> ${HOME}/.bashrc

cd ${HOME}/src

if [ ! -e sra-tools ]
then
    git clone https://github.com/ncbi/sra-tools.git || error_exit "sra-tools download failed"
    cd sra-tools
else
    cd sra-tools
    git pull || error_exit "sra-tools update failed"
fi

./configure --prefix=${HOME} || error_exit "sra-tools configure failed"
make clean
make || error_exit "sra-tools build failed"
make install || error_exit "sra-tools install failed"

exit 0
