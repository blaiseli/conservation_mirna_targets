#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script takes a tree in newick format, restores names transformed for
phylip compatibility, and restores duplicates sequences."""

import argparse
import os
import sys
import warnings

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import p4
p4.var.phylipDataMaxNameLength = 64

#class Globals(object):
#    """Class used to hold global variables as its class attributes."""
#
def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_tree",
        help="Input tree file.")
    parser.add_argument(
        "out_tree",
        help="Output tree file.")
    parser.add_argument(
        "-p", "--phylip_names_dict",
        help="Python module containing the dictionary p4 saved "
        "when renaming the alignment for phylip compatibility.")
    parser.add_argument(
        "-d", "--duplicates_dict",
        help="Python module containing the dictionary p4 saved "
        "when removing duplicate sequences from the alignment.")
    args = parser.parse_args()

    p4.read(args.in_tree)
    t = p4.var.trees[0]
    t.restoreNamesFromRenameForPhylip(args.phylip_names_dict)
    t.restoreDupeTaxa(args.duplicates_dict, asMultiNames=False)
    t.writePhylip(args.out_tree)
    return 0

if __name__ == "__main__":
    sys.exit(main())
