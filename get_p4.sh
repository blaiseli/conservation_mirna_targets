#!/usr/bin/env bash
# Depends on python setuptools and development libraries

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}
# sudo apt-get install libgsl0-dev
program="p4-phylogenetics"

mkdir -p ${HOME}/src
cd ${HOME}/src

if [ ! -e ${program} ]
then
    #git clone https://github.com/pgfoster/${program}.git || error_exit "${program} download failed"
    git clone https://github.com/blaiseli/${program}.git || error_exit "${program} download faile"
    cd ${program}
    git checkout fasta_options
else
    cd ${program}
    git checkout fasta_options
    git pull || error_exit "${program} update failed"
fi

rm -f installation.py
rm -rf ${HOME}/.local/share/doc/p4
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"

python setup.py install --user || error_exit "${program} install failed"

exit 0
