#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script takes a tree in newick format, restores names transformed for
phylip compatibility, and restores duplicates sequences."""

import argparse
import os
import sys
import warnings

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import p4
p4.var.phylipDataMaxNameLength = 64

import ete2

#class Globals(object):
#    """Class used to hold global variables as its class attributes."""
#
def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_tree",
        help="Input newick tree file.")
    parser.add_argument(
        "out_tree",
        help="Output tree image file.")
    parser.add_argument(
        "-t", "--support_threshold",
        type=float,
        help="Threshold for.")
    args = parser.parse_args()

    p4.read(args.in_tree)
    t = p4.var.trees[0]
    #t.restoreNamesFromRenameForPhylip(args.phylip_names_dict)
    #t.restoreDupeTaxa(args.duplicates_dict, asMultiNames=False)
    #t.writePhylip(args.out_tree)

    # Simplify names
    for n in t.iterLeavesNoRoot():
        new_name = n.name.split("_")[:3]
        n.name = "_".join(new_name)
    if args.support_threshold:
        # List nodes with low support
        to_collapse = []
        for n in t.iterInternalsNoRoot():
            if n.name is not None:
                if float(n.name) < args.support_threshold:
                    to_collapse.append(n.nodeNum)
        # Collapse listed nodes
        for num in to_collapse:
            t.collapseNode(num)

    # Convert the tree to ete2
    t2 = ete2.Tree(t.writeNewick(toString=True))
    # Make a circular display style
    ts = ete2.TreeStyle()
    ts.mode = "c"
    ts.arc_start = -45
    ts.arc_span = 90
    ts.show_leaf_name = True
    ts.show_branch_support = True
    # Makes image production fail
    #ts.title.add_face("RdRP_domains_msaprobs_fastme", column=1)

    # Make a style for branches leading to "Branchiostoma" sequences
    nstyle = ete2.NodeStyle()
    nstyle["hz_line_color"] = "Red"
    nstyle["hz_line_width"] = 7.5
    # Make a style for the other branches
    nstyle2 = ete2.NodeStyle()
    nstyle["hz_line_width"] = 5
    #branchiostomas = []
    # Distribute the styles
    for n in t2.traverse():
        if n.name.split("_")[0] == "Branchiostoma":
            #branchiostomas.append(n)
            n.set_style(nstyle)
        else:
            n.set_style(nstyle2)

    # To put a background below the common ancestor
    # of all "Branchiostoma" sequences:
    #b_ancestor = t2.get_common_ancestor(*branchiostomas)
    #nstyle3 = ete2.NodeStyle()
    #nstyle3["bgcolor"] = "LightSteelBlue"
    #b_ancestor.set_style(nstyle3)


    #t2.show(tree_style=ts)
    #t2.render("images/RdRP_domains_msaprobs_fastme.svg", tree_style=ts)
    t2.render(args.out_tree, tree_style=ts)

    return 0

if __name__ == "__main__":
    sys.exit(main())
