#!/bin/bash
# Used to get mapping data suitable for making pileups or viewing with IGV
# Usage: sam2indexedbam.sh <mapping.sam>
# Works if either <mapping.sam>, <mapping.sam.gz> or <mapping.sam.bz2> exists.
# Generates two files: <mapping_sorted.bam> and <mapping_sorted.bam.bai>.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


SAM=${1}

if [ -e ${SAM} ]
then
    # -F 4: skip reads that did not map
    # -u: output uncompressed bam, to save time when piping to samtools sort
    cat ${SAM} | samtools view -Su -F 4 - | samtools sort - ${SAM%.sam}_sorted || error_exit "samtools sort failed"
elif [ -e ${SAM}.gz ]
then
    zcat ${SAM}.gz | samtools view -Su -F 4 - | samtools sort - ${SAM%.sam}_sorted || error_exit "samtools sort failed"
elif [ -e ${SAM}.bz2 ]
then
    bzcat ${SAM}.bz2 | samtools view -Su -F 4 - | samtools sort - ${SAM%.sam}_sorted || error_exit "samtools sort failed"
else
    echo "${SAM} not found, neither .gz or .bz2 versions."
    echo "Aborting."
    exit 1
fi
# There are random segmentation errors with samtools index
#samtools index ${SAM%.sam}_sorted.bam || error_exit "samtools index failed"
indexed=""
while [ ! ${indexed} ]
do
    samtools index ${SAM%.sam}_sorted.bam && indexed="OK"
    if [ ! ${indexed} ]
    then
        rm -f ${SAM%.sam}_sorted.bam.bai
        echo "Indexing failed. Retrying" 1>&2
    fi
done || error_exit "samtools index failed"



exit 0
