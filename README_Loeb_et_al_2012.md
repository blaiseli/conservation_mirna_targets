We want to check the results of figure 2A of [Agarwal et al.
(2015)](http://dx.doi.org/10.7554/eLife.05005), concerning motifs in sequences
lost in dCLIP experiments where miR-155 is KO.


Data
====

Data used for this MEME analysis originally comes from [Loeb et al. (2012)](dx.doi.org/10.1016/j.molcel.2012.10.002).
The data is deposited in GEO: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE41288>

It seems that Agarwal et al. actually used 6 datasets from this repository (see their table 2):
CD4T_155KO_rep1: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012118>
CD4T_155KO_rep2: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012119>
CD4T_155KO_rep3: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012120>
CD4T_WT_rep1: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012121>
CD4T_WT_rep2: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012122>
CD4T_WT_rep3: <http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1012122>

Downloading the data
--------------------

    mkdir -p ~/Blaise/Loeb_et_al_2012_data/raw_data
    cd ~/Blaise/Loeb_et_al_2012_data/raw_data
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012118&id=7882&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_155KO_rep1.txt
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012119&id=7883&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_155KO_rep2.txt
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012120&id=7884&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_155KO_rep3.txt
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012121&id=7885&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_WT_rep1.txt
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012122&id=7886&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_WT_rep2.txt
    wget "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSM1012123&id=7887&db=GeoDb_blob88" -O /dev/stdout | awk -F "\t" 'NF == 2 {print}' > CD4T_WT_rep3.txt
    # [Affymetrix Mouse Genome 430A 2.0 Array](http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL8321)
    wget ftp://ftp.ncbi.nlm.nih.gov/geo/platforms/GPL8nnn/GPL8321/annot/GPL8321.annot.gz
    
There are also pre-processed data available:

> When available, target genes identified using high-throughput CLIP data were
> collected from the supplemental materials of the corresponding studies
> (Lipchina et al., 2011; Loeb et al., 2012; Helwak et al., 2013; Grosswendt et
> al., 2014)


    cd ~/Blaise/Loeb_et_al_2012_data
    # Supplementary data:
    # Table S3. 3UTR miR-155 Noncanonical Sites, Related to Figure 3 and the Results
    wget http://www.cell.com/cms/attachment/2016508720/2037209139/mmc3.xls

We can use the `xlrd` python module to extract the data from this excel document.

    # Installing the xlrd module
    get_xlrd.sh
    python -c "
    import xlrd
    book = xlrd.open_workbook('mmc3.xls')
    sh = book.sheet_by_name('3\'UTR_miR-155_Noncanonical_Site')
    print ', '.join([cell.value.strip() for cell in sh.row(0)])"

Output:

Ensembl or Refseq Identifier, Gene Symbol, Log2(WT Reads), Log2(KO Reads), Log2(WT/KO Reads), Diff binding p-value, Log2(KO/WT) Expression, Start of Peak*, End of Peak*, Alignment**, Peak Sequence

Extract sequences
-----------------

    inspect_Loeb_et_al_results.py \
        -s non_canonical_sequences.fasta \
        -a non_canonical_seed_alignments.fasta \
        mmc3.xls


Run MEME
========
    
    meme -dna -mod zoops -nmotifs 10 -minw 4 -maxw 8 -o non_canonical_sequences_MEME_out non_canonical_sequences.fasta

We reproduce the results of Agarwal et al.


Erase useless data
==================

    rm -rf ~/Blaise/Loeb_et_al_2012_data/raw_data
