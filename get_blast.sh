#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="blast"
version="2.2.31"

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ncbi-${program}-${version}+-src*
wget --continue ftp://ftp.ncbi.nlm.nih.gov/${program}/executables/${program}+/${version}/ncbi-${program}-${version}+-src.tar.gz || error_exit "${program} download failed"
tar -xvzf ncbi-${program}-${version}+-src.tar.gz
cd ncbi-${program}-${version}+-src/c++
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
./configure --prefix=${HOME} --without-gui CFLAGS="-O3 -march=native -fomit-frame-pointer" CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
make || "${program} build failed"
make check || "${program} check failed"
make install || error_exit "${program} install failed"



exit 0
