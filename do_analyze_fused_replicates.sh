#!/bin/bash
# Usage: do_analyze_fused_replicates.sh <replicate1.fq.gz> <replicate2.fq.gz> <library_directory> <nb_procs> <annotations.gtf> <genome_name>
# $1, $2: the two fq.gz files to jointly analyze
# $3: library directory.
# $4: number of processors to use
# $5: .gtf annotation file
# $6: genome name (there should be a corresponding .fa file and set of bowtie2 index files)

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
    #   ----------------------------------------------------------------
    #   Function for exit due to fatal program error
    #       Accepts 1 argument:
    #           string containing descriptive error message
    #   ----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


FQ1=${1}
FQ2=${2}

if [[ "${1}" = /* ]]
then
    # Path is already absolute
    FQ1="${1}"
else
    # Transform the relative path to an absolute one
    FQ1="${PWD}/${1}"
fi

if [[ "${2}" = /* ]]
then
    # Path is already absolute
    FQ2="${2}"
else
    # Transform the relative path to an absolute one
    FQ2="${PWD}/${2}"
fi


lib_dir=${3}
mkdir ${lib_dir}

nb_procs=${4}

#annot="genes_mm10_abundant_ncRNAs_spike_in.gtf"
#genome="mouse_genome_spike_in"
annot=${5}
genome=${6}

# http://stackoverflow.com/questions/11403820/determine-if-relative-or-absolute-path-in-shell-program
# http://stackoverflow.com/questions/17583578/what-command-means-do-nothing-in-a-conditional-in-bash
if [[ "${annot}" = /* ]]
then
    # Path is already absolute
    :
else
    # Transform the relative path to an absolute one
    annot="${PWD}/${annot}"
fi

if [[ "${genome}" = /* ]]
then
    # Path is already absolute
    :
else
    # Transform the relative path to an absolute one
    genome="${PWD}/${genome}"
fi

cd ${lib_dir}

if [[ ! -e ${annot} ]]
then
    error_exit "Annotation file ${annot} not found. File needed for tophat and cufflinks. Aborting."
fi

if [[ ! -e ${FQ1} ]]
then
    error_exit "Raw data file ${FQ1} not found. File needed for tophat. Aborting."
fi

if [[ ! -e ${FQ2} ]]
then
    error_exit "Raw data file ${FQ2} not found. File needed for tophat. Aborting."
fi


LIB=`basename ${lib_dir}`
PLACE=`pwd`

echo ${PLACE} > tophat.log
date >> tophat.log
echo "stderr:" > tophat.errors
cmd="nice -n 19 ionice -c2 -n7 tophat -p ${nb_procs} -o tophat_${LIB}_options_G -G ${annot} ${genome} ${FQ1},${FQ2}"
echo "${cmd}" >> tophat.log
echo "-----" >> tophat.log
echo "stdout:" >> tophat.log
eval ${cmd} >> tophat.log 2>> tophat.errors || error_exit "tophat failed"
echo "-----" >> tophat.log
cat tophat.errors >> tophat.log
date >> tophat.log

# script using the mailsend program
#notify_by_mail.sh "tophat_for_${LIB}_finished" "The analysis ran in ${HOSTNAME}:${PLACE}. Log is attached." tophat.log

if [[ ! -e ${genome}.fa ]]
then
    error_exit "Genome ${genome}.fa not found. File needed for cufflinks. Aborting."
fi

echo ${PLACE} > cufflinks.log
date >> cufflinks.log
echo "stderr:" > cufflinks.errors
cmd="nice -n 19 ionice -c2 -n7 cufflinks -p ${nb_procs} --no-update-check -o cufflinks_${LIB}_options_bG -b ${genome}.fa -G ${annot} tophat_${LIB}_options_G/accepted_hits.bam"
echo "${cmd}" >> cufflinks.log
echo "-----" >> cufflinks.log
echo "stdout:" >> cufflinks.log
eval ${cmd} >> cufflinks.log 2>> cufflinks.errors || error_exit "cufflinks failed"
echo "-----" >> cufflinks.log
cat cufflinks.errors >> cufflinks.log
date >> cufflinks.log

# script using the mailsend program
#notify_by_mail.sh "cufflinks_for_${LIB}_finished" "The analysis ran in ${HOSTNAME}:${PLACE}. Log is attached." cufflinks.log

exit 0
