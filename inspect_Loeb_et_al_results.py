#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads the excel document downloaded at
http://www.cell.com/cms/attachment/2016508720/2037209139/mmc3.xls and does
stuff."""

import argparse
import sys
WRITE = sys.stdout.write

#from string import split, strip
SPLIT = str.split
STRIP = str.strip

from numpy import array, asarray, transpose
#from numpy import argmax, array, asarray
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

# To load dictionaries
from cPickle import load

# To manipulate nucleotide sequences
from string import maketrans
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")
RNA2DNA_TABLE = maketrans("U", "T")

def to_dna(seq):
    """Converts RNA to DNA"""
    return seq.translate(RNA2DNA_TABLE)

def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]

#from collections import deque, defaultdict
from collections import defaultdict
from itertools import imap, product
from operator import or_ as union
from operator import mul

from xlrd import open_workbook

class Globals(object):
    """Class used to hold global variables as its class attributes."""
    #@staticmethod
    #def load_dicts(pickle_file):
    #    """Use cPickle.load to retrieve the dictionaries."""
    #    Globals.tf2degen = load(pickle_file)
    #    Globals.hexamer2tf = load(pickle_file)
    #    Globals.heptamer2tf = load(pickle_file)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "spreadsheet",
        help="Excel document listing non-canonical sites "
        "for miR-155 according to Loeb et al. (2012).")
    parser.add_argument(
        "-s", "--sequences",
        type=argparse.FileType('w'),
        help="Fasta file in which to write the reported peak sequences.")
    parser.add_argument(
        "-a", "--alignments",
        type=argparse.FileType('w'),
        help="Fasta file in which to write the regions reportedly "
        "aligned with the seed.")
    #parser.add_argument(
    #    "-s", "--separate_revcompl",
    #    help="To output results for the reverse complement on a separate line.",
    #    action="store_true")
    args = parser.parse_args()
    book = open_workbook(args.spreadsheet)
    table = book.sheet_by_index(0)
    headers = [cell.value for cell in table.row(0)]
    #print "\n".join(["%d\t%s" % (col, name) for (col, name) in enumerate(headers)])
    #0   Ensembl or Refseq Identifier
    #1   Gene Symbol
    #2   Log2(WT Reads) 
    #3   Log2(KO Reads) 
    #4   Log2(WT/KO Reads)
    #5   Diff binding p-value
    #6   Log2(KO/WT) Expression
    #7   Start of Peak*
    #8   End of Peak*
    #9   Alignment**
    #10  Peak Sequence
    # We skip the header
    for row_num in xrange(1, table.nrows):
        row = [cell.value for cell in table.row(row_num)]
        #if len(row) > 1 and row[10] and set(row[10]) <= set("ACGT"):
        # We only want lines corrsponding to results records.
        # We need to check for non-empty 11th column:
        # empty columns seem to exist.
        #if len(row) > 1 and row[10]:
        if row[10]:
        #if len(row) > 1:
            #print row[9]
            args.alignments.write(">%s_ali\n%s\n" % (row[0], row[9]))
            args.sequences.write(">%s_peak\n%s\n" % (row[0], row[10]))
    return 0

if __name__ == "__main__":
    sys.exit(main())
