#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="jmodeltest"
version="2.1.7"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
# The download id can be found in the source of page https://drive.google.com/folderview?id=0ByrkKOPtF_n_OUs3d0dNcnJPYXM#list
# This page in turn is accessed from the README in https://github.com/ddarriba/jmodeltest2
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=0ByrkKOPtF_n_V2FlcnFDUnljeXc' -O ${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz || error_exit "${program} unpacking failed"

cat << EOF > ${HOME}/bin/${program}
#!/bin/bash
java -jar ${HOME}/src/${program}-${version}/jModelTest.jar \$@
EOF

chmod +x ${HOME}/bin/${program}

${program} -help 2> /dev/null || error_exit "${program} test failed"

exit 0
