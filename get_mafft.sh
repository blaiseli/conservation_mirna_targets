#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="mafft"
version="7.245-without-extensions"

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}-${version}*
wget --continue http://mafft.cbrc.jp/alignment/software/${program}-${version}-src.tgz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}-src.tgz
cd ${program}-${version}/core
make clean
# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
sed -i 's|^PREFIX = /usr/local|PREFIX = ${HOME}|' Makefile
sed -i 's|^CFLAGS = -O3|CFLAGS = -O3 -march=native -fomit-frame-pointer -I${HOME}/include|' Makefile
LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" make || error_exit "${program} build failed"
make install || error_exit "${program} install failed"

exit 0
