We want to see if there are motifs in the 3UTRs of genes enriched in the CLIP experiments when an anti-miR has been transfected.

Data provided by Herve Seitz
============================

    mkdir -p ~/Natalia/RIP/RNAseq_IP_reelle/analyses_Blaise/motifs_dans_cibles_enrichies_edgeR_TMM
    cd ~/Natalia/RIP/RNAseq_IP_reelle/analyses_Blaise/motifs_dans_cibles_enrichies_edgeR_TMM

Genes enriched in the CLIP experiments
--------------------------------------

`Target_abundances_m1m2_EdgeR_TMM_adjusted_p_values_sample_A.dat`
`Target_abundances_m3_EdgeR_TMM_adjusted_p_values_sample_A.dat`

3UTRs of all detected genes
---------------------------

`Detected_mRNA_UTRs.fa.bz2`

### Count sequences

    bzcat Detected_mRNA_UTRs.fa.bz2 | grep "^>" | wc -l

Output:

64053


Preparing the data
------------------

### Convert the data to `bgz` compressed fasta for easyer handling with biopython

    # Append gene name to sequence ID using sed before recompressing
    sed 's/>\([0-9][0-9]*\); gene: \(.*\)/>\1_\2/' Detected_mRNA_UTRs.fa \
        | bgzip > Detected_mRNA_UTRs.fa.bgz

We will search for motifs in these 3UTRs for the enriched and non-enriched genes.


Motif search in 3UTRs
=====================

Extract the 3UTRs
-----------------

    extract_CLIP_UTRs_for_motif_searches.py \
        -i Target_abundances_m1m2_EdgeR_TMM_adjusted_p_values_sample_A.dat \
        -f m1m2_different_UTRs.fa \
        -c m1m2_control_UTRs.fa Detected_mRNA_UTRs.fa.bgz
    extract_CLIP_UTRs_for_motif_searches.py \
        -i Target_abundances_m3_EdgeR_TMM_adjusted_p_values_sample_A.dat \
        -f m3_different_UTRs.fa \
        -c m3_control_UTRs.fa Detected_mRNA_UTRs.fa.bgz


Search for motifs
-----------------

A simple approach is to search for motifs in the differentially-clipped genes
and in the control, and consider only the motifs not present in the control.

    #meme -dna -mod zoops -nmotifs 10 -minw 4 -maxw 8 -o m1m2_different_UTRs_MEME_out m1m2_different_UTRs.fa
    #meme -dna -mod zoops -nmotifs 10 -minw 4 -maxw 8 -o m1m2_control_UTRs_MEME_out m1m2_control_UTRs.fa
    #meme -dna -mod zoops -nmotifs 10 -minw 4 -maxw 8 -o m3_different_UTRs_MEME_out m3_different_UTRs.fa
    #meme -dna -mod zoops -nmotifs 10 -minw 4 -maxw 8 -o m3_control_UTRs_MEME_out m3_control_UTRs.fa
    meme -dna -mod zoops \
        -nmotifs 10 -minw 4 -maxw 8 \
        -o m1m2_different_UTRs_nopsp_MEME_out m1m2_different_UTRs.fa
    meme -dna -mod zoops \
        -nmotifs 10 -minw 4 -maxw 8 \
        -o m3_different_UTRs_nopsp_MEME_out m3_different_UTRs.fa

We  could try with `-mod anr` to allow  any number of motif occurrences per sequence.
The `-mod zoops` option searches for at most one per sequence, but is supposed to be faster.

According to the MEME website:

> MEME on the web can take a second (control) set of input sequences and then
> discovers motifs that are enriched in the primary set relative to the control
> set. This is called discriminative motif discovery.

And in the web interface help:

> Discriminative mode
>
>    You provide two sets of sequences and MEME discovers motifs that are
>    enriched in the first (primary) set relative to the second (control) set.
>    In Discriminative mode, we first calculate a position-specific prior from
>    the two sets of sequences. MEME then searches the first set of sequences
>    for motifs using the position-specific prior to inform the search. This
>    approach is based on the simple discriminative prior "D" described in
>    Section 3.5 of Narlikar et al. We modify their approach to search for the
>    "best" initial motif width, and to handle protein sequences using spaced
>    triples.
>
>    Refer to the psp-gen documentation and to our paper for more details.

    psp-gen \
        -pos m1m2_different_UTRs.fa \
        -neg m1m2_control_UTRs.fa \
        -minw 4 -maxw 8 \
        -alpha DNA \
        > m1m2_psp.txt

    psp-gen \
        -pos m3_different_UTRs.fa \
        -neg m3_control_UTRs.fa \
        -minw 4 -maxw 8 \
        -alpha DNA \
        > m3_psp.txt

    meme -dna -mod zoops \
        -psp m1m2_psp.txt \
        -nmotifs 10 -minw 4 -maxw 8 \
        -o m1m2_different_UTRs_MEME_out m1m2_different_UTRs.fa
    meme -dna -mod zoops \
        -psp m3_psp.txt \
        -nmotifs 10 -minw 4 -maxw 8 \
        -o m3_different_UTRs_MEME_out m3_different_UTRs.fa

No motif has a low E-value.
