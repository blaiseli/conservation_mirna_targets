#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="2.2.6"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf bowtie2-${version}*
# Download it
wget --continue http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/${version}/bowtie2-${version}-source.zip || error_exit "bowtie2 download failed"
unzip bowtie2-${version}-source.zip
cd bowtie2-${version}
mv Makefile Makefile.original
# Use native processor architecture for compiling 
sed 's/^RELEASE_FLAGS\(.*\)$/#RELEASE_FLAGS\1\nRELEASE_FLAGS = -O3 -march=native -funroll-loops -g3/g' Makefile.original > Makefile.tmp
# Remove the rule that sets clang as the compiler
sed -e '/^ifneq.*shell uname -r/,+4d' Makefile.tmp > Makefile
rm Makefile.tmp
make || error_exit "bowtie2 build failed"
# Install in home directory
mkdir -p ${HOME}/bin
for binary in "bowtie2" "bowtie2-align-s" "bowtie2-align-l" "bowtie2-build" "bowtie2-build-s" "bowtie2-build-l" "bowtie2-inspect" "bowtie2-inspect-s" "bowtie2-inspect-l"
do
    # Force link re-creation if they already exist
    ln -sf ${HOME}/src/bowtie2-${version}/${binary} ${HOME}/bin/. || error_exit "${binary} install failed"
done

exit 0
