Getting a biopython version that includes a MAF alignement parser
=================================================================

N-mer conservation is estimated from a multiple genome alignment built by UCSC
and provided in the MAF format.

Until the official biopython release includes my changes to the unofficial MAF
parser (pull request [504](https://github.com/biopython/biopython/pull/504)),
you need to install my fork of biopython.

    # http://askubuntu.com/questions/588390/how-do-i-check-whether-a-module-is-installed-or-not-in-python
    python -c "from Bio.AlignIO.MafIO import MafIndex" \
        1> /dev/null \
        2> /dev/null
    # Test the return code of the command.
    # 0 means success, something else means failure.
    if (($?))
    then
        echo "It seems that the needed MAF parser is not installed."
        echo "Trying to install it."
        # Clone my fork using git
        git clone git@github.com:blaiseli/biopython.git
        # Switch to the alignio-maf branch
        git checkout alignio-maf
        # Install biopython locally
        python setup.py install --user
        python -c "from Bio.AlignIO.MafIO import MafIndex" \
            1> /dev/null \
            2> /dev/null
        if (($?))
        then
            echo "Still no MAF parser available. Expect problems."
        fi
    fi

For a locally-installed version to be taken into account it seems that your
`~/.local/lib/python2.7/site-packages` directory should be before
`/usr/lib/pymodules/python2.7` in `sys.path` (within python).


Initial settings
================

    set +u

All analyses will happen relative to a base directory.

    # This USERDIR variable was relevant in the context of our local installation
    # and was used to avoid downloading some already present files.
    [[ ${USERDIR} ]] || USERDIR="${HOME}"
    # To override, either edit here or set when calling `bash` (see example later).
    [[ ${BASEDIR} ]] || BASEDIR="${USERDIR}/Conservation_n-mers"

The analyses were run centered on two different genomes (human and mouse) and
could be intended for two different seed definitions (positions 2-7 and 2-8 of
the mature miRNA). Actually, only the seed length is relevant here.

    # Set default values:
    # To override, either edit here or set when calling `bash` (see example later).
    # Set this to either "hg19" or "mm10"
    [[ ${GENOME} ]] || GENOME="hg19"
    [[ ${LEN} ]] || LEN="6"
    # Set the location of this file and other distributed files:
    # http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
    # PACKAGEDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    # The above trick only works if the commands are run from a script
    # present in the same directory as the distributed files.
    # Otherwise, set the location manually:
    [[ ${PACKAGEDIR} ]] || PACKAGEDIR="${USERDIR}/Conservation_n-mers_packaged"
    # You can set these as environment variables
    # when you tell bash to execute the code in this README
    # Example (extracting code and piping to `bash` with environment variables set):
    #cat README_scan_maf_for_conservation.md \
    #    | grep "^    " | sed 's/^    //' \
    #    | BASEDIR="${HOME}/Conservation_n-mers" PACKAGEDIR="${HOME}/Conservation_n-mers_packaged" GENOME="hg19" LEN="7" bash
    set -u


Data obtention
==============

MAF genome alignments are available from UCSC. These are centered around one
species.

We work using version "hg19" of the human genome or version "mm10" of the mouse
genome because MAF alignements were available from UCSC for these genome
versions at the time this work was done.

UCSC genome alignments
----------------------

    MAF_DIR=${BASEDIR}/Maf_alignments/${GENOME}
    mkdir -p ${MAF_DIR}
    cd ${MAF_DIR}

### Getting the .maf alignments

Determine the maximum chromosome number:

    # TODO: Why http or ftp ? Could we add more genomes ?
    case ${GENOME} in
        "hg19")
            chroms=`seq 1 22`" X Y M"
            base_URL="http://hgdownload.soe.ucsc.edu/goldenPath/hg19/multiz100way/maf"
            ;;
        "mm10")
            chroms=`seq 1 19`" X Y M"
            base_URL="ftp://hgdownload.cse.ucsc.edu/goldenPath/mm10/multiz60way/maf"
            ;;
        "dm6")
            chroms="2L 2R 3L 3R 4 X Y M"
            base_URL="ftp://hgdownload.cse.ucsc.edu/goldenPath/dm6/multiz27way/maf"
            ;;
        *)
            echo -e "\nLocation of alignment data for ${GENOME} unknow\nAborting\n"
            exit 1
            ;;
    esac

We might not use the mitochondrial chomosome because RefSeq data is not available
for mitochondria (see
<http://redmine.soe.ucsc.edu/forum/index.php?t=msg&goto=11996&S=870dde03873bcf78dfa08313a2c6fe63>)


    for chr in ${chroms}
    do
        # Do not download if a symbolic link can be made
        # Would it be better to make a hard link?
        [[ -e ${USERDIR}/Pan-vertebrate_alignment_${GENOME}_centered/chr${chr}.maf.gz ]] && ln -sf ${USERDIR}/Pan-vertebrate_alignment_${GENOME}_centered/chr${chr}.maf.gz .
        [[ -e chr${chr}.maf.gz ]] || wget -t 0 -nv --continue "${base_URL}/chr${chr}.maf.gz"
    done



UCSC genome sequences
---------------------

We might later extract 3-prime UTR sequences from the fasta-formatted genome.

    GENOME_DIR="${BASEDIR}/UCSC_genomes/${GENOME}"
    mkdir -p "${BASEDIR}/UCSC_genomes"
    cd ${BASEDIR}/UCSC_genomes
    # Do not download if a link can be made
    [[ -e ${USERDIR}/UCSC_genomes/${GENOME} ]] && ln -sf ${USERDIR}/UCSC_genomes/${GENOME} .
    [[ -e ${GENOME_DIR} ]] || mkdir -p ${GENOME_DIR}
    cd ${GENOME_DIR}
    case ${GENOME} in
        "hg19")
            fastagz="chromFa.tar.gz"
            ;;
        "mm10")
            fastagz="chromFa.tar.gz"
            ;;
        "dm6")
            fastagz="dm6.fa.gz"
            ;;
        *)
            echo -e "\nLocation of sequence data for ${GENOME} unknow\nAborting\n"
            exit 1
            ;;
    esac
    [[ -e ${fastagz} ]] || wget -t 0 -nv --continue http://hgdownload.soe.ucsc.edu/goldenPath/${GENOME}/bigZips/${fastagz}

### List the standard and "clean" chromosomes and extract them

    # TODO: This will need to be adapted if more genomes are added
    case ${GENOME} in
        "hg19")
            tar -tzf ${fastagz} \
                | grep -v "gl" | grep -v "hap" \
                | grep -v "GL" | grep -v "JH" \
                > chromosome_selection.txt    
            for chr in ${chroms}
            do
                [[ -e chr${chr}.fa ]] || tar -xvzf ${fastagz} chr${chr}.fa
            done    
            ;;
        "mm10")
            tar -tzf ${fastagz} \
                | grep -v "gl" | grep -v "hap" \
                | grep -v "GL" | grep -v "JH" \
                > chromosome_selection.txt    
            for chr in ${chroms}
            do
                [[ -e chr${chr}.fa ]] || tar -xvzf ${fastagz} chr${chr}.fa
            done    
            ;;
        "dm6")
            for chr in ${chroms}
            do
                [[ -e chr${chr}.fa ]] || echo chr${chr}
            done > chromosome_selection.txt
            extract_fasta.py -s chromosome_selection.txt ${fastagz}
            ;;
        *)
            echo -e "\nLocation of sequence data for ${GENOME} unknow\nAborting\n"
            exit 1
            ;;
    esac

### Extract the chromosomes we want


RefSeq annotations
------------------

We might later use RefSeq gene annotations to know 3-prime UTR coordinates.

    RefSeq_DIR="${BASEDIR}/RefSeq/${GENOME}"
    mkdir -p "${BASEDIR}/RefSeq"
    cd ${BASEDIR}/RefSeq
    # Do not download if a link can be made
    [[ -e ${USERDIR}/RefSeq/${GENOME} ]] && ln -sf ${USERDIR}/RefSeq/${GENOME} .
    [[ -e ${RefSeq_DIR} ]] || mkdir -p ${RefSeq_DIR}
    cd ${RefSeq_DIR}
    [[ -e refGene.txt.gz ]] || wget -t 0 -nv --continue http://hgdownload.soe.ucsc.edu/goldenPath/${GENOME}/database/refGene.txt.gz
    [[ -e refGene.txt ]] || gunzip refGene.txt.gz



Searching for n-mer conservation in the .maf alignment
------------------------------------------------------

We want to scan the maf alignments obtained from UCSC and for each n-mer in the
sequence of the reference genome, record for which genomes this n-mer is also
present.



### Checking that all alignment blocks have the reference genome in the "+" strand

Our scripts assume that all alignment blocks in the MAF files have the
reference genome on the "+" strand.

This can be checked as follows.

    # This takes a long time
    #for chr in ${chroms}
    #do
    #    zcat ${MAF_DIR}/chr${chr}.maf.gz \
    #        | awk -v chr="${chr}" -v genome="${GENOME}" '$1=="s" && $2==genome".chr"chr {print $5}' | uniq
    #done \
    #    | uniq

The above commands should return just "+". It was the case for "mm10" and
"hg19".

### Get conservation information for the seeds and non-seeds

    results_dir="${BASEDIR}/${GENOME}_${LEN}-mer"
    mkdir -p ${results_dir}
    cd ${results_dir}
    if [[ ${CHROM} ]]
    then
        chr=${CHROM}
        echo "Scanning chr${chr} for ${LEN}-mer conservation"
        #time ${PACKAGEDIR}/scan_maf2conservation.py \
        #    -m ${MAF_DIR}/chr${chr}.maf.gz \
        #    -r ${GENOME} \
        #    -l ${LEN} \
        #    | grep -v "^#" | bgzip \
        #    > chr${chr}_${LEN}-mer_conservation.bed.gz
        time ${PACKAGEDIR}/scan_maf2conservation.py \
            -m ${MAF_DIR}/chr${chr}.maf.gz \
            -r ${GENOME} \
            -l ${LEN} \
            | grep -v "^#" | bgzip \
            > chr${chr}_${LEN}-mer_conservation.bed.gz
        #bgzip chr${chr}_${LEN}-mer_conservation.bed
        size=$( echo "`stat -c "%s" chr${chr}_${LEN}-mer_conservation.bed.gz` / (1024 * 1024)" | bc )
        echo "chr${chr}_${LEN}-mer_conservation.bed.gz: ${size} MB"
        echo "tabix indexing chr${chr}_${LEN}-mer_conservation.bed.gz"
        tabix -p bed chr${chr}_${LEN}-mer_conservation.bed.gz
    else
        for chr in ${chroms}
        do
            echo "Scanning chr${chr} for ${LEN}-mer conservation"
            time ${PACKAGEDIR}/scan_maf2conservation.py \
                -m ${MAF_DIR}/chr${chr}.maf.gz \
                -r ${GENOME} \
                -l ${LEN} \
                | grep -v "^#" | bgzip \
                > chr${chr}_${LEN}-mer_conservation.bed.gz
            size=$( echo "`stat -c "%s" chr${chr}_${LEN}-mer_conservation.bed.gz` / (1024 * 1024)" | bc )
            echo "chr${chr}_${LEN}-mer_conservation.bed.gz: ${size} MB"
            echo "tabix indexing chr${chr}_${LEN}-mer_conservation.bed.gz"
            tabix -p bed chr${chr}_${LEN}-mer_conservation.bed.gz
        done
    fi



