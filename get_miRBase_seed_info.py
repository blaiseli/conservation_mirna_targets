#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script extracts seed information from a miRBase mature miRNA fasta file
and creates a database to store the information. It also generates a list of
seeds that are present in the miRBase data in at least one species present in
the UCSC alignment."""

import argparse
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from os.path import splitext
from re import sub
from string import maketrans, split
RNA2DNA_TABLE = maketrans("U", "T")

def to_dna(seq):
    """Converts an upper-case RNA sequence into an upper-case DNA sequence.
    Actually, this just substitutes Us for Ts."""
    return seq.translate(RNA2DNA_TABLE)

from Bio import SeqIO
#from sqlite3 import connect, IntegrityError
from sqlite3 import connect
# To store dictionaries
from cPickle import dump, HIGHEST_PROTOCOL


class Globals(object):
    """Class used to hold global variables as its class attributes."""
    # Dictionary to convert miRBase species codes into NCBI taxon IDs
    mirtax2taxid = {}
    taxid2mirtax = {}
    # Dictionary to convert UCSC genome codes into NCBI taxon IDs
    ucsc2taxid = {}
    taxid2ucsc = {}
    ## Dictionary to convert some species Latin names
    ## from UCSC version to miRBase version.
    #UCSC_lat2lat = {
    #    "Canis lupus familiaris" : "Canis familiaris",
    #    "Gorilla gorilla gorilla" : "Gorilla gorilla",
    #    "Pongo pygmaeus abelii" : "Pongo pygmaeus",
    #    "Takifugu rubripes" : "Fugu rubripes"}
    @staticmethod
    def store_dicts(pickle_file_name):
        """Use cPickle.load to retrieve the dictionaries."""
        with open(pickle_file_name, "wb") as pickle_file:
            dump(Globals.mirtax2taxid, pickle_file, HIGHEST_PROTOCOL)
            dump(Globals.taxid2mirtax, pickle_file, HIGHEST_PROTOCOL)
            dump(Globals.ucsc2taxid, pickle_file, HIGHEST_PROTOCOL)
            dump(Globals.taxid2ucsc, pickle_file, HIGHEST_PROTOCOL)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #######################
    # NCBI taxonomy input #
    #######################
    parser.add_argument(
        "-n", "--ncbi_name2id",
        required=True,
        help="File associating NCBI taxonomy IDs to Latin names.",
        type=argparse.FileType("r"))
    #################
    # miRBase input #
    #################
    parser.add_argument(
        "-o", "--organisms",
        required=True,
        help="miRBase file containing organisms information.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-m", "--mir_fasta",
        required=True,
        help="File in .fasta format containing the mature miRNA sequences.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-c", "--high_conf_mir_fasta",
        required=True,
        help="File in .fasta format containing the mature miRNA sequences "
        "for the high confidence miRNA only.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "--seed_coords",
        help="Space-separated 1-based start and end positions of the seed "
        "in the mature miRNA sequence.",
        nargs=2,
        type=int,
        default=(2, 8))
    ############################
    # UCSC genome naming input #
    ############################
    parser.add_argument(
        "-u", "--ucsc_genome_info",
        required=True,
        help="File associating species Latin names "
        "to UCSC genome abbreviations.",
        type=argparse.FileType("r"))
    ##########
    # output #
    ##########
    parser.add_argument(
        "-d", "--database",
        required=True,
        help="Name of the sqlite file in which to write seed information "
        "in the form of a database. A pickle file with the same prefix "
        "will also be generated and will store dictionaries.")
    parser.add_argument(
        "-s", "--seeds_list",
        required=True,
        help="File in which to write the list of seeds found in the input.",
        type=argparse.FileType("w"))
    parser.add_argument(
        "-t", "--taxa",
        required=True,
        help="File in which to write the list of species codes "
        "found in the input.",
        type=argparse.FileType("w"))
    args = parser.parse_args()
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip

    WRITE("\nReading NCBI data...\n")
    # key: Taxon name from NCBI
    # value: NCBI taxid
    name2taxid = dict([split(strip(line), "\t") for line in args.ncbi_name2id])

    with connect(args.database) as seed_db:
        seed_db.execute("PRAGMA foreign_keys = ON")
        seed_db.execute("PRAGMA synchronous = OFF")
        # http://stackoverflow.com/questions/27160987/sqlite-python-sqlite3-operationalerror-database-is-locked
        # Wait 600s if database is busy
        seed_db.execute("PRAGMA busy_timeout = 600000")
        #####################
        # Making taxa table #
        #####################
        WRITE("\nCreating taxa table...\n")
        # Headers in args.organisms are
        #organism   #division   #name   #tree   #NCBI-taxid
        seed_db.execute(
            "create table if not exists taxa "
            "(taxid integer primary key, "
            "genome_code text, "
            "mirtax text, "
            "name text, "
            "taxonomy text)")
        mir_taxids = set([])
        with args.organisms as input_file:
            WRITE("\nParsing organisms information from miRBase...\n")
            # Skip header
            header = input_file.readline()
            assert header.startswith("#")
            # Fill the table with the rest of the file
            for line in input_file:
                mirtax, division, name, tree, taxid = split(strip(line), "\t")
                if name not in name2taxid:
                    msg = "".join([
                        "\nIgnoring %s. " % name,
                        "ID %s not found in NCBI taxonomy.\n" % taxid])
                    warnings.warn(msg)
                    continue
                if name2taxid[name] != taxid:
                    msg = "".join([
                        "\nIgnoring %s " % name,
                        "which has 2 different IDs: ",
                        "%s and %s\n" % (taxid, name2taxid[name])])
                    warnings.warn(msg)
                    continue
                #assert name2taxid[name] == taxid
                Globals.mirtax2taxid[mirtax] = taxid
                Globals.taxid2mirtax[taxid] = mirtax
                mir_taxids.add(taxid)
                # genome_code may be update later
                seed_db.execute(
                    "insert into taxa(taxid, mirtax, name, taxonomy) values "
                    "(%s, \"%s\", \"%s\", \"%s\")" % (
                        taxid,
                        mirtax,
                        name,
                        # "/" seems to more appropriate for a hierarchy and it
                        # might be not good to have ";" in SQL database fields
                        sub(";", "/", tree)))


        # Build Globals.ucsc2taxid trying to find
        # the ID matching one in Globals.mir2taxid
        def try_insertion(name, genome_code):
            """This function tries find the NCBI taxonomy ID corresponding to
            the Latin *name* associated with *genome_code*. If the taxonomy ID
            has already been found for miRBase data, the association between
            this ID and the *genome_code* is registered in a global dictionary
            and the table taxa is updated accordingly. Else, the function
            returns False."""
            taxid = name2taxid[name]
            if taxid in mir_taxids:
                Globals.ucsc2taxid[genome_code] = taxid
                Globals.taxid2ucsc[taxid] = genome_code
                # Update table taxa to add something
                # in the genome_code column
                seed_db.execute(
                    "update taxa set genome_code = \"%s\" "
                    "where taxid == %s" % (genome_code, taxid))
                return True
            else:
                return False

        with args.ucsc_genome_info as input_file:
            WRITE("\nParsing genome codes information from UCSC...\n")
            for line in input_file:
                name, genome_code = split(strip(line), "\t")
                if try_insertion(name, genome_code):
                    # We could insert genome_code in the table
                    continue
                else:
                    inserted = False
                    # Try removing supspecies names and the like
                    shorter_name = name
                    name_items = split(shorter_name, " ")
                    while len(name_items) > 2:
                        shorter_name = " ".join(name_items[:-1])
                        inserted = try_insertion(shorter_name, genome_code)
                        if inserted:
                            break
                        else:
                            name_items = split(shorter_name, " ")
                    if inserted:
                        # We could insert genome_code in the table
                        continue
                    else:
                        # This might be a taxon absent from miRBase
                        taxid = name2taxid[name]
                        Globals.ucsc2taxid[genome_code] = taxid
                        Globals.taxid2ucsc[taxid] = genome_code
                        # Entry without mirtax data
                        seed_db.execute(
                            "insert into "
                            "taxa(taxid, genome_code, name) "
                            "values (%s, \"%s\", \"%s\")" %  (
                                taxid,
                                genome_code,
                                name))

        # TODO: Space could be gained by cleaning the dictionaries first.
        # Indeed, some taxids are in miRBase data, but not in UCSC data.
        # We are not interested in these species.
        # Cleaning could be done during the seeds list determination.
        dict_file_name = "%s.pickle" % splitext(args.database)[0]
        WRITE("\nPickling dictionaries in %s ...\n" % dict_file_name)
        Globals.store_dicts(dict_file_name)

        ######################
        # Making seeds table #
        ######################
        WRITE("\nCreating seeds table...\n")
        # high_conf is 1 if high_conf, else 0
        seed_db.execute(
            "create table if not exists seeds "
            "(seed text primary key, high_conf integer)")
        # Determine the seeds belonging to high confidence miRNAs
        # TODO: What if it is high_conf in a species but not in another ?
        seed_start = args.seed_coords[0] - 1
        seed_end = args.seed_coords[1]
        high_conf = set([])
        with args.high_conf_mir_fasta as input_file:
            for record in SeqIO.parse(input_file, "fasta"):
                seq_name = split(record.name)[0]
                if not seq_name:
                    seq_name = split(record.id)[0]
                mirtax = split(seq_name, "-")[0]
                #species.add(mirtax)
                #seed = to_dna(str(record.seq.upper())[1:8])
                seed = to_dna(str(record.seq.upper())[seed_start:seed_end])
                high_conf.add(seed)
        ##########################
        # Making seed_taxa table #
        ##########################
        seed_db.execute(
            "create table if not exists seed_taxa "
            "(seed text, taxid integer, from_miRBase integer, "
            "primary key (seed, taxid), "
            "foreign key(seed) references seeds(seed), "
            "foreign key(taxid) references taxa(taxid))")
        species = set([])
        seeds = set([])
        with args.mir_fasta as input_file:
            WRITE("\nParsing miRBase mature miRNA sequences...\n")
            for record in SeqIO.parse(input_file, "fasta"):
                seq_name = split(record.name)[0]
                if not seq_name:
                    seq_name = split(record.id)[0]
                mirtax = split(seq_name, "-")[0]
                if mirtax not in Globals.mirtax2taxid:
                    # This taxon should have been skipped
                    # when constructing the taxa table.
                    # Skip this record.
                    continue
                taxid = Globals.mirtax2taxid[mirtax]
                if taxid not in Globals.taxid2ucsc:
                    # Delete entry in taxa table, because we will not use
                    # a species not present in the UCSC alignment.
                    seed_db.execute(
                        "delete from taxa where taxid == %s" % taxid)
                else:
                    # We will work with seeds in DNA format
                    seed = to_dna(str(record.seq.upper())[seed_start:seed_end])
                    seeds.add(seed)
                    # We use "or ignore" because we may encounter
                    # a given seed more than once, and that's normal.
                    if seed in high_conf:
                        seed_db.execute(
                            "insert or ignore into seeds(seed, high_conf) "
                            "values (\"%s\", 1)" % seed)
                    else:
                        seed_db.execute(
                            "insert or ignore into seeds(seed, high_conf) "
                            "values (\"%s\", 0)" % seed)
                    # Now we are sure the seed is in the seeds table.
                    # We can reference it from the seed_taxa table.
                    species.add(mirtax)
                    # It is normal to have the same (seed, taxid) pair
                    # appear more than once: Several miRNAs may share seeds.
                    # Hence the "or ignore".
                    seed_db.execute(
                        "insert or ignore into "
                        "seed_taxa(seed, taxid, from_mirBase) "
                        "values (\"%s\", \"%s\", 1)" % (
                            seed,
                            Globals.mirtax2taxid[mirtax]))
                    #try:
                    #    seed_db.execute(
                    #        "insert into seed_taxa(seed, taxid) values "
                    #        "(\"%s\", \"%s\")" % (
                    #            seed, Globals.mirtax2taxid[mirtax]))
                    #except IntegrityError:
                    #    # It is normal to have the same (seed, taxid) pair
                    #    # appear more than once:
                    #    # Several miRNAs may share seeds.
                    #    pass
                    #    #warnings.warn(
                    #    #    "taxon %s already associated to "
                    #    #    "seed %s\n" % (mirtax, seed))
            # Compacting the database after deletinons fro `taxa`.
            seed_db.execute("VACUUM")
            seed_db.commit()
            WRITE("\nWriting miRBase species and seeds...\n")
            args.taxa.write("\n".join(sorted(species)))
            args.taxa.write("\n")
            args.seeds_list.write("\n".join(sorted(seeds)))
            args.seeds_list.write("\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
