#!/bin/bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib
# (Not sure it works)

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="2.3.3"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf pigz-${version}*
# Download it
wget http://www.zlib.net/pigz/pigz-${version}.tar.gz || error_exit "pigz download failed"
tar -xvzf pigz-${version}.tar.gz
cd pigz-${version}
# Trying to optimize
sed -i 's|^CFLAGS\(.*\)$|CFLAGS\1 -march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib|g' Makefile
LDFLAGS="-Wl,-rpath,${HOME}/lib -I${HOME}/include -L${HOME}/lib" make test || error_exit "pigz build failed"
ln -sf ${HOME}/src/pigz-${version}/pigz ${HOME}/bin/. || error_exit "pigz install failed"
ln -sf ${HOME}/src/pigz-${version}/unpigz ${HOME}/bin/. || error_exit "pigz install failed"

exit 0
