Getting a biopython version that includes a MAF alignement parser
=================================================================

Seed match conservation is estimated from a multiple genome alignment built by
UCSC and provided in the MAF format.

Until the official biopython release includes my changes to the unofficial MAF
parser (pull request [504](https://github.com/biopython/biopython/pull/504)),
you need to install my fork of biopython.

    # http://askubuntu.com/questions/588390/how-do-i-check-whether-a-module-is-installed-or-not-in-python
    python -c "from Bio.AlignIO.MafIO import MafIndex" \
        1> /dev/null \
        2> /dev/null
    # Test the return code of the command.
    # 0 means success, something else means failure.
    if (($?))
    then
        echo "It seems that the needed MAF parser is not installed."
        echo "Trying to install it."
        # Clone my fork using git
        git clone git@github.com:blaiseli/biopython.git
        # Switch to the alignio-maf branch
        git checkout alignio-maf
        # Install biopython locally
        python setup.py install --user
        python -c "from Bio.AlignIO.MafIO import MafIndex" \
            1> /dev/null \
            2> /dev/null
        if (($?))
        then
            echo "Still no MAF parser available. Expect problems."
        fi
    fi

For a locally-installed version to be taken into account it seems that your
`~/.local/lib/python2.7/site-packages` directory should be before
`/usr/lib/pymodules/python2.7` in `sys.path` (within python).


Initial settings
================

All analyses will happen relative to a base directory.

    # To override, either edit here or set when calling `bash` (see example later).
    [[ ${BASEDIR} ]] || BASEDIR="${HOME}/Conservation_miRNA_targets"

The analyses were run centered on two different genomes (human and mouse) and
for two different seed definitions (positions 2-7 and 2-8 of the mature miRNA).
This README file shows how to perform the analyses for one genome x seed
definition combination.

    # Set default values:
    # To override, either edit here or set when calling `bash` (see example later).
    # Set this to either "hg19" or "mm10"
    [[ ${GENOME} ]] || GENOME="hg19"
    # set this to either "2-7" or "2-8"
    [[ ${SEED} ]] || SEED="2-7"
    # Set the location of this file and other distributed files:
    # http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
    # PACKAGEDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    # The above trick only works if the commands are run from a script
    # present in the same directory as the distributed files.
    # Otherwise, set the location manually:
    [[ ${PACKAGEDIR} ]] || PACKAGEDIR="${HOME}/Conservation_miRNA_targets_packaged"
    # You can set these as environment variables
    # when you tell bash to execute the code in this README
    # Example (extracting code and piping to `bash` with environment variables set):
    #cat README_miRNA_target_conservation.md \
    #    | grep "^    " | sed 's/^    //' \
    #    | BASEDIR="${HOME}/Conservation_miRNA_targets" PACKAGEDIR="${HOME}/Conservation_miRNA_targets_packaged" GENOME="hg19" SEED="2-8" bash



Data obtention
==============

MAF genome alignments are available from UCSC. These are centered around one
species.

We work using version "hg19" of the human genome or version "mm10" of the mouse
genome because MAF alignements were available from UCSC for these genome
versions at the time this work was done.

UCSC genome alignments
----------------------

    MAF_DIR=${BASEDIR}/Maf_alignments/${GENOME}
    mkdir -p ${MAF_DIR}
    cd ${MAF_DIR}

### Getting information about the genomes used in the alignment

    case ${GENOME} in
        "hg19")
            URL="http://hgdownload.cse.ucsc.edu/goldenPath/${GENOME}/multiz100way/README.txt"
            select_lines="head -124 | tail -114"
            ;;
        "mm10")
            URL="ftp://hgdownload.cse.ucsc.edu/goldenPath/${GENOME}/multiz60way/README.txt"
            select_lines="head -84 | tail -75"
            ;;
        *)
            echo -e "\nLocation of alignment data for ${GENOME} unknow\nAborting\n"
            exit 1
            ;;
    esac
    # We provide an already edited `UCSC_species2genome_${GENOME}.txt` file
    # You can use it:
    if [[ -e ${PACKAGEDIR}/UCSC_species2genome_${GENOME}.txt ]]
    then
        cp ${PACKAGEDIR}/UCSC_species2genome_${GENOME}.txt .
    else
        wget -t 0 -nv --continue ${URL}
        cat README.txt | ${select_lines} \
            | grep -v "^== " | grep -v "^$" \
            > UCSC_species2genome_${GENOME}.txt
    fi

If you did not use the provided `UCSC_species2genome_${GENOME}.txt` file,
manually-edit the one pre-generated using the above commands to make a
tab-separated file with two columns, the first with Latin names, the second
with genome codes as in the .maf files.



Note that there are Latin name differences between these data and miRBase data.

The following correspondences were detected:

| UCSC Latin name           | miRBase Latin name |
| ------------------------- | ------------------ |
| *Canis lupus familiaris*  | *Canis familiaris* |
| *Gorilla gorilla gorilla* | *Gorilla gorilla*  |
| *Pongo pygmaeus abelii*   | *Pongo pygmaeus*   |
| *Takifugu rubripes*       | *Fugu rubripes*    |

The script `get_miRBase_seed_info.py` tries to handle this correctly, using
NCBI taxonomy IDs.

### Getting the .maf alignments

Determine the maximum chromosome number:

    case ${GENOME} in
        "hg19")
            max_chr="22"
            base_URL="http://hgdownload.soe.ucsc.edu/goldenPath/hg19/multiz100way/maf"
            ;;
        "mm10")
            max_chr="19"
            base_URL="ftp://hgdownload.cse.ucsc.edu/goldenPath/mm10/multiz60way/maf"
            ;;
        *)
            echo -e "\nLocation of alignment data for ${GENOME} unknow\nAborting\n"
            exit 1
            ;;
    esac

We do not use the mitochondrial chomosome because RefSeq data is not available
for mitochondria (see
<http://redmine.soe.ucsc.edu/forum/index.php?t=msg&goto=11996&S=870dde03873bcf78dfa08313a2c6fe63>)


    for chr in `seq 1 ${max_chr}` X Y
    do
        wget -t 0 -nv --continue "${base_URL}/chr${chr}.maf.gz"
    done

### Extract lists of species names for each chromosome

    for chr in `seq 1 ${max_chr}` X Y
    do
        zcat chr${chr}.maf.gz | awk '$1=="s" {print $2}' \
            | awk -F "." '{print $1}' \
            | sort -u > chr${chr}_species.txt
    done

### Check that the lists are the same

    # Count number of distinct md5sum values for lists of species
    nb_sums=$(md5sum chr*_species.txt | awk '{print $1}' | sort -u | wc -l)
    # The lists of species have been sorted, so (same species) <=> (same md5sum).
    if [[ ${nb_sums} != "1" ]]
    then
        echo -e "\nNot all chromosome alignments have the same species.\nAborting\n"
        exit 1
    fi

### Keep one and discard the others

    mv chrY_species.txt UCSC_genomes.txt
    rm -f chr*_species.txt



UCSC genome sequences
---------------------

We will extract 3-prime UTR sequences from the fasta-formatted genome.

    GENOME_DIR="${BASEDIR}/UCSC_genomes/${GENOME}"
    mkdir -p ${GENOME_DIR}
    cd ${GENOME_DIR}
    wget -t 0 -nv --continue http://hgdownload.soe.ucsc.edu/goldenPath/${GENOME}/bigZips/chromFa.tar.gz

### List the standard and "clean" chromosomes

    tar -tzf chromFa.tar.gz \
        | grep -v "gl" | grep -v "hap" \
        | grep -v "GL" | grep -v "JH" \
        > chromosome_selection.txt    

### Extract the chromosomes we want

    for chr in `seq 1 ${max_chr}` X Y
    do
        tar -xvzf chromFa.tar.gz chr${chr}.fa
    done    


RefSeq annotations
------------------

We use RefSeq gene annotations to know 3-prime UTR coordinates.

    RefSeq_DIR="${BASEDIR}/RefSeq/${GENOME}"
    mkdir -p ${RefSeq_DIR}
    cd ${RefSeq_DIR}
    wget -t 0 -nv --continue http://hgdownload.soe.ucsc.edu/goldenPath/${GENOME}/database/refGene.txt.gz
    gunzip refGene.txt.gz


NCBI taxonomy data
------------------

We use taxonomy information from NCBI to match miRBase data with UCSC data.

    NCBI_DIR="${BASEDIR}/NCBI_data"
    mkdir -p ${NCBI_DIR}
    cd ${NCBI_DIR}
    wget -t 0 -nv --continue ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump_readme.txt
    wget -t 0 -nv --continue ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
    tar -xvzf taxdump.tar.gz

### Make files associating NCBI taxid to a name

    awk -F "\t" '{print $3"\t"$1}' names.dmp | sort -u > name2taxid.txt
    awk -F "\t" '$7=="scientific name"{print $1"\t"$3}' names.dmp > taxid2name.txt


These files will be used when parsing miRBase data with the script
`get_miRBase_seed_info.py`.

Note that several names can be associated to the same taxid.

Problem: taxid are not always the same for the correspondences detected
previously:


| Name                      | taxid |
| ------------------------- | -----:| 
| *Canis familiaris*        |  9615 |
| *Canis lupus familiaris*  |  9615 |
| *Gorilla gorilla*         |  9593 |
| *Gorilla gorilla gorilla* |  9595 |
| *Pongo pygmaeus*          |  9600 |
| *Pongo pygmaeus abelii*   |  9601 |
| *Takifugu rubripes*       | 31033 |
| *Fugu rubripes*           | 31033 |

Decision rule to make the `taxa` table in the sqlite3 database created by `get_miRBase_seed_info.py`:

* Record the available miRBase `taxid` if information from miRBase and NCBI are coherent.

* If the UCSC name has a `taxid` found in miRBase, update the record with the corresponding `genome_code`.

* Else try with the `taxid` obtained with a broader taxonomy (for instance, remove the last "gorilla"), broadening until the *Genus species* binome.

* If no `taxid` already recorded is found, make a new record in the table.


mirBase data
------------

We will use every miRNA from miRBase, but we will annotate specifically "high confidence" miRNAs, just in case we need that info.

    miRBase_DIR="${BASEDIR}/miRBase_data"
    mkdir -p ${miRBase_DIR}
    cd ${miRBase_DIR}
    wget -t 0 -nv --continue ftp://mirbase.org/pub/mirbase/21/organisms.txt.gz
    wget -t 0 -nv --continue ftp://mirbase.org/pub/mirbase/21/high_conf_mature.fa.gz
    wget -t 0 -nv --continue ftp://mirbase.org/pub/mirbase/21/mature.fa.gz

### Generate lists of organisms

    zcat organisms.txt.gz > organisms.txt

### Extract seeds

    # We start outputting results, so we create a directory for this
    results_DIR="${BASEDIR}/${GENOME}_seed_${SEED}"
    mkdir -p ${results_DIR}
    coords=$(echo ${SEED} | tr "-" " ")
    zcat high_conf_mature.fa.gz > high_conf_mature.fa
    zcat mature.fa.gz > mature.fa
    ${PACKAGEDIR}/get_miRBase_seed_info.py \
        -m mature.fa \
        -c high_conf_mature.fa \
        --seed_coords ${coords} \
        -n ${NCBI_DIR}/name2taxid.txt \
        -o organisms.txt \
        -u ${MAF_DIR}/UCSC_species2genome_${GENOME}.txt \
        -d ${results_DIR}/sites.sqlite \
        -s seeds_${GENOME}_seed_${SEED}.txt \
        -t miRBase_species_${GENOME}_seed_${SEED}.txt

Options `-d`, `-s` and `-t` define the output files. Adapt the names in order
to avoid overwriting the files for other analyses.

Along with the lists of seeds and taxa and the sqlite3 database, python
dictionaries for converting between miRBase species codes or UCSC genome names
and NCBI taxonomy IDs have been pickled in `${results_DIR}/sites.pickle`.

We now have a sqlite3 database with `taxa`, `seeds` and `seed_taxa` tables. We
can consult it from python. For instance, if we want to know what are the UCSC
genome codes for the species having a miRNA with seed "TTTTTTT", the python
commands would be (assuming the current working directory is ${results_DIR}):

```python
from sqlite3 import connect
db = connect("sites.sqlite")
print "\n".join(map(str, db.execute("select genome_code from taxa where taxid in (select taxid from seed_taxa where seed == \"TTTTTTT\")").fetchall()))
```

The result should be `(u'danRer7',)` if `${SEED}` is "2-8".


### Making the list of non-seeds

    # -l option is to specify seed length
    # Appending "+1" to ${SEED} and reversing the string
    # creates the seed length formula (e.g.: "2-8" -> "1+8-2"),
    # which is piped to the `bc` calculator. This is a hack...
    # The length is used to compute all possible k-mers,
    # and compare them with existing seeds of llength k.
    ${PACKAGEDIR}/make_no_seed_list.py \
        -s seeds_${GENOME}_seed_${SEED}.txt \
        -l $(echo ${SEED}"+1" | rev | bc) \
        > non_seeds_${GENOME}_seed_${SEED}.txt


Seed localization in 3-prime UTRs
=================================

The idea is to make .bed files
(<http://genome.ucsc.edu/FAQ/FAQformat.html#format1>) for the 3-prime UTR exons
and for the seed locations, and then compute their intersection using
`bedtools` (<http://bedtools.readthedocs.org/en/latest/>). 

Then, the aligments for these seed locations will be looked in the .maf files
and the species for which the seed match is conserved with respect to the mouse
sequence will be registered.


3-prime UTR exon coordinates extraction
---------------------------------------

We will first identify the coordinates of 3-prime UTRs from
`${RefSeq_DIR}/refGene.txt`

We will use the following fields from `${RefSeq_DIR}/refGene.txt`:

* `name`: Name of gene (usually `transcript_id` from GTF)
* `chrom`: Reference sequence chromosome or scaffold
* `strand`: `+` or `-` for strand
* `txEnd`: Transcription end position
* `cdsEnd`: Coding region end
* `exonStarts`: Exon start positions
* `exonEnds`: Exon end positions

Note that the coordinates in this file are using the same conventions as in the
.bed format: starts are "inclusive" zero-based, but ends are "exclusive" (or,
equivalently, 1-based).

The .bed file will be used to extract and assemble 3-prime UTRs from .maf alignments.


Some CDS "ends" are not known for genes on the `+` strand and some CDS "starts"
are not known for genes on the `-` strand:

You may get an idea of the number of such cases by counting the number of "unk" status:

    awk  -F "\t" \
        'BEGIN {print "#status\tcount (+ strand)"} $4=="+" {histo[$15]++} END {for (status in histo) print status"\t"histo[status]}' \
        ${RefSeq_DIR}/refGene.txt
    awk  -F "\t" \
        'BEGIN {print "#status\tcount (- strand)"} $4=="-" {histo[$14]++} END {for (status in histo) print status"\t"histo[status]}' \
        ${RefSeq_DIR}/refGene.txt

We will work on the selection of genes whose 3-prime UTR start can be defined.

But some genes are on unreliable chromosomes. Also, there appears to be no
annotation corresponding to genes in chromosome M. We will restrict the
analyses to genes on reliable nuclear chromosomes.

    cd ${results_DIR}
    mkdir bed_UTRs
    for chr in `seq 1 ${max_chr}` X Y
    do
        ${PACKAGEDIR}/refseq2utrbed.py \
            --refseq_file ${RefSeq_DIR}/refGene.txt \
            --chromosome chr${chr} \
            --bed_all bed_UTRs/chr${chr}_3UTR_all.bed
    done


Searching for seed conservation in the .maf alignment
-----------------------------------------------------

We want to parse the portions of maf alignment obtained from UCSC and look for
seed matches in the sequence of the reference genome, then record for which
species the seed match is also present.


### Search for seed matches

We use structures similar to [tries](https://en.wikipedia.org/wiki/Trie) to do
this pattern matching using all seeds together.
These tries were implemented as recursive dictionaries in python.

We construct .bed files indicating seed matches in human genome.

    mkdir bed_seeds

#### Seeds from all miRNAs belonging to species for which the genome is present in the UCSC alignment

    for chr in `seq 1 ${max_chr}` X Y
    do
        ${PACKAGEDIR}/trie.py \
            ${miRBase_DIR}/seeds_${GENOME}_seed_${SEED}.txt \
            ${GENOME_DIR}/chr${chr}.fa \
            > bed_seeds/seeds_on_${GENOME}_chr${chr}.bed
    done


#### Non-seeds

    for chr in `seq 1 ${max_chr}` X Y
    do
        ${PACKAGEDIR}/trie.py \
            ${miRBase_DIR}/non_seeds_${GENOME}_seed_${SEED}.txt \
            ${GENOME_DIR}/chr${chr}.fa \
            > bed_seeds/non_seeds_on_${GENOME}_chr${chr}.bed
    done

#### Intersecting with 3-prime UTR coordinates

We intersect the beds of seed matches and 3UTR locations, to get the seed
matches in 3-prime UTRs. The goal is to use this to get just the interesting
portions of the maf alignment when looking for conserved seed matches.

    mkdir bed_seeds_in_UTRs

A seed should match in the oposite strand as the 3-prime UTR, hence the use of
the `-S` option of `bedtools intersect`. We do not want the seed matches on the
border of a 3-prim UTR exon, hence the use of the `-f 1.0` option. We do not
want seed positions duplicated, hence the use of the `-u` option.

We are ignoring the seeds that would match on a junction between two 3-prime
UTR exons (or on any post-transcriptionally edited portion of the mRNA that
would occur before miRNA intervention). If the seed matches are randomly
distributed (or at least have no bias for intron junctions location), such
ignored seed matches should be a minority.

#### For seeds

    for chr in `seq 1 ${max_chr}` X Y
    do
        bedtools intersect -S -f 1.0 -u \
            -a bed_seeds/seeds_on_${GENOME}_chr${chr}.bed \
            -b bed_UTRs/chr${chr}_3UTR_all.bed \
            > bed_seeds_in_UTRs/seeds_on_${GENOME}_chr${chr}_3UTR_all.bed
    done


#### For non-seeds

    for chr in `seq 1 ${max_chr}` X Y
    do
        bedtools intersect -S -f 1.0 -u \
            -a bed_seeds/non_seeds_on_${GENOME}_chr${chr}.bed \
            -b bed_UTRs/chr${chr}_3UTR_all.bed \
            > bed_seeds_in_UTRs/non_seeds_on_${GENOME}_chr${chr}_3UTR_all.bed
    done


The two sets (seed and non-seed) are disjoint, so we can process the seeds and
non-seeds in parallel without fearing interferences when writing the results.


#### Compressing the seeds location to save disk space

    tar -cv bed_seeds/ | lbzip2 > bed_seeds.tar.bz2 && rm -rf bed_seeds


### Extract the maf alignement corresponding to the 3-prime UTRs

#### Checking that all alignment blocks have the reference genome in the "+" strand

Our scripts assume that all alignment blocks in the MAF files have the
reference genome on the "+" strand.

This can be checked as follows.

    # This takes a long time
    #for chr in `seq 1 ${max_chr}` X Y
    #do
    #    zcat ${MAF_DIR}/chr${chr}.maf.gz \
    #        | awk -v chr="${chr}" -v genome="${GENOME}" '$1=="s" && $2==genome".chr"chr {print $5}' | uniq
    #done \
    #    | uniq

The above commands should return just "+". It was the case for "mm10" and
"hg19".

#### Get conservation information for the seeds and non-seeds

    mkdir sites_db
    sites_db_dir="sites_db/sites_in_UTR_all"
    mkdir ${sites_db_dir}
    for chr in `seq 1 ${max_chr}` X Y
    do
        zcat ${MAF_DIR}/chr${chr}.maf.gz > chr${chr}.maf
        # For seeds
        echo -e "\nchr${chr}" >> find_conserved_seeds_in_maf.log
        time ${PACKAGEDIR}/find_conserved_seeds_in_maf.py \
            -m chr${chr}.maf \
            -r ${GENOME} \
            -b bed_seeds_in_UTRs/seeds_on_${GENOME}_chr${chr}_3UTR_all.bed \
            -s sites.sqlite \
            -d ${sites_db_dir} &>> find_conserved_seeds_in_maf.log
        # For non-seeds
        echo -e "\nchr${chr}" >> find_conserved_non_seeds_in_maf.log
        time ${PACKAGEDIR}/find_conserved_seeds_in_maf.py \
            -m chr${chr}.maf \
            -r ${GENOME} \
            -b bed_seeds_in_UTRs/non_seeds_on_${GENOME}_chr${chr}_3UTR_all.bed \
            -s sites.sqlite \
            -d ${sites_db_dir} &>> find_conserved_non_seeds_in_maf.log
        # Save space
        rm -f chr${chr}.maf
    done


#### Database design

Optionally, the information obtained using `find_conserved_seeds_in_maf.py` can
be added to the `sites.sqlite` database, using option "--write_db".

This section briefly describes how the database is organized.

The tables are described with the following convention:

* In **bold** the primary keys.
* In *italics* the external keys.


1) `seeds`

**`seed`** | `high_conf`
---------- | ----------


2) `taxa`

**`taxid`** | `genome_code` | `mirtax` | `name` | taxonomy
----------- | ------------- | -------- | ------ | --------


3) `seed_taxa`

*`seed`* | *`taxid`* | `from_miRBase`
-------- | --------- | --------------


4) `seed_sites`

*`seed`* | **`site`** |
-------- | ---------- |


5) `site_taxa`

*`site`* | *`taxid`*
-------- | ---------


#### Example database query

Check that all sites are present in Mus musculus:

```python
from sqlite3 import connect
db = connect("sites.sqlite")
print "\n".join(map(str, db.execute("select * from site_taxa where site not in (select distinct site from site_taxa where taxid == 10090)").fetchall()))
```

This should print a blank line.

