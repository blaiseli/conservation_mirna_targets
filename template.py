#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This is a template python script.
Modify it to suit your needs."""

import argparse
import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


class Globals(object):
    """Class used to hold global variables as its class attributes."""

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("first_arg",
            help="This is a first positional argument. "
            "It must be an integer, either 0 or 1.",
            type=int, choices=[0, 1])
    parser.add_argument("input_file",
            help="This is a second positional argument. "
            "It must be a readable file.",
            type=argparse.FileType("r"))
    parser.add_argument("-t", "--test",
            help="This is an optional argument.", default="foo")
    parser.add_argument("-s", "--switch",
            help="This is an optional boolean switch.",
            action="store_true")
    args = parser.parse_args()
    sys.stdout.write("First positional argument is: %s\n" % args.first_arg)
    if args.switch:
        warnings.warn("This is an example warning message.\n")
        sys.stdout.write("The switch has been turned on "
                "on the command line.\n")
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    split = str.split
    with args.input_file as input_file:
        for line in input_file:
            fields = split(strip(line, "\n"), "\t")
    sys.stdout.write("The optional argument is: %s\n" % args.test)
    return 0

if __name__ == "__main__":
    sys.exit(main())
