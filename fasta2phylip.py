#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script converts a fasta alignment into a phylip-like one using p4 and
generates a dictionary that p4 can later use to restore original sequence
names."""

import argparse
import os
import sys
import warnings

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import p4
p4.var.phylipDataMaxNameLength = 64

#class Globals(object):
#    """Class used to hold global variables as its class attributes."""
#
def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_fasta",
        help="Input fasta file.")
    parser.add_argument(
        "out_phylip",
        help="Output phylip file.")
    args = parser.parse_args()

    p4.read(args.in_fasta)
    ali = p4.var.alignments[0]
    #ali = p4.var.sequenceLists[0]
    if os.path.isfile("p4_renameForPhylip_dict.py"):
        os.remove("p4_renameForPhylip_dict.py")
    ali.renameForPhylip(dictFName="p4_renameForPhylip_dict.py")
    ali.writePhylip(args.out_phylip, interleave=False, whitespaceSeparatesNames=False, flat=True)
    sys.stdout.write("The dictionnary allowing p4 to restore shortened "
                     "names is saved in module p4_renameForPhylip_dict.py\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
