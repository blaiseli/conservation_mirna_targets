Data obtention
==============

We work using version hg19 of the human genome (Reason: MAF alignments not
available for more recent version).


UCSC Human genome alignments
----------------------------

    mkdir -p ~/Blaise/Pan-vertebrate_alignment
    cd ~/Blaise/Pan-vertebrate_alignment

### Getting information about the genomes used in the alignment

    wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/multiz100way/README.txt
    cat README.txt | head -124 | tail -114 | grep -v "^== " | grep -v "^$" > UCSC_species2genome.txt

Then manually edit `UCSC_species2genome.txt` to make a tab-separated file with
two columns, the first with Latin names, the second with genome codes as in the
.maf files.

Save the result (to be commited in git repository):

    cp UCSC_species2genome.txt ~/Blaise/bin/UCSC_species2genome.txt

[//]: # (This problem is normally dealt with correctly by get_miRBase_seed_info.py

Manually search for Latin names differences between these data and miRBase data.
The following correspondences were detected:

| UCSC Latin name           | miRBase Latin name |
| ------------------------- | ------------------ |
| *Canis lupus familiaris*  | *Canis familiaris* |
| *Gorilla gorilla gorilla* | *Gorilla gorilla*  |
| *Pongo pygmaeus abelii*   | *Pongo pygmaeus*   |
| *Takifugu rubripes*       | *Fugu rubripes*    |

It is probably safer to use NCBI taxonomy IDs.)

### Getting the .maf alignments

    for chr in `seq 1 22` M X Y
    do
        wget "http://hgdownload.soe.ucsc.edu/goldenPath/hg19/multiz100way/maf/chr${chr}.maf.gz"
    done

### Making uncompressed versions

    for chr in `seq 1 22` X Y
    do
        zcat chr${chr}.maf.gz > chr${chr}.maf
    done

### Extract lists of species names

    for chr in `seq 1 22` X Y
    do
        awk '$1=="s" {print $2}' chr${chr}.maf | awk -F "." '{print $1}' | sort | uniq > chr${chr}_species.txt
    done

### Check that the lists are the same

    md5sum chr*_species.txt

Output:

    7ae0e4ec72da3e5af691c5977d7e35f3  chr10_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr11_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr12_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr13_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr14_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr15_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr16_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr17_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr18_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr19_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr1_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr20_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr21_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr22_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr2_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr3_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr4_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr5_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr6_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr7_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr8_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chr9_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chrX_species.txt
    7ae0e4ec72da3e5af691c5977d7e35f3  chrY_species.txt

### Keep one and discard the others

    mv chrY_species.txt UCSC_genomes.txt
    rm -f chr*_species.txt



UCSC Human genome
-----------------

We use `hg19` because it is the one used in the alignment provided by UCSC.

    mkdir -p ~/Blaise/UCSC_genomes
    cd ~/Blaise/UCSC_genomes
    mkdir H_sapiens
    cd H_sapiens
    wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/chromFa.tar.gz

### List the standard and "clean" chromosomes

    tar -tzf chromFa.tar.gz | grep -v "gl" | grep -v hap > chromosome_selection.txt    

### Extract the chromosomes we want

    for chr in `seq 1 22` M X Y
    do
        tar -xvzf chromFa.tar.gz chr${chr}.fa
    done    


RefSeq data
-----------

    mkdir -p ~/Blaise/RefSeq
    cd ~/Blaise/RefSeq
    wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/refGene.txt.gz
    gunzip refGene.txt.gz


NCBI taxonomy data
------------------

    mkdir -p ~/Blaise/NCBI_data
    cd ~/Blaise/NCBI_data
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump_readme.txt
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
    tar -xvzf taxdump.tar.gz

Output:

    citations.dmp
    delnodes.dmp
    division.dmp
    gc.prt
    gencode.dmp
    merged.dmp
    names.dmp
    nodes.dmp
    readme.txt

### Make files associating NCBI taxid to a name

    awk -F "\t" '{print $3"\t"$1}' names.dmp | sort -u > name2taxid.txt
    awk -F "\t" '$7=="scientific name"{print $1"\t"$3}' names.dmp > taxid2name.txt


Note that several names can be associated to the same taxid.

Problem: taxid are not always the same for the correspondences detected
previously:


| Name                      | taxid |
| ------------------------- | -----:| 
| *Canis familiaris*        |  9615 |
| *Canis lupus familiaris*  |  9615 |
| *Gorilla gorilla*         |  9593 |
| *Gorilla gorilla gorilla* |  9595 |
| *Pongo pygmaeus*          |  9600 |
| *Pongo pygmaeus abelii*   |  9601 |
| *Takifugu rubripes*       | 31033 |
| *Fugu rubripes*           | 31033 |

Decision rule to make the `taxa` table:

* Record the available miRBase `taxid` if information from miRBase and NCBI are coherent.

* If the UCSC name has a `taxid` found in miRBase, update the record with the corresponding `genome_code`

* Else try with the `taxid` obtained with a broader taxonomy (for instance, remove the last "gorilla"), broadening until the *Genus species* binome.

* If no `taxid` already recorded is found, make a new record in the table.


mirBase data
------------

We will work using the "high confidence" miRNA from miRBase.

    mkdir -p ~/Blaise/miRBase_data
    cd ~/Blaise/miRBase_data
    wget ftp://mirbase.org/pub/mirbase/21/organisms.txt.gz
    wget ftp://mirbase.org/pub/mirbase/21/high_conf_mature.fa.gz
    wget ftp://mirbase.org/pub/mirbase/21/mature.fa.gz

### Generate lists of organisms

    zcat organisms.txt.gz > organisms.txt

### Extract seeds

    zcat high_conf_mature.fa.gz > high_conf_mature.fa
    zcat mature.fa.gz > mature.fa
    get_miRBase_seed_info.py \
        -m mature.fa \
        -c high_conf_mature.fa \
        -n ../NCBI_data/name2taxid.txt \
        -o organisms.txt \
        -u ../Pan-vertebrate_alignment/UCSC_species2genome.txt \
        -d ../Conservation_miRNA_targets/sites.sqlite \
        -s seeds.txt \
        -t miRBase_species.txt

Options `-d`, `-s` and `-t` define the output files. Adapt the names in order
to avoid overwriting the files for other analyses.

Output:


    Reading NCBI data...

    Creating taxa table...

    Parsing organisms information from miRBase...
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:163: UserWarning: 
    Ignoring Hydra magnipapillata which has 2 different IDs: 6085 and 6087
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Herpes Simplex Virus 1. ID 10298 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Kaposi sarcoma-associated herpesvirus. ID 37296 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Mouse cytomegalovirus. ID 10366 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Mareks disease virus type 1. ID 10390 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Mareks disease virus type 2. ID 36353 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Mouse gammaherpesvirus 68. ID 33708 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Herpes Simplex Virus 2. ID 10310 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Herpes B virus. ID 10325 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Herpesvirus of turkeys. ID 37108 not found in NCBI taxonomy.
    /mnt/data/home/herve.seitz/Blaise/bin/get_miRBase_seed_info.py:156: UserWarning: 
    Ignoring Herpesvirus saimiri strain A11. ID 570519 not found in NCBI taxonomy.

    Parsing genome codes information from UCSC...

    Pickling dictionaries in ../Conservation_miRNA_targets/sites.pickle ...

    Creating seeds table...

    Parsing miRBase mature miRNA sequences...

    Writing miRBase species and seeds...


Along with the lists of seeds and taxa and the database, dictionaries for
converting between miRBase species codes or UCSC genome names and NCBI taxonomy
IDs have been pickled in `../Conservation_miRNA_targets/sites.pickle`.

We now have a sqlite3 database with `taxa`, `seeds` and `seed_taxa` tables.
We can consult it from python:

    >>> from sqlite3 import connect
    >>> db = connect(../Conservation_miRNA_targets/sites.sqlite)
    >>> print "\n".join(map(str, db.execute("select genome_code from taxa where taxid in (select taxid from seed_taxa where seed == \"TTTTTTT\")").fetchall()))                                 

Response:

    (u'danRer7',)


### Making the list of non-seeds

    make_no_seed_list.py -s seeds.txt > non_seeds.txt



Notes on getting a custom version of Biopython with a MafIO module
==================================================================

I used the instructions on the following page:
<http://biopython.org/wiki/GitUsage>

I cloned it in `sei-lot:~/src/`.

I created a `alignio-maf` branch in which I added a modified version of
`Bio/AlignIO/MafIO.py` from
<https://github.com/polyatail/biopython/tree/alignio-maf>

I pushed the branch with `git push origin alignio-maf`.

In `daisy:~/src/`, I cloned the fork:

    git clone git@github.com:blaiseli/biopython.git

(It was necessary to create `~/.ssh/id_rsa.pub` in daysy and paste it on my
github account first.)

I switched to the alignio-maf branch:

    git checkout alignio-maf

I installed biopython locally:

    python setup.py install --user

For this version to be taken into account,
`/mnt/data/home/herve.seitz/.local/lib/python2.7/site-packages` should be
before `/usr/lib/pymodules/python2.7` in sys.path.

I further updated my copy of Biopython.

Commit used: https://github.com/blaiseli/biopython/commit/21905019fee2b489f095994689f963e7739218f0


Seed localization in Human 3'UTRs
=================================

The idea is to make .bed files
(<http://genome.ucsc.edu/FAQ/FAQformat.html#format1>) for the 3'UTR exons and
for the seed locations, and then compute their intersection using `bedtools`
(<http://bedtools.readthedocs.org/en/latest/>). 

Then, the aligments for these seed locations will be looked in the .maf files
and the species for which the seed match is conserved with respect to the human
sequence will be registered.


3'UTR exon coordinates extraction
---------------------------------

We will first identify the coordinates of 3'UTRs from
`~/Blaise/RefSeq/refGene.txt`

The format of this file is described in
`~/Blaise/RefSeq/RefSeq_schema.html`

We will use the following fields from `~/Blaise/RefSeq/refGene.txt`:

* `name`: Name of gene (usually `transcript_id` from GTF)
* `chrom`: Reference sequence chromosome or scaffold
* `strand`: `+` or `-` for strand
* `txEnd`: Transcription end position
* `cdsEnd`: Coding region end
* `exonStarts`: Exon start positions
* `exonEnds`: Exon end positions

Note that the coordinates in this file are using the same conventions as in the
.bed format: starts are "inclusive" zero-based, but ends are "exclusive" (or,
equivalently, 1-based).

The .bed file will be used to extract and assemble 3'UTRs from .maf alignments.


Some CDS "ends" are not known for genes on the `+` strand and some CDS "starts"
are not known for genes on the `-` strand:

    herve.seitz@daisy:~/Blaise/RefSeq$ awk -F "\t" '$4=="+" {print $15}' refGene.txt | sort | uniq -c
      21086 cmpl
         30 incmpl
       6193 unk
    herve.seitz@daisy:~/Blaise/RefSeq$ awk -F "\t" '$4=="-" {print $14}' refGene.txt | sort | uniq -c
      20153 cmpl
         21 incmpl
       5810 unk

We will work on the selection of genes whose 3'UTR start can be defined.

But some genes are on unreliable chromosomes. Also, there appears to be no
annotation corresponding to genes in chromosome M. We will restrict the
analyses to genes on human chromosomes 1 to 22, X and Y.


Using commit 09dd062b75bf92cc24c47c102a03d93c87de6169 (branch seed_db):

    cd ~/Blaise/Conservation_miRNA_targets_seed
    mkdir bed_UTRs
    for chr in `seq 1 22` X Y
    do
        refseq2utrbed.py \
            --refseq_file ../RefSeq/refGene.txt \
            --chromosome chr${chr} \
            --bed_all bed_UTRs/chr${chr}_3UTR_all.bed \
            --bed_longest bed_UTRs/chr${chr}_3UTR_longest.bed
    done

    for chr in `seq 1 22` X Y
    do
        nb=`cat bed_UTRs/chr${chr}_3UTR_all.bed | wc -l`
        echo -e "${chr}\t${nb}"
    done

Output:

    1   4186
    2   2523
    3   2215
    4   1589
    5   1781
    6   2158
    7   1889
    8   1388
    9   1540
    10  1677
    11  2457
    12  2161
    13  644
    14  1242
    15  1210
    16  1631
    17  2316
    18  605
    19  2657
    20  1183
    21  483
    22  930
    X   2111
    Y   224


    for chr in `seq 1 22` X Y
    do
        nb=`cat bed_UTRs/chr${chr}_3UTR_longest.bed | wc -l`
        echo -e "${chr}\t${nb}"
    done

Output:

    1   3898
    2   2372
    3   2047
    4   1445
    5   1646
    6   1998
    7   1750
    8   1256
    9   1424
    10  1564
    11  2268
    12  2005
    13  609
    14  1152
    15  1113
    16  1529
    17  2115
    18  554
    19  2460
    20  1075
    21  432
    22  854
    X   1844
    Y   167


Searching for seed conservation in the .maf alignment
-----------------------------------------------------

We want to parse the portions of maf alignment obtained from UCSC and look for
seed matches in the H. sapiens sequence, then record for which species the seed
match is also present.


### Search for seed matches

Could use "tries" to do this pattern matching using all seeds together.

Or use regular expressions? Or a (frozen)set (scan the sequence and record the position
for the seed if the fragment is in the set of all seeds) ?

Tries were implemented as recursive dictionaries in python.

We construct .bed files indicating seed matches in human genome.

    mkdir bed_seeds

#### Seeds from all miRNAs belonging to species for which the genome is present in the UCSC alignment

    for chr in `seq 1 22` X Y
    do
        trie.py ../miRBase_data/seeds.txt ../UCSC_genomes/H_sapiens/chr${chr}.fa > bed_seeds/seeds_on_hg19_chr${chr}.bed
    done

[//]: # (We could try to implement tries in C to achieve better speed.)
[//]: # (Also, parallelizing the search can easily be done: one processor per chromosome.)
[//]: # (But too much parallelism can lead to memory shortage.)


#### Non-seeds

    for chr in `seq 1 22` X Y
    do
        trie.py ../miRBase_data/non_seeds.txt ../UCSC_genomes/H_sapiens/chr${chr}.fa > bed_seeds/non_seeds_on_hg19_chr${chr}.bed
    done

#### Intersecting with 3'UTR coordinates

We intersect the beds of seed matches and 3UTR, to get the seed matches in 3'UTRs.
The goal is to use this to get just the interesting portions of the maf alignment.

    mkdir bed_seeds_in_UTRs

A seed should match in the oposite strand as the 3'UTR, hence the use of the
`-S` option of `bedtools intersect`. We do not want the seed matches on the
border of a 3'UTR exon, hence the use of the `-f 1.0` option. We do not want
seed positions duplicated, hence the use of the `-u` option.

[//]: # (Bedtools citation: Quinlan AR and Hall IM, 2010. BEDTools: a flexible suite of utilities for comparing genomic features. Bioinformatics 26(6):841-842.)

We are ignoring the seeds that would match on a junction between 2 3'UTR exons
(or on any post-transcriptionally edited portion of the mRNA that would occur
before miRNA intervention). If the seed matches are randomly distributed (or at
least have no bias for intron junctions location), such ignored seed matches
should be a minority.

#### For seeds

    for chr in `seq 1 22` X Y
    do
        bedtools intersect -S -f 1.0 -u \
        -a bed_seeds/seeds_on_hg19_chr${chr}.bed \
        -b bed_UTRs/chr${chr}_3UTR_all.bed \
        > bed_seeds_in_UTRs/seeds_on_hg19_chr${chr}_3UTR_all.bed
    done

    for chr in `seq 1 22` X Y
    do
        bedtools intersect -S -f 1.0 -u \
        -a bed_seeds/seeds_on_hg19_chr${chr}.bed \
        -b bed_UTRs/chr${chr}_3UTR_longest.bed \
        > bed_seeds_in_UTRs/seeds_on_hg19_chr${chr}_3UTR_longest.bed
    done

#### For non-seeds

    for chr in `seq 1 22` X Y
    do
        bedtools intersect -S -f 1.0 -u \
        -a bed_seeds/non_seeds_on_hg19_chr${chr}.bed \
        -b bed_UTRs/chr${chr}_3UTR_all.bed \
        > bed_seeds_in_UTRs/non_seeds_on_hg19_chr${chr}_3UTR_all.bed
    done

    for chr in `seq 1 22` X Y
    do
        bedtools intersect -S -f 1.0 -u \
        -a bed_seeds/non_seeds_on_hg19_chr${chr}.bed \
        -b bed_UTRs/chr${chr}_3UTR_longest.bed \
        > bed_seeds_in_UTRs/non_seeds_on_hg19_chr${chr}_3UTR_longest.bed
    done

#### Checking numbers of different seeds

    awk '{print $4}' bed_seeds_in_UTRs/non_seeds_on_hg19_chr*_3UTR_all.bed | sort | uniq | wc -l
    # 11032
    awk '{print $4}' bed_seeds_in_UTRs/seeds_on_hg19_chr*_3UTR_all.bed | sort | uniq | wc -l
    # 5352
    echo "11032 + 5352" | bc
    # 16384
    echo "4 ^ 7" | bc
    # 16384

The two sets are disjoint, so we can process the seeds and non-seeds in parallel without fearing interferences.

#### Compressing the seeds location to save disk space

    tar -cv bed_seeds/ | lbzip2 > bed_seeds.tar.bz2 && rm -rf bed_seeds/

### Extract the maf alignement corresponding to the 3'UTRs:

#### Checking that all alignment blocks have the hg19 reference in the "+" strand

    for chr in `seq 1 22` X Y
    do
        echo chr${chr}
        zcat ../Pan-vertebrate_alignment/chr${chr}.maf.gz | awk -v chr="${chr}" '$1=="s" && $2=="hg19.chr"chr {print $5}' | sort | uniq -c
    done

Output:

    chr1
    9075072 +
    chr2
    9261643 +
    chr3
    7539762 +
    chr4
    6848165 +
    chr5
    6583199 +
    chr6
    6574382 +
    chr7
    5887679 +
    chr8
    5331871 +
    chr9
    4522490 +
    chr10
    5293971 +
    chr11
    5061233 +
    chr12
    5205717 +
    chr13
    3641299 +
    chr14
    3492442 +
    chr15
    3317608 +
    chr16
    3398676 +
    chr17
    3612070 +
    chr18
    2825631 +
    chr19
    2226068 +
    chr20
    2415354 +
    chr21
    1333459 +
    chr22
    1443327 +
    chrX
    4484253 +
    chrY
    475040 +



[//]: # (Use mafExtractor from mafTools?)
[//]: # (Example of maf extraction:)

[//]: # (    ~/src/mafTools/bin/mafExtractor --verbose -m maf_extracts/chrY.maf -s hg19.chrY --start 24563750 --stop 24564028 > test_extract_verbose.maf)

[//]: # (Note that we have to prepend "hg19." to chromosome names.)

Using commit 16dfde3f88a90829b7301e9e17e5f8a609d6225e (branch seed_db):

    mkdir sites_db
    UTR_type="all"
    sites_db_dir="sites_db/sites_in_UTR_${UTR_type}"
    mkdir ${sites_db_dir}
    for chr in `seq 1 22` X Y
    do
        echo -e "\nchr${chr}"
        time find_conserved_seeds_in_maf.py \
        -m ../Pan-vertebrate_alignment/chr${chr}.maf \
        -r hg19 \
        -b bed_seeds_in_UTRs/seeds_on_hg19_chr${chr}_3UTR_${UTR_type}.bed \
        -s sites.sqlite \
        -d ${sites_db_dir}
    done

In parallel, for the non-seeds (using commit 8a50b73f6a976271d4b6491e33b1df754bef160e (branch seed_db)):

    UTR_type="all"
    sites_db_dir="sites_db/sites_in_UTR_${UTR_type}"
    for chr in Y X `seq 22 -1 1`
    do
        echo -e "\nchr${chr}"
        time find_conserved_seeds_in_maf.py \
        -m ../Pan-vertebrate_alignment/chr${chr}.maf \
        -r hg19 \
        -b bed_seeds_in_UTRs/non_seeds_on_hg19_chr${chr}_3UTR_${UTR_type}.bed \
        -s sites.sqlite \
        -d ${sites_db_dir}
    done


#### Database design


The tables are described with the following convention:

* In **bold** the primary keys.
* In *italics* the external keys.


1) `seeds`

**`seed`** | `high_conf`
---------- | ----------


2) `taxa`

**`taxid`** | `genome_code` | `mirtax` | `name` | taxonomy
----------- | ------------- | -------- | ------ | --------


3) `seed_taxa`

*`seed`* | *`taxid`* | `from_miRBase`
-------- | --------- | --------------


4) `seed_sites`

*`seed`* | **`site`** |
-------- | ---------- |


5) `site_taxa`

*`site`* | *`taxid`*
-------- | ---------




Check that all sites are present in Homo sapiens:

    >>> db.execute("select * from site_taxa where site not in (select distinct site from site_taxa where taxid == 9606)").fetchall()
    []


Checking for co-localization with JASPAR motif matches
------------------------------------------------------

JASPAR documentation: <http://jaspar.genereg.net/html/TEMPLATES/help.html#BROWSE>

> The JASPAR CORE database contains a curated, non-redundant set of  profiles
> from published articles. All profiles are derived from published collections
> of experimentally defined transcription factor binding sites for
> multi-cellular eukaryotes. The database represents a curated collection of
> target sequences. The binding sites were historically determined either in
> SELEX experiments, or by the collection of data from the experimentally
> determined binding regions of actual regulatory regions; this distinction is
> clearly marked in the profiles"'" annotation. A number of new high-throughput
> techniques, like ChIP-seq, can also be used.
> 
> One of the central goals with JASPAR_CORE is to give the single, "best" model
> for each transcription factor. This means that the database is non-redundant
> in the sense that there are not many models for the same factor (with some
> few exceptions motivated by biological complexity, such as dimers and/or
> splice forms)
> 
>  
> 
> The prime difference to similar resources (TRANSFAC, etc) consist of the open
> data access, non-redundancy and quality: JASPAR CORE is a smaller set that is
> non-redundant and curated.
> 
> JASPAR_CORE is what most scientists mean when referring to JASPAR in
> manuscripts.



### Getting JASPAR data

    mkdir -p ~/Blaise/JASPAR_data
    cd ~/Blaise/JASPAR_data
    # get pfm matrices from JASPAR_CORE for vertebrates
    wget --continue http://jaspar.genereg.net/html/DOWNLOAD/JASPAR_CORE/pfm/nonredundant/pfm_vertebrates.txt
    # get what looks like places where motifs have been found in some genomes
    wget --continue http://jaspar.genereg.net/html/DOWNLOAD/bed_files.tgz
    tar -xvzf bed_files.tgz
    # get fasta files from which motifs were computed
    wget --continue http://jaspar.genereg.net/html/DOWNLOAD/sites.tar.gz
    tar -xvzf sites.tar.gz
    # remove garbage
    rm -f sites/name_chane.pl~


### Looking for overlaps between seed matches and JASPAR hits    

    JASPAR_dir="${HOME}/Blaise/JASPAR_data"
    results_dir="${HOME}/Blaise/Conservation_miRNA_targets"
    mkdir -p "${results_dir}/bed_seeds_in_UTRs_and_JASPAR"
    # -wa to write the seed entry ("A"), not the matrix entry ("B")
    # -f 0.1 to only report when the seed is entirely covered by the matrix match
    bed_options="-wa -f 1.0 -u"
    # sort numerically by chromosome number, start and end
    sort_by_coord="sort -k1.4,1n -k2,2n -k3,3n"
    for bed_file in ${JASPAR_dir}/bed_files/*.bed
    do
        matrix=$(basename ${bed_file%.bed})
        echo ${matrix}
        for chr in `seq 1 22` X Y
        do
            seed_file="${results_dir}/bed_seeds_in_UTRs/seeds_on_hg19_chr${chr}_3UTR_all.bed"
            bed_output="${results_dir}/bed_seeds_in_UTRs_and_JASPAR/seeds_on_hg19_chr${chr}_3UTR_all_and_${matrix}.bed"
            cmd="bedtools intersect -b ${bed_file} -a ${seed_file} ${bed_options} | ${sort_by_coord} > ${bed_output}"
            echo ${cmd}
            eval ${cmd}
        done
    done

Problem with this approach: the bed files downloaded from JASPAR are not well documented.


We will rather work using pfm matrices that come from JASPAR CORE.

For each 6-mer or 7-mer, we want to associate the matrices (or actually:
corresponding transcription factors) whose majority-rule consensuses contain
the sequence or its reverse complement.

We will use these matrices
<http://jaspar.genereg.net/html/DOWNLOAD/JASPAR_CORE/pfm/nonredundant/pfm_vertebrates.txt>

### Possible approach for associating transcription factors to seeds

* Read the matrices from the pfm file

* For each matrix:
    * compute its majority rule consensus
    * decompose it in 6-mers and 7-mers
    * add the corresponding transcription factor to a dictionary of sets using the k-mers as keys

    JASPAR_dir="${HOME}/Blaise/JASPAR_data"
    cd ${JASPAR_dir}
    # The first stored dict gives the number of sequences
    # in a the (possibly degenerate) majority rule consensus.
    # The two next stored dicts are for hexamers and heptamers.
    # They have sets of transcription factors as values.
    associate_kmers_to_pfm_matrices.py pfm_vertebrates.txt pfm_vertebrates_kmer_dicts.pickle
    # Try to protect the pickle file from accidental writing
    chmod -w pfm_vertebrates_kmer_dicts.pickle

* For each seed, use the dictionary to retrieve the transcription factors

    echo -e "AAC ACTTGG ACCGGT\nAACCGTT ACGGTTA GUGACU" | give_transcription_factors.py pfm_vertebrates_kmer_dicts.pickle -s

Output:

/mnt/data/home/herve.seitz/Blaise/bin/give_transcription_factors.py:154: UserWarning: Seed AAC is not a 6-mer or a 7-mer
ACTTGG: 
rev_ACTTGG: 
ACCGGT: 
rev_ACCGGT: 
AACCGTT:    
rev_AACCGTT:    
ACGGTTA:    
rev_ACGGTTA:    
GTGACT: FOS, FOSL1, JUND
rev_GTGACT: Pax2


    # Extract the seeds from the database
    # and pipe the list to the script
    # loading the pickled dictionaries
    python -c "from sqlite3 import connect;
    db = connect('../Conservation_miRNA_targets/sites.sqlite');
    human_seeds = db.execute('select * from seed_taxa where taxid == 9606 and from_miRBase == 1').fetchall();
    print ' '.join(sorted(rec[0] for rec in human_seeds));
    db.close();" \
        | give_transcription_factors.py pfm_vertebrates_kmer_dicts.pickle \
        > ../Conservation_miRNA_targets/sites_db/human_seed_2-8_in_JASPAR.txt



Checking if seed matches are included in RNA binding protein binding motifs
---------------------------------------------------------------------------

RBPmap is a database of such binding sites, describred in
[Paz et al. (2104)](http://dx.doi.org/10.1093/nar/gku406).

### Getting data from RBPmap

    mkdir -p ~/Blaise/RBPmap
    cd ~/Blaise/RBPmap
    wget --continue http://rbpmap.technion.ac.il/source/RBPmap_1.0.tar.gz
    tar -xvzf RBPmap_1.0.tar.gz 

The `~/Blaise/RBPmap/RBP_PSSMs` directory contain motif matrices where
sometimes the values at a given position may be very close but not equal for
two different nucleotides. We do not capture that when doing a majority rule
consensus.

    # Same as for JASPAR matrices
    # The first stored dict gives the number of sequences
    # in a the (possibly degenerate) majority rule consensus.
    # The two next stored dicts are for hexamers and heptamers.
    # They have sets of binding proteins as values.
    associate_kmers_to_pfm_matrices.py -f RBPmap -d pfm_kmer_dicts.pickle RBP_PSSMs/*.txt
    # Try to protect the pickle file from accidental writing
    chmod -w pfm_kmer_dicts.pickle

    # Extract the seeds from the database
    # and pipe the list to the script
    # loading the pickled dictionaries
    python -c "from sqlite3 import connect;
    db = connect('../Conservation_miRNA_targets_seed_2-8/sites.sqlite');
    human_seeds = db.execute('select * from seed_taxa where taxid == 9606 and from_miRBase == 1').fetchall();
    print ' '.join(sorted(rec[0] for rec in human_seeds));
    db.close();" \
        | give_transcription_factors.py -n pfm_kmer_dicts.pickle \
        > ../Conservation_miRNA_targets_seed_2-8/sites_db/human_seed_2-8_in_RBPmap.txt


