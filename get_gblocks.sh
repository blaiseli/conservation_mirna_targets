#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

#sudo apt-get install ncompress

program="Gblocks"
version="0.91b"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}_${version}*

wget --continue http://molevol.cmima.csic.es/castresana/${program}/${program}_Linux64_${version}.tar.Z || error_exit "${program} download failed"
tar -xvZf ${program}_Linux64_${version}.tar.Z || error_exit "${program} unpacking failed: mmaybe you need to install ncompress"
cd ${program}_${version}
echo -e "o\nnad3.pir\ng\nq" | ./Gblocks || error_exit "${program} test failed"
cp Gblocks ${HOME}/bin/. || error_exit "${program} install failed"

exit 0
