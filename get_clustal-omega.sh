#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


program="argtable"
version="2.13"
altversion="2-13"

mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}${altversion}*
wget --continue "http://downloads.sourceforge.net/project/${program}/${program}/${program}-${version}/${program}${altversion}.tar.gz"
tar -xvzf ${program}${altversion}.tar.gz
cd ${program}${altversion}
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
make || "${program} build failed"
make check || "${program} check failed"
make install || error_exit "${program} install failed"


program="clustal-omega"
version="1.2.1"

#mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${program}-${version}*
wget --continue http://www.clustal.org/omega/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}

# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries

#./configure --prefix=${HOME} OPENMP_CFLAGS="-fopenmp" CFLAGS="-DHAVE_OPENMP -O3 -march=native -fomit-frame-pointer -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib"
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer -I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} configure failed" 
make || "${program} build failed"
make check || "${program} check failed"
make install || error_exit "${program} install failed"

exit 0
