#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a fasta file containing 3'UTRS of mRNAs found in a CLIP
study and does stuff."""

import argparse
import sys
WRITE = sys.stdout.write

SPLIT = str.split
STRIP = str.strip
JOIN = str.join
UPPER = str.upper

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


from Bio import SeqIO
from collections import defaultdict


def longest(strings):
    """Returns the longest of the strings in *strings*."""
    return sorted(strings, key=len)[-1]


class Globals(object):
    """Class used to hold global variables as its class attributes."""
    #@staticmethod
    #def load_dicts(pickle_file):
    #    """Use cPickle.load to retrieve the dictionaries."""
    #    Globals.tf2degen = load(pickle_file)
    #    Globals.hexamer2tf = load(pickle_file)
    #    Globals.heptamer2tf = load(pickle_file)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "fasta_file",
        help="Fasta file containing the 3'UTR sequences.")
    parser.add_argument(
        "-i", "--include_genes",
        help="List of genes to include.")
    parser.add_argument(
        "-f", "--fasta",
        help="File in which to write the chosen sequences: "
        "those for the genes in the list provided with the *-i* option.")
    parser.add_argument(
        "-c", "--control_fasta",
        help="File in which to write the control sequences: "
        "those for the genes not in the list provided with the *-i* option.")
    parser.add_argument(
        "--no_check",
        action="store_true",
        help="Use this option if you do not want "
        "checks for sequence coherence in the input.")
    args = parser.parse_args()
    with open(args.include_genes, "r") as include_file:
        # skip header
        include_file.readline()
        gene_set = {fields[0] for fields in map(
            SPLIT,
            map(STRIP, include_file))}
    gi2sequences = defaultdict(list)
    gene2sequences = defaultdict(list)
    nogene2sequences = defaultdict(list)
    sequences = SeqIO.index(args.fasta_file, "fasta")
    for (identifier, record) in sequences.items():
        sequence = str(record.seq)
        fields = SPLIT(identifier, "_")
        gi = fields[0]
        # No need to output empty sequences
        if len(sequence):
            gene_name = JOIN("_", fields[1:])
            if gene_name in gene_set:
                gene2sequences[gene_name].append(sequence)
            else:
                nogene2sequences[gene_name].append(sequence)
        gi2sequences[gi].append(sequence)
    if not args.no_check:
        for (gi, sequences) in gi2sequences.items():
            if len(sequences) > 1:
                zero_len = len([seq for seq in sequences if len(seq) == 0])
                if zero_len:
                    print "ID %s has %d zero-length sequence(s)" % (gi, zero_len)
                else:
                    try:
                        assert all([seq == sequences[0] for seq in sequences]), "ID %s has different sequences" % gi
                    except AssertionError:
                        print "ID %s has different sequences" % gi
            else:
                try:
                    assert len(sequences[0]) > 0, "ID %s has zero-length sequence" % gi
                except AssertionError:
                    print "ID %s has zero-length sequence" % gi
    assert longest(["aa", "zzzz", "zaz", "aa"]) == "zzzz"
    with open(args.fasta, "w") as fasta_file:
        fasta_file.write(JOIN(
            "\n",
            (">%s\n%s" % (
                gene_name,
                UPPER(longest(sequences))) for (
                    gene_name,
                    sequences) in gene2sequences.iteritems())))
    with open(args.control_fasta, "w") as fasta_file:
        fasta_file.write(JOIN(
            "\n",
            (">%s\n%s" % (
                gene_name,
                UPPER(longest(sequences))) for (
                    gene_name,
                    sequences) in nogene2sequences.iteritems())))
    return 0

if __name__ == "__main__":
    sys.exit(main())
