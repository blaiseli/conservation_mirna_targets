Initial setting up
==================

    set +u
    # This USERDIR variable was relevant in the context of our local installation
    # and was used to avoid downloading some already present files.
    [[ ${USERDIR} ]] || USERDIR="${HOME}"
    [[ ${BASEDIR} ]] || BASEDIR="${USERDIR}/Conservation_n-mers"
    [[ ${GENOME} ]] || GENOME="hg19"
    [[ ${CONV_DIR} ]] || CONV_DIR="${PACKAGEDIR}/ID_conversions"
    #[[ ${CONV_DIR} ]] || CONV_DIR="${USERDIR}/ID_conversions"
    #[[ ${ANALYSIS_DIR} ]] || ANALYSIS_DIR="${USERDIR}/Pinzon_et_al_biological_role_miRNA_reanalyzes/remove_over_conserved_sites_from_TargetScan_results"
    [[ ${ANALYSIS_DIR} ]] || ANALYSIS_DIR="${BASEDIR}/remove_over_conserved_sites_from_TargetScan_results"
    set -u
    mkdir -p ${ANALYSIS_DIR}/logs
    case "${GENOME}" in
        "hg19")
            ts_version="vert_70"
            species="H_sapiens"
            conversion="-e ${CONV_DIR}/Ensembl_transcript_info_${species}.txt"
            common_name="human"
            ;;
        "mm10")
            ts_version="mmu_71"
            species="M_musculus"
            conversion="-e ${CONV_DIR}/Ensembl_transcript_info_${species}.txt"
            common_name="mouse"
            ;;
        "dm6")
            echo "${GENOME} not implemented" 1>&2
            exit 1
            #ts_version="fly_12"
            #species="D_melanogaster"
            # TODO:
            #conversion=""
            #common_name="fly"
            ;;
        *)
            echo "${GENOME} not implemented" 1>&2
            exit 1
            ;;
    esac
    phylext_file="${PACKAGEDIR}/Phylogenetic_extension/Phylogenetic_extension_${species}_centered.txt"
    #phylext_file="${ANALYSIS_DIR}/Phylogenetic_extension/Phylogenetic_extension_${species}_centered.txt"
    #[[ -e ${phylext_file} ]] || ln -s ${USERDIR}/bin/Overconservation_flagging_${common_name}_centered.txt ${phylext_file}


Outline of the work to be done
==============================

We want to remove over-conserved sites from TargetScan results.

This cannot be done at the level of the Summary_Counts.txt file, because this
file has summary data per gene.

It may be that the Family_Info.txt files will be more useful.

We want to make a modified version of this file, adding a column of
overconservation flags: "yes", "no", "non_match", "unk_phylext" or "unk_coords".

* "non_match" would be for sites that are actually not really seed matches,
like "3comp", "cent.4.14", "cent.4.15", "cent.5.15". We only consider sites
of the following types: "7mer-1a", "7mer-m8", "8mer", and for the last one,
we only consider conservation of the match for positions 2-7 of the miRNA seed.
(Actually, we will rather skip such sites rather than report them with a flag.)

* "unk_phylext" (for "unknown phylogenetic extension") would be for sites
corresponding to a seed match for a miRNA for which we could not unambiguously
determine its phylogenetic extension using the 75% or more criteron: It was not
present at 75% or more in one clade and at 0% outside. This should correspond
to seeds absent from `${phylext_file}`.

* "unk_coords" (for "unknown coordinates") would be used when we are not able to
find the genomic coordinates of the site, due to missing information.

Additionally, "_unchecked" (for "seed not checked") can be appeneded to a "yes"
or "no" flag when the seed for the (miRNA, species) combination is not known in
the miR_Family_Info.txt file from TargetScan, so that it cannot be checked that
the sequence at the genomic coordinates corresponds to a seed match.

We will add another column indicating the genomic coordinate of the seed match
(in the form <chrN>:<1-based-start-pos>-<1-based-end-pos>:strand), when known,
or the reason why we could not calculate it:

* "ID_unk" when we are not able to convert the Ensembl transcript ID into a Genbank ID found in the refGene data.

* "special_chromosome" when the refGene entry was found but the transcript
* comes from a non-standard chromosome.

* "CDS_unk" when the refGene entry was found but without the necessary information.

* "too_short" would be used when the genomic coordinates of the site
would fall outside the transcript.

* "across_intron" would be used when the genomic coordinates of the site
would yield a site matching across an intron. Such sites have been ignored
while generating conservation data.

* "no_match" would be used when the genomic coordinates of the site
would yield a site not matching the seed.


To know whether a site from Family_Info is over-conserved, we need to find its
conservation in the results from `find_conserved_seeds_in_maf.py`.

If there is an intersection between the genomes in which this seed match was
found and the genomes listed in the corresponding line in
`${phylext_file}`, the flag will be "yes".
If the intersection is empty, the flag will be "no".
If no corresponding line is found, the flag will be "unk_phylext"

The information from `${phylext_file}` will be
loaded in memory in a dictionary with the seed as a key.


The main problem seems to be matching a site from Family_Info (localizable by
an Ensembl ENST* transcript ID and UTR-relative coordinates) with a line in a
sites_db/sites_in_UTR_all/<seed>.txt file (using BED-style genomic
coordinates).

The intron-exon and UTR structure of a transcript is available from
`${BASEDIR}/RefSeq/refGene.txt`, but in this file the transcripts are
identified through Genbank NM_* or NR_* IDs. 

Hopefully, the matching between the two types of transcript IDs will be done
through information downloadable using the Mysql interface to the Ensembl
database. This matching table can be loaded in memory in dictionaries.

Next step will be to use transcript structure information in order to convert
UTR-relative coordinates into genomic coordinates. To such a coordinate we will
add the genomic UTR start and the lengths of the preceding introns. Preceding
introns can be determined by adding the lengths of successive exons after UTR
start while the sum is strictly shorter than the UTR-relative coordinate.

Attention will need to be paid on details pertaining to the way coordinates are
given: BED-style or not, for instance.

It should be checked that the calculated genomic coordinates correspond to a
seed match for the miRNA family given in the Family_Info file using fasta files
from `${BASEDIR}/UCSC_genomes/${GENOME}/`.

The seed match will be revers-complemented into a seed, which will determine in
which file in a `sites_db/sites_in_UTR_all/` directory seed-match conservation
will have to be looked for. 


Steps to do the work
====================


Downloading Predicted site data for TargetScan
----------------------------------------------

    # Set a default TargetScan directory if it is not defined in the environment
    set +u
    [[ ${TS_DIR} ]] || TS_DIR="${USERDIR}/TargetScan_data"
    set -u
    mkdir -p ${TS_DIR}
    # The cd command only has effects in the parentheses
    (
        cd ${TS_DIR}
        mkdir -p ${ts_version}
        cd ${ts_version}
        wget -t 0 -nv --continue http://www.targetscan.org/${ts_version}/${ts_version}_data_download/Conserved_Family_Info.txt.zip
        wget -t 0 -nv --continue http://www.targetscan.org/${ts_version}/${ts_version}_data_download/Nonconserved_Family_Info.txt.zip
        wget -t 0 -nv --continue http://www.targetscan.org/${ts_version}/${ts_version}_data_download/miR_Family_Info.txt.zip
        [[ -e Conserved_Family_Info.txt ]] || unzip Conserved_Family_Info.txt.zip
        [[ -e Nonconserved_Family_Info.txt ]] || unzip Nonconserved_Family_Info.txt
        [[ -e miR_Family_Info.txt ]] || unzip miR_Family_Info.txt
    )


Generate conversion table to match ensembl and RefGen transcript IDs
--------------------------------------------------------------------

This is necessary for human-centered analyses. Mouse-centered TargetScan, as of
version 6 uses "NM_*"-style transcript IDs.
TODO: Check for Drosophila

We proceeded manually to get conversion info from Ensembl, through <www.ensembl.org/biomart/martview/>


Choices
-------

* Database: Ensembl Genes 83

* Dataset: Homo sapiens genes (GRCh38.p5) or Mus musculus genes (GRCm38.p4)

* Attributes:
- Ensembl Gene ID
- Ensembl Transcript ID
- Chromosome Name
- Strand
- Transcript Start (bp)
- Transcript End (bp)
- EntrezGene transcript name ID
- RefSeq mRNA [e.g. NM_001195597]
- RefSeq ncRNA [e.g. NR_002834]

Result saved in "Ensembl_transcript_info_H_sapiens.txt" and put in "${PACKAGEDIR}/ID_conversions".
Result saved in "Ensembl_transcript_info_M_musculus.txt" and transferred to "${PACKAGEDIR}/ID_conversions".


In the H. sapiens case, elected options supposedly corresponded to the following URL:
<http://www.ensembl.org/biomart/martview/8ecb5fb654714a2f68286b422651473d?VIRTUALSCHEMANAME=default&ATTRIBUTES=hsapiens_gene_ensembl.default.feature_page.ensembl_gene_id|hsapiens_gene_ensembl.default.feature_page.ensembl_transcript_id|hsapiens_gene_ensembl.default.feature_page.chromosome_name|hsapiens_gene_ensembl.default.feature_page.strand|hsapiens_gene_ensembl.default.feature_page.transcript_start|hsapiens_gene_ensembl.default.feature_page.transcript_end|hsapiens_gene_ensembl.default.feature_page.entrezgene_transcript_name|hsapiens_gene_ensembl.default.feature_page.refseq_mrna|hsapiens_gene_ensembl.default.feature_page.refseq_ncrna&FILTERS=&VISIBLEPANEL=attributepanel>

Or to the following `perl` code:

#```perl
#
## An example script demonstrating the use of BioMart API.
## This perl API representation is only available for configuration versions >=  0.5 
#use strict;
#use BioMart::Initializer;
#use BioMart::Query;
#use BioMart::QueryRunner;
#
#my $confFile = "PATH TO YOUR REGISTRY FILE UNDER biomart-perl/conf/. For Biomart Central Registry navigate to
#                        http://www.biomart.org/biomart/martservice?type=registry";
##
## NB: change action to 'clean' if you wish to start a fresh configuration  
## and to 'cached' if you want to skip configuration step on subsequent runs from the same registry
##
#
#my $action='cached';
#my $initializer = BioMart::Initializer->new('registryFile'=>$confFile, 'action'=>$action);
#my $registry = $initializer->getRegistry;
#
#my $query = BioMart::Query->new('registry'=>$registry,'virtualSchemaName'=>'default');
#
#        
#    $query->setDataset("hsapiens_gene_ensembl");
#    $query->addAttribute("ensembl_gene_id");
#    $query->addAttribute("ensembl_transcript_id");
#    $query->addAttribute("chromosome_name");
#    $query->addAttribute("strand");
#    $query->addAttribute("transcript_start");
#    $query->addAttribute("transcript_end");
#    $query->addAttribute("entrezgene_transcript_name");
#    $query->addAttribute("refseq_mrna");
#    $query->addAttribute("refseq_ncrna");
#
#$query->formatter("TSV");
#
#my $query_runner = BioMart::QueryRunner->new();
############################### GET COUNT ############################
## $query->count(1);
## $query_runner->execute($query);
## print $query_runner->getCount();
######################################################################
#
#
############################### GET RESULTS ##########################
## to obtain unique rows only
## $query_runner->uniqueRowsOnly(1);
##
#$query_runner->execute($query);
#$query_runner->printHeader();
#$query_runner->printResults();
#$query_runner->printFooter();
######################################################################
#```



Run the python script that generates the flagged Family_Info files
------------------------------------------------------------------

    [[ ${CHROM} ]] || CHROM="Y"
    N_MER_CONSERVATION_DIR="${BASEDIR}/${GENOME}_6-mer"
    GENOME_DIR="${BASEDIR}/UCSC_genomes/${GENOME}"
    REFSEQ_DIR="${BASEDIR}/RefSeq_${GENOME}_centered"
    #[[ ${F_CONSERVATION} ]] || F_CONSERVATION="Conserved"
    #[[ ${F_CONSERVATION} ]] || F_CONSERVATION="Nonconserved"
    for F_CONSERVATION in "Conserved" "Nonconserved"
    do
        #outfile="${ts_version}_${F_CONSERVATION}_Family_Info_overconservation_flagged.txt"
        outfile="${ts_version}_${F_CONSERVATION}_Family_Info_overconservation_flagged_chr${CHROM}.txt"
        flag_over_conserved_sites.py \
            -f ${TS_DIR}/${ts_version}/${F_CONSERVATION}_Family_Info.txt ${conversion} \
            -g ${GENOME_DIR} \
            -p ${phylext_file} \
            -r ${GENOME} \
            -c chr${CHROM} \
            --refseq_file ${REFSEQ_DIR}/refGene.txt \
            -s ${N_MER_CONSERVATION_DIR} \
            -m ${TS_DIR}/${ts_version}/miR_Family_Info.txt \
            -o ${ANALYSIS_DIR}/${outfile} \
            1> ${ANALYSIS_DIR}/logs/flag_${ts_version}_${F_CONSERVATION}_${GENOME}_chr${CHROM}.log \
            2> ${ANALYSIS_DIR}/logs/flag_${ts_version}_${F_CONSERVATION}_${GENOME}_chr${CHROM}.err
    done
    
    exit 0
