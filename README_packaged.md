
Content
=======

This directory contains the scripts and information necessary to run the
analyses determining the degree of conservation of n-mers, and use this
information to add flags to TargetScan results files (actually, generating new
versions of these files with extra columns). The flags indicate whether a given
predicted seed match is more conserved than the miRNA itself or not.



Data
----

During the analyses, data from various sources will be downloaded. Some of the
data may use a lot of space (genome alignments in MAF format, in particular).


Some information cannot easily be produced automatically, so we provide two
directories containing pre-generated information:

* `ID_conversions/`: This directory contains files indicating the
  correspondence between genbank and Ensembl transcript identifiers.

* `Phylogenetic_extension/`: This directory contains files indicating species
  in which a given miRNA seems absent, taking into account various sources
  including miRBase, and a search for homologues to known miRNA genes (analysis
  performed by Hervé Seitz).



Analysis scripts
----------------

There are README files and scripts for two phases of the analyses.
The second phase relies on data or results obtained during the first one.

For the first phase:

* `do_scan_maf2conservation.sh` (wrapper script)

* `README_scan_maf_for_conservation.md` ("executable" markdown description of
  the analyses)

* `scan_maf2conservation.py` (python script producind the results)

For the second phase:

* `do_flag_over_conserved_sites.sh` (wrapper script)

* `README_flag_over_conserved_sites_in_TargetScan_results.md` ("executable"
  markdown description of the analyses)

* `flag_over_conserved_sites.py` (python script producind the results)



Other things
------------

In addition, there is a script used to extract code portions from the markdown
formatted README files (`markdown2code.sh`) and the present
`README_packaged.md` file.



Usage
=====

The README files are in markdown format. This can be read using any text
editor, or converted to other formats using tools like `pandoc`.

The detail of the analyses are described there, together with code sections.

These code sections can be extracted with `markdown2code.sh` and piped to
`bash` for execution.

For your convenience, this is actually done in two wrapper scripts:
`do_scan_maf2conservation.sh` and `do_flag_over_conserved_sites.sh`.


First phase
-----------

The first script takes as arguments an identifier for a reference genome (for
instance "hg19" or "mm10"), a chromosome number or letter (for instance "4",
"Y" or "M"), and a directory in which data will be downloaded and results will
be written (subdirectories will be created so that all this is not too
disorganized).

It will execute the analyses described in
`README_scan_maf_for_conservation.md`. This mainly consists in downloading a
MAF-formatted genome alignment file from
[UCSC](http://hgdownload.cse.ucsc.edu/downloads.html), scanning it with the
`scan_maf2conservation.py` python script, and generating a bed file telling, at
each position along the chromosome of the reference genome

This first phase may download and install a custom version of biopython (in the
user directory). See <https://github.com/blaiseli/biopython/tree/alignio-maf>
and <https://github.com/biopython/biopython/pull/504>.


Second phase
------------

The second script takes the same arguments, in the same order. The third
argument (directorry in which things happen) should be the same as for the
first script.

This second script will execute the analyses described in
`README_flag_over_conserved_sites_in_TargetScan_results.md`. This mainly
consists in downloading data from TargetScan and using the bed file generated
during the previous phase as well as the information provided in the
`ID_conversions/` and `Phylogenetic_extension/` to produce a flagged version of
TargetScan results, using the `flag_over_conserved_sites.py` python script.


Example usage
-------------

If you want to execute everything for the human-centered and mouse-centered
studies, sequencially, you may use the following in the shell (we used `bash`):


    for chromosome in X $(seq 1 22) Y M
    do
        `do_scan_maf2conservation.sh` "hg19" Pinzon_et_al_over_conservation_analyses
    done

    for chromosome in X $(seq 1 19) Y M
    do
        `do_scan_maf2conservation.sh` "mm10" Pinzon_et_al_over_conservation_analyses
    done


/Important/: For things to work properly, all scripts and README files need to
reside in the same directory, together with the two directories
`ID_conversions/` and `Phylogenetic_extension/`
