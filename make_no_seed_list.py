#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script takes a list of DNA n-mers (seeds) and generates the list of
same-length DNA n-mers that are not in the list."""

import argparse
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from os.path import splitext
from re import sub
from string import maketrans, split
RNA2DNA_TABLE = maketrans("U", "T")

def to_dna(seq):
    """Converts an upper-case RNA sequence into an upper-case DNA sequence.
    Actually, this just substitutes Us for Ts."""
    return seq.translate(RNA2DNA_TABLE)

from Bio import SeqIO
#from sqlite3 import connect, IntegrityError
from sqlite3 import connect
# To store dictionaries
from cPickle import dump, HIGHEST_PROTOCOL

try:
    from seed2num import make_seeds
except ImportError:
    sys.stderr.write(
        "This script requires the presence of the seed2num module.\n")
    sys.exit(1)

class Globals(object):
    """Class used to hold global variables as its class attributes."""

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-s", "--seeds_list",
        required=True,
        help="File containing one seed (n-mer) per line.",
        type=argparse.FileType("r"))
    parser.add_argument(
        "-l", "--seed_length",
        help="Length of a seed (value of *n* in n-mer).",
        type=int,
        default=7)
    args = parser.parse_args()
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip

    # Generate list of potential no-seeds
    numtoseed, seedtonum = make_seeds(seed_length=args.seed_length)
    no_seeds = set(seedtonum)
    del numtoseed
    del seedtonum

    for line in args.seeds_list:
        no_seeds.remove(strip(line))
    WRITE("\n".join(sorted(no_seeds)))
    WRITE("\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
