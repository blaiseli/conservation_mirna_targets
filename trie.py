#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""Trie-like implementation to search for a set of same-size DNA words in fasta
sequences."""

import argparse
import sys
WRITE = sys.stdout.write
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


from string import maketrans, split
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")
RNA2DNA_TABLE = maketrans("U", "T")

def to_dna(seq):
    return seq.translate(RNA2DNA_TABLE)

def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]

#from collections import deque, defaultdict
from collections import defaultdict


from Bio import SeqIO
#from Bio.Seq import reverse_complement



def add_word(word, subdict):
    """Recursively adds *word* to *subdict*."""
    if word[0] not in subdict:
        subdict[word[0]] = {}
    #if len(word[1:]):
    if word[1:]:
        add_word(word[1:], subdict[word[0]])


def has_word(word, subdict):
    """Recursively searches *word* in *subdict*."""
    if word[0] not in subdict:
        return False
    #if len(word[1:]):
    if word[1:]:
        return has_word(word[1:], subdict[word[0]])
    else:
        return True



class Globals(object):
    """Class used to hold global variables as its class attributes."""

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("words_file",
            help="File containing the list of words.",
            type=argparse.FileType("rU"))
    parser.add_argument("seq_file",
            help="File containing the sequences in which to search for words.",
            type=argparse.FileType("r"))
    parser.add_argument("--exclude_refs",
            help="File containing the names of references to exclude from the search.",
            type=argparse.FileType("r"))
    #parser.add_argument("-t", "--test",
    #        help="This is an optional argument.", default="foo")
    #parser.add_argument("-s", "--switch",
    #        help="This is an optional boolean switch.",
    #        action="store_true")
    args = parser.parse_args()
    #sys.stdout.write("First positional argument is: %s\n" % args.first_arg)
    #if args.switch:
    #    warnings.warn("This is an example warning message.\n")
    #    sys.stdout.write("The switch has been turned on "
    #            "on the command line.\n")
    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    #split = str.split
    matches = {}
    trie = {}
    rev_trie = {}
    exclude = set([])
    word_len = None
    if args.exclude_refs:
        with args.exclude_refs as input_file:
            for line in input_file:
                ref = strip(line, "\n")
                if ref:
                    if ref[0] != "#":
                        exclude.add(ref)
    with args.words_file as input_file:
        for line in input_file:
            word = to_dna(strip(line, "\n").upper())
            if word_len is None:
                word_len = len(word)
            else:
                assert word_len == len(word), "All words should have the same length."
            add_word(word, trie)
            add_word(dna_rev_compl(word), rev_trie)
            # key: sequence name
            # value: list of (match position, strand) for word in sequence
            matches[word] = defaultdict(list)
    with args.seq_file as input_file:
        for record in SeqIO.parse(input_file, "fasta"):
            seq_name = split(record.name)[0]
            if not seq_name:
                seq_name = split(record.id)[0]
            if seq_name in exclude:
                continue
            seq_text = str(record.seq.upper())
            seq_len = len(seq_text)
            #print seq_name, seq_len
            #fragment = deque(seq_text[0:word_len-1], word_len)
            for index in xrange(1 + seq_len - word_len):
            #for (index, letter) in enumerate(seq_text[word_len-1:]):
                #fragment.append(letter)
                #str_frag = "".join(fragment)
                # More efficient than deque
                str_frag = seq_text[index:index+word_len]
                if has_word(str_frag, trie):
                    matches[str_frag][seq_name].append((index, "+"))
                if has_word(str_frag, rev_trie):
                    matches[dna_rev_compl(str_frag)][seq_name].append((index, "-"))
    for seed, seq_matches in sorted(matches.items()):
        for seq_name, positions in sorted(seq_matches.items()):
            for (start, strand) in positions:
                WRITE("%s\t%d\t%d\t%s\t1000\t%s\n" % (
                    seq_name, start, start + word_len, seed, strand))
    return 0

if __name__ == "__main__":
    sys.exit(main())
