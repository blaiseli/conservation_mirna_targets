#!/usr/bin/env bash
echo  "WARNING: Does not work: download binary and make links to ~/bin instead"
exit 1

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

mkdir -p ${HOME}/src

# https://github.com/cole-trapnell-lab/cufflinks
program="eigen"
version="3.2.5"

cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${program}-${version}*
# Download it
wget --continue -O ${program}-${version}.tar.bz2 http://bitbucket.org/${program}/${program}/get/${version}.tar.bz2 || error_exit "${program} download failed"
mkdir ${program}-${version}
tar -xvjf ${program}-${version}.tar.bz2 -C ${program}-${version} --strip-components 1
cd ${program}-${version}
cp -af Eigen ${HOME}/include/.

program="cufflinks"
version="2.2.1"

cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${program}-${version}*
# Download it
wget --continue http://cole-trapnell-lab.github.io/${program}/assets/downloads/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
#./configure --prefix=${HOME} --with-zlib=${HOME} --with-eigen=${HOME} --with-bam=${HOME} --with-bam-libdir=${HOME}/lib CFLAGS="-O3 -march=native -fomit-frame-pointer" CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
#./configure --prefix=${HOME} --with-zlib=${HOME} --with-eigen=${HOME} --with-bam=${HOME} --with-bam-libdir=${HOME}/lib || error_exit "${program} config failed"
./configure --prefix=${HOME} --with-zlib=${HOME} --with-eigen=${HOME} --with-bam=${HOME} || error_exit "${program} config failed"
#--with-bam-libdir=${HOME}/lib
#./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CXXFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
make || error_exit "${program} build failed"
make install || error_exit "${program} install failed"

exit 0
