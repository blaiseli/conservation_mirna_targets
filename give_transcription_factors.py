#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads a series of pfm matrices fro JASPAR to build a dictionary
associating k-mers to matrix consensuses containing such k-mers."""

import argparse
import sys
WRITE = sys.stdout.write

#from string import split, strip
SPLIT = str.split
STRIP = str.strip

from numpy import array, asarray, transpose
#from numpy import argmax, array, asarray
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

# To load dictionaries
from cPickle import load

# To manipulate nucleotide sequences
from string import maketrans
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")
RNA2DNA_TABLE = maketrans("U", "T")

def to_dna(seq):
    """Converts RNA to DNA"""
    return seq.translate(RNA2DNA_TABLE)

def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]

#from collections import deque, defaultdict
from collections import defaultdict
from itertools import imap, product
from operator import or_ as union
from operator import mul


def read_pfm(file_handle, tf_name):
    """Reads four lines from *file_handle*.
    Computes the consensus of the corresponding ATGC profile.
    Lists the contained k-mers.
    Records *tf_name* in *Globals.kmer2tf* at he k-mer keys.
    Records the motif degenerescence in *Globals tf_degen*."""
    a_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    motif_len = len(a_counts)
    if motif_len < 6:
        warnings.warn("Skipping %s: matrix too short." % tf_name)
        # No hexamer can be found in the motif consensus
        # skip the other lines and do nothing else
        file_handle.readline()
        file_handle.readline()
        file_handle.readline()
        return
    c_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    g_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    t_counts = [int(count) for count in SPLIT(STRIP(file_handle.readline()))]
    counts = transpose(array([a_counts, c_counts, g_counts, t_counts]))
    consensus = []
    for acgt_counts in counts:
        letters = set("A")
        max_count = acgt_counts[0]
        count = acgt_counts[1]
        if count > max_count:
            letters = set("C")
            max_count = count
        elif count == max_count:
            letters.add("C")
        count = acgt_counts[2]
        if count > max_count:
            letters = set("G")
            max_count = count
        elif count == max_count:
            letters.add("G")
        count = acgt_counts[3]
        if count > max_count:
            letters = set("T")
            max_count = count
        elif count == max_count:
            letters.add("T")
        consensus.append(letters)
    assert tf_name not in Globals.tf2degen
    Globals.tf2degen[tf_name] = reduce(mul, imap(len, consensus), 1)
    hexamers = reduce(
            union,
            [{"".join(letters) for letters in product(*consensus[s:s+6])}
                for s in range(len(consensus) - 5)])
    for hexamer in hexamers:
        Globals.hexamer2tf[hexamer].add(tf_name)
    if len(consensus) < 7:
        return
    heptamers = reduce(
            union,
            [{"".join(letters) for letters in product(*consensus[s:s+6])}
                for s in range(len(consensus) - 5)])
    for heptamer in heptamers:
        Globals.heptamer2tf[heptamer].add(tf_name)
    return

class Globals(object):
    """Class used to hold global variables as its class attributes."""
    # key: transcription factor name
    # value: consensus degeneration for this transcription factor
    tf2degen = defaultdict(int)
    # key: k-mer
    # value: set of transcription factor names having the k-mer in their consensus
    hexamer2tf = defaultdict(set)
    heptamer2tf = defaultdict(set)
    @staticmethod
    def load_dicts(pickle_file):
        """Use cPickle.load to retrieve the dictionaries."""
        Globals.tf2degen = load(pickle_file)
        Globals.hexamer2tf = load(pickle_file)
        Globals.heptamer2tf = load(pickle_file)

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "kmer_dicts",
        help="Pickle file in which to store the dictionary.",
        type=argparse.FileType("rb"))
    parser.add_argument(
        "-s", "--separate_revcompl",
        help="To output results for the reverse complement on a separate line.",
        action="store_true")
    parser.add_argument(
        "-n", "--no_seed",
        help="Only outputs results for the reverse complement.",
        action="store_true")
    args = parser.parse_args()
    with args.kmer_dicts as pickle_file:
        #WRITE("\nPickling dictionaries in %s ...\n" % pickle_file.name)
        Globals.load_dicts(pickle_file)
    input_seeds = map(to_dna, SPLIT(STRIP(sys.stdin.read())))
    for seed in input_seeds:
        rev_seed = dna_rev_compl(seed)
        seed_l = len(seed)
        if seed_l == 6:
            seed2tf = Globals.hexamer2tf
        elif seed_l == 7:
            seed2tf = Globals.heptamer2tf
        else:
            warnings.warn("Seed %s is not a 6-mer or a 7-mer" % (seed))
            continue
        if args.separate_revcompl or args.no_seed:
            if not args.no_seed:
                WRITE("%s:\t%s\n" % (
                    seed,
                    ", ".join(sorted(seed2tf[seed]))))
            WRITE("rev_%s:\t%s\n" % (
                seed,
                ", ".join(sorted(seed2tf[rev_seed]))))
        else:
            WRITE("%s:\t%s\n" % (
                seed,
                ", ".join(sorted(seed2tf[seed] | seed2tf[rev_seed]))))
    return 0

if __name__ == "__main__":
    sys.exit(main())
