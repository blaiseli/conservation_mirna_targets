#!/bin/bash
# Usage: do_flag_over_conserved_sites.sh *genome* *chromosome* [*base_directory*]
#
# Data download and analyses will happen in *base_directory* (within sub-directories).

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#   ----------------------------------------------------------------
#   Function for exit due to fatal program error
#       Accepts 1 argument:
#           string containing descriptive error message
#   ----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


# To exit with error if a variable is not defined
set -u


# Set the location of this file and other distributed files:
# http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
PACKAGEDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# The above trick only works if the commands are run from a script
# present in the same directory as the distributed files.

# To have these variable defined in child shells
export ${PACKAGEDIR}

set +u
USERDIR=${3}
[[ ${USERDIR} ]] || USERDIR="${HOME}"
set -u
export ${USERDIR}


genome=${1}

# Create directories for the analyses and their logs if they do not exist yet
mkdir -p ${USERDIR}/logs

# Scan maf file for one chromosome
# This generates a bed file indicating the genomes
# having the same n-mers a the reference one
#
# WARNING: This may download large genome alignments
# Be sure you have enough disk space. Have a look at the README file.
#
# `markdown2code.sh` extracts the 4-space-indented code blocks
# from the markdow-formatted README file.
# The extracted code is piped to bash and will behave according to
# some environment variables, either defined below, or previously exported.
${PACKAGEDIR}/markdown2code.sh ${PACKAGEDIR}/README_scan_maf_for_conservation.md \
    | BASEDIR="${USERDIR}/Conservation_n-mers" \
    GENOME="${genome}" CHROM="${2}" LEN="6" \
    bash \
    1> ${USERDIR}/logs/Conservation_6-mers_${genome}_chr${2}.log \
    2> ${USERDIR}/logs/Conservation_6-mers_${genome}_chr${2}.err \
    || error_exit "scan_maf_for_conservation failed"

exit 0
