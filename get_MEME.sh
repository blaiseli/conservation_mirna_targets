#!/usr/bin/env bash

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

program="meme"
#version="1.5.0"
version="4.10.2"
URL="http://meme-suite.org/meme-software/${version}/${program}_${version}.tar.gz"

mkdir -p ${HOME}/src
cd ${HOME}/src
# In case it's already there, remove it
rm -rf ${program}_${version}*
# Download it
wget ${URL} || error_exit "${program} download failed"
tar -xvzf ${program}_${version}.tar.gz || error_exit "${program} unpacking failed"
cd ${program}_${version}
./configure --prefix=${HOME} CFLAGS="-O3 -march=native -fomit-frame-pointer" CPPFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib -Wl,-rpath,${HOME}/lib" || error_exit "${program} config failed"
make || error_exit "${program} build failed"
make check || error_exit "${program} check failed"
make test || error_exit "${program} test failed"
make install || error_exit "${program} install failed"

exit 0
