#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""
"""

import argparse
import sys
import warnings
from string import strip

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

WRITE = sys.stdout.write

#################
# main function #
#################

def main():
    """Main function of the program."""
    #WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "operation",
        choices=["intersection", "union", "difference"],
        help="Name of the set operation to perform between the two sets.")
    parser.add_argument(
        "file1",
        help="File containing one name per line, "
        "to be used as first set of elements.")
    parser.add_argument(
        "file2",
        help="File containing one name per line, "
        "to be used as second set of elements.")
    args = parser.parse_args()
    
    with open(args.file1) as file1:
        set1 = set(map(strip, file1))
    with open(args.file2) as file2:
        set2 = set(map(strip, file2))
    set_result = getattr(set1, args.operation)(set2)
    WRITE("\n".join(sorted(set_result)))
    if set_result:
        WRITE("\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
