#!/usr/bin/env bash
# To use after get_zlib.sh so that the programs can use the locally-compiled zlib

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

project="MSAProbs"
program=`echo ${project} | tr '[:upper:]' '[:lower:]'`
version="0.9.7"


mkdir -p ${HOME}/src
cd ${HOME}/src

rm -rf ${project}-${version}*

wget --continue http://downloads.sourceforge.net/project/${program}/${project}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${project}-${version}.tar.gz
cd ${project}-${version}/${project}

# Use native processor architecture for compiling 
# Add the path to possible locally-compiled included libraries
sed -i 's|^COMMON_FLAGS|#COMMON_FLAGS|' Makefile
sed -i 's|^CXXFLAGS = $(COMMON_FLAGS)|CXXFLAGS = -O3 $(OPENMP) -Wall -funroll-loops -march=native -fomit-frame-pointer -I . -I ${HOME}/include -L${HOME}/lib -Wl,-rpath,${HOME}/lib|' Makefile

make || "${program} build failed"

ln -sf ${HOME}/src/${project}-${version}/${project}/${program} ${HOME}/bin/. || error_exit "${program} install failed"

exit 0
