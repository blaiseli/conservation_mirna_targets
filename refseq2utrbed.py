#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script will parse a refGene.txt file and generate a .bed file with the
coordinates of 3'UTRs.
"""

import argparse
from os import devnull
import sys
import warnings
def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

from collections import namedtuple
from operator import attrgetter

Three_p = namedtuple(
    "Three_p",
    ["name", "chrom", "strand", "coords", "length", "gene"])


def write_beds(records, bed_all_file, bed_longest_file):
    """Writes the coordinates of the 3'UTR present in <records> from a same
    gene in file *bed_all_file*. The longest 3'UTR among those present in <records>
    is used to write an entry in the *bed_longest_file* file."""
    # Last one should be the longest.
    utrs = sorted(records, key=attrgetter("length"))
    # To get BED format coordinates, we need to substract 1
    # from our internal 1-based start coordinates.
    for utr in utrs:
        for coord in utr.coords:
            bed_all_file.write("\t".join([
                utr.chrom,
                str(coord[0] - 1),
                str(coord[1]), utr.gene, "0", utr.strand]))
            bed_all_file.write("\n")
    # Re-use the last one, which should be the longest
    for coord in utr.coords:
        bed_longest_file.write("\t".join([
            utr.chrom,
            str(coord[0] - 1),
            str(coord[1]), utr.gene, "0", utr.strand]))
        bed_longest_file.write("\n")


class Globals(object):
    """Class used to hold global variables as its class attributes."""

def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="This argument makes the script display more messages.")
    parser.add_argument(
        "--refseq_file",
        required=True,
        help="File formatted as "
        "http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/refGene.txt.gz "
        "(after gunzipping).",
        type=argparse.FileType("r"))
    parser.add_argument(
        "--bed_all",
        help="Bed file in which to write the coordinates for "
        "all the 3'UTRs.")
    parser.add_argument(
        "--bed_longest",
        help="Bed file in which to write the coordinates for "
        "the longest 3'UTR of each gene.")
    parser.add_argument(
        "--chromosome",
        help="Chromosomes from which we want to extract 3'UTRs.")
    parser.add_argument(
        "--chromosomes",
        help="File containing the list of chromosomes "
        "from which we want to extract 3'UTRs.\n"
        "One chromosome per line.",
        type=argparse.FileType("r"))
    args = parser.parse_args()
    if not (args.bed_all or args.bed_longest):
        sys.stderr.write("At least one of the two following options should be set:\n")
        sys.stderr.write("--bed_all\n")
        sys.stderr.write("--bed_longest\n")
        sys.exit(1)
    if not args.bed_all:
        bed_all_file = open(devnull, "w")
    else:
        bed_all_file = open(args.bed_all, "w")
    if not args.bed_longest:
        bed_longest_file = open(devnull, "w")
    else:
        bed_longest_file = open(args.bed_longest, "w")

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip
    split = str.split
    # Make the list of chromosomes we're interested in.
    Globals.chromosomes = []
    if args.chromosomes:
        with args.chromosomes as chrom_file:
            for line in chrom_file:
                chrom_name = strip(line)
                if chrom_name:
                    if chrom_name[0] != "#":
                        Globals.chromosomes.append(chrom_name)
    if args.chromosome:
        Globals.chromosomes.append(args.chromosome)
    if args.verbose:
        sys.stdout.write("The following chromosomes will be taken into account:\n")
        sys.stdout.write(", ".join(Globals.chromosomes))
        sys.stdout.write("\n")
    # Read input file.
    # Coordinates are in bed format.
    # We convert this in 1-based inclusive.
    with args.refseq_file as refseq_file:
        last_gene = ""
        records = []
        for line in refseq_file:
            fields = split(strip(line, "\n"), "\t")
            # transcript_id
            #name = fields[1]
            #chrom = fields[2]
            #strand = fields[3]
            name, chrom, strand = fields[1:4]
            if chrom not in Globals.chromosomes:
                # Skip genes not in a chromosome we're interested in.
                continue
            # Determine 3'UTR "start" end "end"
            if strand == "+":
                #cdsEndStat = fields[14]
                if fields[14] != "cmpl":
                    # Skip data without a reliable CDS "end" coordinate.
                    continue
                # BED-style end coordinates are "1-based".
                # We use them as-is internally.
                #txEnd = fields[5]
                utr_end = int(fields[5])
                #cdsEnd = fields[7]
                # UTR starts just after the end of the CDS.
                # Hence the "+ 1".
                utr_start = int(fields[7]) + 1
            else:
                #cdsStartStat = fields[13]
                if fields[13] != "cmpl":
                    # Skip data without a reliable CDS "start" coordinate.
                    continue
                #txStart = fields[4]
                # txStart is 0-based, because it is BED-style.
                # Internally, we want a 1-based utr_start
                # Hence the "+ 1".
                utr_start = int(fields[4]) + 1
                #cdsStart = fields[6]
                # cdsStart is 0-based, because it is BED-style.
                # This happens to corresponds to the 1-based coordinate
                # of the position just before: utr_end
                utr_end = int(fields[6])
            # Gene name
            name2 = fields[12]
            if name2 != last_gene:
                if records:
                    write_beds(records, bed_all_file, bed_longest_file)
                    # Reset records
                    records = []
                last_gene = name2
            #exonStarts = [int(pos) for pos in fields[9].split(",")]
            #exonEnds = [int(pos) for pos in fields[10].split(",")]
            # Exon ends from the file are 1-based, but exon starts are 0-based,
            # because it is BED-style.
            # We have to add 1 to get the starts 1-based
            # and have everything internally consistant.
            exon_starts = [int(pos) + 1 for pos in fields[9].split(",") if pos]
            exon_ends = [int(pos) for pos in fields[10].split(",") if pos]
            exon_coords = zip(exon_starts, exon_ends)
            # Determine exon overlap with 3'UTR
            # List of (length, (start, end))
            fragment_coords = []
            fragment_lengths = []
            for exon in exon_coords:
                if exon[1] < utr_start:
                    # Skip exons that end before the 3'UTR
                    continue
                if exon[0] > utr_end:
                    # No more exon will overlap with the 3'UTR
                    break
                frag_start = max(utr_start, exon[0])
                frag_end = min(utr_end, exon[1])
                frag_coords = (frag_start, frag_end)
                fragment_coords.append(frag_coords)
                frag_length = (frag_end + 1) - frag_start
                fragment_lengths.append(frag_length)
            records.append(Three_p(name, chrom, strand, fragment_coords, sum(fragment_lengths), last_gene))
        if records:
            write_beds(records, bed_all_file, bed_longest_file)
    return 0

if __name__ == "__main__":
    sys.exit(main())
