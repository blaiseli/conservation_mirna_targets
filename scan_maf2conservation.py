#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""
This script will read a maf-formatted genome alignment a reference sequence
(that should be present in the alignment) and a motif length *k*.
For each position in the reference genome, it will output the list of genomes
that have the same sequence of length *k* as the reference starting at this
alignement position. The output is in bed format.
"""

import argparse
import sys
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


#import copy
import os
OPJ = os.path.join
OPE = os.path.exists
OPB = os.path.basename
from os.path import splitext
from gzip import open as gzopen
from re import sub
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write

#maf_path = "/home/bli/src/alignio-maf"
#sys.path.insert(1, maf_path)
try:
    #from Bio import AlignIO
    #from Bio.AlignIO.MafIO import MafIndex
    from Bio.AlignIO.MafIO import MafIterator
except ImportError:
    sys.stderr.write(
        "This script requires the presence of the MafIO "
        "Biopython extra module\n"
        "(http://biopython.org/wiki/Multiple_Alignment_Format)\n")
    sys.exit(1)

#try:
#    from seed2num import make_seeds
#except ImportError:
#    sys.stderr.write(
#        "This script requires the presence of the seed2num module.\n")
#    sys.exit(1)


from operator import attrgetter
from operator import and_ as inter
#from operator import or_
#
#from bisect import insort
#from bisect import bisect_left
from itertools import ifilter, imap, izip, product, repeat

from collections import deque, namedtuple

#from itertools import ifilterfalse
#import pysam
#
#from Bio.Seq import reverse_complement
from sqlite3 import connect, IntegrityError, OperationalError
# To get back dictionaries that convert between codes and NCBI taxonomy IDs
from cPickle import load

from string import maketrans, Template
DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")


def dna_rev_compl(seq):
    """Faster than Bio.Seq.reverse_complement"""
    return seq.translate(DNA_COMPL_TABLE)[::-1]


Column = namedtuple(
    "Column",
    ["real_pos", "letter", "genomes"])


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################
class Globals(object):
    """This object holds global variables."""
    @staticmethod
    def load_dicts(pickle_file_name):
        """Use cPickle.load to retrieve the dictionaries."""
        with open(pickle_file_name, "rb") as pickle_file:
            Globals.mirtax2taxid = load(pickle_file)
            Globals.taxid2mirtax = load(pickle_file)
            Globals.ucsc2taxid = load(pickle_file)
            Globals.taxid2ucsc = load(pickle_file)


class Options(object):
    """This object contains the values of the global options."""
    ref = None


#################
# main function #
#################

def main():
    """Main function of the program."""
    WRITE("#%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-m", "--maf",
        required=True,
        help="Alignemnt file in .maf format from which sub-alignments "
        "will be extracted. Its name should be in the form "
        "<ref_chromosome>.maf, where <ref_chromosome> is a chromosome name "
        "in the reference sequence.")
    parser.add_argument(
        "-r", "--ref",
        required=True,
        help="Name (as in the .maf file) of the of the organism used as "
        "reference for the zones to extract in the .bed files. Ex: hg19")
    parser.add_argument(
        "-l", "--seed_length",
        type=int,
        default=7,
        help="Length of the seeds.")
    #parser.add_argument(
    #    "-d", "--db_dir",
    #    required=True,
    #    help="Name of the directory in which to write the files containing "
    #    "sites information.")
    args = parser.parse_args()
    Options.maf_file = args.maf
    Options.ref = args.ref

    # To avoid repeatedly calling these as string methods:
    upper = str.upper
    strip = str.rstrip
    split = str.split
    join = str.join
    # To join the letters of the seeds
    cat = "".join

    chrom_name, ext = splitext(OPB(args.maf))
    open_maf = open
    if ext == ".bgz":
        chrom_name, ext = splitext(chrom_name)
        open_maf = gzopen
    elif ext == ".gz":
        chrom_name, ext = splitext(chrom_name)
        open_maf = gzopen
    #mafindex = "%s.mafindex" % splitext(args.maf)[0]
    #assert ext == ".maf"
    if chrom_name[-3:] in {"bis", "ter"}:
        chrom_name = chrom_name[:-3]
    ref_chrom = join(".", [Options.ref, chrom_name])
    WRITE("#Will search for sequence conservation in %s\n" % ref_chrom)

    id_getter = attrgetter("id")
    seq_getter = attrgetter("seq")
    genomes_getter = attrgetter("genomes")
    letter_getter = attrgetter("letter")

    def has_ref_chrom(fields):
        """Tells if a given tuple has its first element equal to the reference
        chromosome"""
        return fields[0] == chrom_name

    def is_ref(seq_record):
        """Tells if the id attribute of a *seq_record* is ref_chrom."""
        return id_getter(seq_record) == ref_chrom

    def get_upper_seq(seq_record):
        """Returns the uppercase version of the seq attribute
        of *seq_record*."""
        return upper(str(seq_getter(seq_record)))

    #def get_id_and_seq(seq_record):
    #    """Returns the id attribute uppercase version
    #    of the seq attribute of *seq_record*."""
    #    return (id_getter(seq_record), get_upper_seq(seq_record))
    #
    #def get_genome_code_and_seq(seq_recor):
    #    """Returns the genome code and uppercase version
    #    of the seq attribute of *seq_record*."""
    #    return (split(id_getter(seq_rec), ".")[0], get_upper_seq(seq_record))

    # Used to avoid recalculations when bed is well sorted:
    #site_info_file = open(
    #    OPJ(args.db_dir, "%s_conservation.bed" % chrom_name), "w")
    # ifilter may be useless since we work with
    # per-reference chromosome bed files.
    # Multiple sequence alignments
    #msas = idx.search([start], [end])
    # bed strands are "+"/"-", biopython strands are 1/-1
    WRITE("#Parsing maf file: %s.\n" % args.maf)
    with open_maf(args.maf) as maf_file:
        maf_iter = MafIterator(maf_file)

        def iter_aligned_columns():
            """yields aligned columns, with the necessary information to
            compute its genomic position and to know which genomes have the
            same value as the reference in that column."""
            for ali in maf_iter:
                ref_seq = next(ifilter(is_ref, ali))
                # start of the alignment in zero-based coordinates
                start = ref_seq.annotations["start"]
                # length of the alignment
                ref_len = ref_seq.annotations["size"]
                gapped_seq = get_upper_seq(ref_seq)
                ungapped_seq = sub("-", "", gapped_seq)
                assert len(ungapped_seq) == ref_len
                gaps = 0
                for pos, letter in enumerate(gapped_seq):
                    if letter == "-":
                        gaps += 1
                    genomes = {split(identifier, ".")[0] for identifier in {
                        id_getter(seq_rec) for seq_rec in ali if get_upper_seq(
                            seq_rec)[pos] == letter}}
                    assert Options.ref in genomes
                    real_pos = start + pos - gaps
                    yield Column(real_pos, letter, genomes)

        local_ali = deque()
        ungapped_length = 0
        discontinuities = 0
        for column in iter_aligned_columns():
            # Detect when alignment blocks are not contiguous
            # and flush the deque before appending in that case
            real_end = column.real_pos
            if len(local_ali):
                last_col = local_ali[-1]
                last_real_pos = last_col.real_pos
                if real_end > last_real_pos + 1:
                    # alignment blocks are not contiguous
                    local_ali.clear()
                    ungapped_length = 0
                    discontinuities += 1

            local_ali.append(column)
            start_col = local_ali[0]
            while start_col.letter == "-":
                # purge from starting gaps
                local_ali.popleft()
                if len(local_ali):
                    start_col = local_ali[0]
                else:
                    break
            if column.letter != "-":
                # This column should not have been popped yet
                ungapped_length += 1
            if ungapped_length == args.seed_length:
                # genomes that are equal to the reference
                # all along the local_ali
                genomes = reduce(inter, map(genomes_getter, local_ali))
                real_start = start_col.real_pos
                gapped_seq = cat(map(letter_getter, local_ali))
                WRITE("%s\n" % join("\t", (
                    chrom_name,
                    str(real_start),
                    str(real_end + 1),
                    sub("-", "", gapped_seq),
                    join(",", sorted(genomes)))))
                removed = local_ali.popleft()
                assert removed.letter != "-"
                ungapped_length -= 1
        warnings.warn("%d discontinuities found" % discontinuities)
    return 0

if __name__ == "__main__":
    sys.exit(main())
