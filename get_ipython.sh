#!/usr/bin/env bash
# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

version="4.0.0"
program="ipython"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/i/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="4.0.0"
program="traitlets"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/t/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="0.1.0"
program="ipython_genutils"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/i/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="4.0.4"
program="decorator"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/d/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="4.0.1"
program="pexpect"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/p/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="0.5"
program="ptyprocess"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/p/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="0.5"
program="pickleshare"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/p/${program}/${program}-${version}.tar.gz || error_exit "${program} download failed"
tar -xvzf ${program}-${version}.tar.gz
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"


version="0.8.1"
program="simplegeneric"

mkdir -p ${HOME}/src
cd ${HOME}/src
rm -rf ${program}-${version}*
wget --continue --no-check-certificate https://pypi.python.org/packages/source/s/${program}/${program}-${version}.zip || error_exit "${program} download failed"
unzip ${program}-${version}.zip
cd ${program}-${version}
CFLAGS="-march=native -fomit-frame-pointer -I${HOME}/include -L${HOME}/lib" LDFLAGS="-Wl,-rpath,${HOME}/lib" python setup.py build || error_exit "${program} build failed"
python setup.py install --user || error_exit "${program} install failed"



exit 0
