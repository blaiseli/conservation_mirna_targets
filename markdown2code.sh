#!/bin/sh
# Usage: markdown2code.sh <README.md>
# This will extract the parts indented by 4 spaces.
# This can be further selected with head, tail, grep and piped to bash
# (or the interpretation language the code was intended for)
# Environment variables used in README.md can be set between the pipe and bash
# Example: markdown2code.sh README.md | VARIABLE="test" bash

grep "^    " ${1} | sed 's/^    //'
