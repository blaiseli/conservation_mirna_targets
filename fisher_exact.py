#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""

This script performs a Fischer exact test using sets of elements provided as
files with one element per line.

"""

import argparse
import sys
import warnings
from string import strip
from scipy.stats import fisher_exact

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

WRITE = sys.stdout.write

#################
# main function #
#################

def main():
    """Main function of the program."""
    #WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-d", "--domain",
        required=True,
        help="Name of the file containing the domain "
        "within which to perform the test.\n"
        "The file should contain one element per line.")
    parser.add_argument(
        "file1",
        help="File containing one name per line, "
        "to be used as first set of elements.")
    parser.add_argument(
        "file2",
        help="File containing one name per line, "
        "to be used as second set of elements.")
    parser.add_argument(
        "file3",
        help="File containing one name per line, "
        "to be used as third set of elements.")
    parser.add_argument(
        "file4",
        help="File containing one name per line, "
        "to be used as fourth set of elements.")
    args = parser.parse_args()
    
    with open(args.domain) as file0:
        domain = set(map(strip, file0))
    with open(args.file1) as file1:
        set1 = domain & set(map(strip, file1))
    with open(args.file2) as file2:
        set2 = domain & set(map(strip, file2))
    with open(args.file3) as file3:
        set3 = domain & set(map(strip, file3))
    with open(args.file4) as file4:
        set4 = domain & set(map(strip, file4))

    counts1 = len(set1 & set3)
    counts2 = len(set1 & set4)
    counts3 = len(set2 & set3)
    counts4 = len(set2 & set4)
    print "%d\t%d\t%d" % (counts1, counts2, len(set1))
    print "%d\t%d\t%d" % (counts3, counts4, len(set2))
    print "%d\t%d" % (len(set3), len(set4))
    _, pvalue = fisher_exact([[counts1, counts2], [counts3, counts4]])
    WRITE("p-value: %s\n" % pvalue)
    _, pvalue = fisher_exact([[counts1, counts3], [counts2, counts4]])
    WRITE("p-value: %s\n" % pvalue)
    return 0

if __name__ == "__main__":
    sys.exit(main())
