Content
=======

This repository mainly contains scripts used for a study of miRNA seed match
conservation.

Most scripts are written in python or bash.

There also are some markdown-formatted README files that I like to call
"executable READMEs". They explain the analyses and contain code sections,
indented by four spaces. To a certain extent, the analyses can be reproduced by
simply extracting these code sections and piping them to bash. Some environment
variables can be used to select options when performing the analyses.
    
    [[ ${OPTION} ]] || OPTION="default"
    echo "Extracting and executing the code setting OPTION to ${OPTION}"
    # This code section is commented in order to avoid infinite recursion
    echo "Actually not executing all the code"
    #grep "^    " README.md | sed 's/^    //' | OPTION=${OPTION} bash


If you want to execute only some parts of the code, you may use `grep`, `tail`,
`head` and others, but this may skip sections where variables are set, that are
necessary in later parts.

I also provide the `markdown2code.sh` script that will do the `grep` and `sed` for you.

    # filtering out the line that would continue recursion
    markdown2code.sh README.md | grep -v "markdown2code.sh" | OPTION=${OPTION} bash


This script, together with `pandoc` can be used as a poor man's [literate
programming](https://en.wikipedia.org/wiki/Literate_programming) kit.


Be careful: my executable READMEs might download huge amount of data (MAF genome
alignments are large files). Study them before executing them.
