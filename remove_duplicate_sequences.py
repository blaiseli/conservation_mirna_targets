#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script removes duplicate sequences from a fasta alignment usin p4 and
generates a dictionary that p4 can later use to restore duplicated taxa."""

import argparse
import os
import sys
import warnings

def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import p4
p4.var.doCheckForDuplicateSequenceNames = False

#class Globals(object):
#    """Class used to hold global variables as its class attributes."""
#
def main():
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "in_fasta",
        help="Input fasta file.")
    parser.add_argument(
        "out_fasta",
        help="Output fasta file.")
    args = parser.parse_args()

    p4.read(args.in_fasta)
    ali = p4.var.alignments[0]
    #ali = p4.var.sequenceLists[0]
    if os.path.isfile("p4DupeSeqRenameDict.py"):
        os.remove("p4DupeSeqRenameDict.py")
    ali.checkForDuplicateSequences(removeDupes=True, makeDict=True)
    ali.writeFasta(args.out_fasta, width=0, writeExtraNewline=False)
    sys.stdout.write("The dictionnary allowing p4 to restore removed "
                     "duplicates is saved in module p4DupeSeqRenameDict.py\n")
    return 0

if __name__ == "__main__":
    sys.exit(main())
