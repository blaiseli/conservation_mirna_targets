#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""
This script will read a fasta-formatted file (possibly gzipped) and a file
containing a list of sequence names. It will extract each sequence separately
into its own fasta file.
"""

import argparse
import sys
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s:%s" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning


#import copy
import os
OPJ = os.path.join
OPE = os.path.exists
OPD = os.path.dirname
OPB = os.path.basename
from os.path import splitext
from gzip import open as gzopen
# see https://wiki.python.org/moin/PythonSpeed/PerformanceTips#Doing_Stuff_Less_Often
sys.setcheckinterval(1000)
WRITE = sys.stdout.write

from Bio import SeqIO

#from operator import attrgetter
#from operator import and_ as inter
#from operator import or_
#
#from bisect import insort
#from bisect import bisect_left
from itertools import imap


#from string import maketrans, Template
#DNA_COMPL_TABLE = maketrans("ACGT", "TGCA")
#
#
#def dna_rev_compl(seq):
#    """Faster than Bio.Seq.reverse_complement"""
#    return seq.translate(DNA_COMPL_TABLE)[::-1]



#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################
class Globals(object):
    """This object holds global variables."""


class Options(object):
    """This object contains the values of the global options."""


#################
# main function #
#################

def main():
    """Main function of the program."""
    #WRITE("#%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "fasta_file",
        help="File in fasta format containing the sequences of which some "
        "have to be extracted.")
    parser.add_argument(
        "-s", "--seq_names",
        required=True,
        help="File containing the list of sequences to extract.\n"
        "One sequence name per line.",
        type=argparse.FileType("r"))
    args = parser.parse_args()

    # To avoid repeatedly calling these as string methods:
    strip = str.rstrip

    seq_names = set(imap(strip, args.seq_names))
    out_dir = OPD(args.fasta_file)
    base_name, ext = splitext(OPB(args.fasta_file))
    if ext == ".bgz":
        open_fasta = gzopen
    elif ext == ".gz":
        open_fasta = gzopen
    else:
        open_fasta = open

    with open_fasta(args.fasta_file) as fasta_file:
        for record in SeqIO.parse(fasta_file, "fasta"):
            if record.name in seq_names:
                SeqIO.write(
                    record,
                    OPJ(out_dir, "%s.fa" % record.name), "fasta")

    return 0

if __name__ == "__main__":
    sys.exit(main())
